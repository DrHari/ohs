﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using RDotNet;


//keep this members in line with the AMCO db designed
/*
 * empID varchar(40),
empName varchar(50),
empEmail varchar(100),
passWrd varchar(100),
location varchar(100),
site varchar(100),
floor varchar(100),
registereWithBeinge int
*/
namespace Amco_sample
{

    public class Amco_Person
    {
        public string empID { get; set; }
        public string empName { get; set; }
        public string empEmail { get; set; }
        public string location { get; set; }
        public string site { get; set; }
        public string floor { get; set; }
        public string passWrd { get; set; }
        public int registereWithBeinge { get; set; }
        public string Being_PersonID { get; set; }  // the id returned by registering with Beigne

        public Amco_Person ( Amco_Person p)
        {
            empID = p.empID;
            empName = p.empName;
            empEmail = p.empEmail;
            location = p.location;
            site = p.site;
            floor = p.floor;
            passWrd = p.passWrd;
            registereWithBeinge = p.registereWithBeinge;
            Being_PersonID = p.Being_PersonID;


        }
       public Amco_Person()
        {

        }

    }

    public class amco_params
    {
        public int paramID { get; set; }
        public String paramName { get; set; }

        public amco_params(int n, String name)
        {
            paramID = n;
            paramName = name;
        }
        public amco_params()
        {
            paramID = 0;
            paramName = "";
        }
    }
    public class  Amco_HR
    {
        private string myConnectionString;
        public MySqlConnection connection;
        public string amco_server{get;set;}
        public string amco_db { get; set; }
        public string amco_user { get; set;}

        public string amco_pwd { get; set; }

        public bool validUser { get; set; } 

        public Amco_HR()
        {
            amco_server = "localhost";
            amco_db = "amco_hr";
            amco_user = "root";
            amco_pwd = "hari";
            int nAttempts = 0;
            myConnectionString = "Server=" + amco_server + ";Database=" + amco_db + ";Uid=" + amco_user + ";pwd=" + amco_pwd + ";";
            // myConnectionString = "Server=localhost;Database=amco_hr;Uid=root;pwd=hari;";
            validUser = false;
            //here thro a form , that will accept user name, pwd
            // for amco give 3 or 4 users created in the database 
            // three wrong password attempts close the application
          Variables.amco_log =  new Logger(myConnectionString);
            App_Login amco_login = new App_Login();
        LOGIN_ATTEMPTS:
            amco_login.userName = "";
            amco_login.pwd = "";
            amco_login.ShowDialog();
            amco_user = amco_login.userName;
            amco_pwd = amco_login.pwd;
            try
            {
                myConnectionString = "Server=" + amco_server + ";Database=" + amco_db + ";Uid=" + amco_user + ";pwd=" + amco_pwd + ";";
                connection = new MySqlConnection(myConnectionString);

                connection.Open();

                if (connection.State != ConnectionState.Open)
                {
                    MessageBox.Show(" Connection opening Error..Configure the connection parameter.. check..");
                    // MessageBox.Show(e.Message);

                }
                else {
                    validUser = true;
                    // if the user is valid and he is yeto register with Beinge and not logged in.. do the check here..
                   // check_Beinge_Sign_Log_in();
                    amco_login.Close();
                }
            }
            catch(MySql.Data.MySqlClient.MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server. Check User Name & pwd or Contact administrator..");
                        Variables.amco_log.LogMessage(amco_user, "UnAuthorized access "+ nAttempts);
                        nAttempts++;
                        if (nAttempts < 3) { goto LOGIN_ATTEMPTS; };
                        break;
                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        Variables.amco_log.LogMessage(amco_user, "UnAuthorized access " + nAttempts);
                        nAttempts++;
                        if (nAttempts < 3) { goto LOGIN_ATTEMPTS; };
                        // all three attempts Failed.
                        break;
                    default:
                        MessageBox.Show(ex.Message);
                        Variables.amco_log.LogMessage(amco_user, "SQL connecting exception " + ex.Message);
                        break;
                }
               
            }

            //TBD -  error handling & exception to be done
           

        }
         ~Amco_HR()
        {
           if (connection !=null)  connection.Close();
        }

       
        public int init_data()
        {
            //if there is a excel file, read from excel file and upload data to the local data base
            // add a button and on click, select a file and upload the new employee list provided by HR
            

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = @"F:/Learning/Amco_sample/Amco_sample/bin/Debug/";
            openFileDialog1.Filter = "xls files (*.xls)|*.xlsx";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            int returnResult=0;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    string excelFileNamewithPath = openFileDialog1.FileName;
                    // MessageBox.Show(openFileDialog1.InitialDirectory + openFileDialog1.FileName);
                    returnResult= ReadFromXLSFile(excelFileNamewithPath); 
                    if(returnResult==1) MessageBox.Show("Data From " + excelFileNamewithPath + ".. copied into database..");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }

            }

            return returnResult;

        }
        private int ReadFromXLSFile(string fname)
        {
            Microsoft.Office.Interop.Excel.Application xlApp;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlApp = new Microsoft.Office.Interop.Excel.Application();

            
            xlWorkBook = xlApp.Workbooks.Open(fname, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

           
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            int columns = 0, rows = 0;
            if (xlWorkSheet != null)
            {
                columns = xlWorkSheet.UsedRange.Columns.Count;
                rows = xlWorkSheet.UsedRange.Rows.Count; ;
            }
           // MessageBox.Show("Opened XLS file for importing..");
          //  MessageBox.Show("No of rows in the data "+rows.ToString());
           // MessageBox.Show("No of columns in the data "+ columns.ToString());
            
            MySqlCommand cmd;
            int startNo = 64;
            string endRange;
            //Range range;
            string rowValues = null;
            string[] cellValues = new string[100];
            int no_of_new_employees = 0;
            //Verify the excel file is in the format that we need. check on reading the first row.
            //if it not maches, return, after displaying the error message
            //empid	empname	emailId	password	location	site	floor	

            String[] ColumnNames = { "Empid", "EMPLOYEE NAME", "TAFE Email address", "Email Password", "Location", "Site", "Floor" }; // ,  "registeredWithBeinge",  "BeingePersonID" };
            //Empid	EMPLOYEE NAME	TAFE Email address	Email Password	Location	Site	Floor


            for (int ii = 1; ii <= columns; ii++)
            {

               // endRange = Convert.ToChar(startNo + ii).ToString() + firstrow.ToString();
                

                cellValues[ii] = xlWorkSheet.Cells[1, ii].Text.ToString();
                if(cellValues[ii]!=ColumnNames[ii-1])
                    {
                    
                    MessageBox.Show(" File is not in the format, kindly check the format..");
                    return 0;
                    
                    }
                

            }
                

                // file is ok on the format .. continue reading..
            for (int jRow = 2; jRow <= rows; jRow++)
              {
                rowValues = null;
                for (int i = 1; i <= columns; i++)
                {

                    endRange = Convert.ToChar(startNo + i).ToString() + jRow.ToString();
                    // MessageBox.Show(endRange);
                  
                    cellValues[i] = xlWorkSheet.Cells[jRow, i].Text.ToString();
                

                    rowValues += cellValues[i] + ",";

                }


                // now insert this row into mySQL database
                // TBD provided , this emp id ( trim(cellValues[1])  does not exist..
                // so connect to db and get for this empid, if returns == 0, then  set flag newEmployee= true
                bool newEmployee = false;

                List<Amco_Person> person2get = new List<Amco_Person>();
                String queryString = "SELECT  * FROM amco_hr.amco_emp where empID= " + cellValues[1].Trim();
                int count = getList(person2get, queryString);
                if (count == 0)
                {
                    newEmployee = true;   // no such emp id exist.. can add an employee
                }

                
                if (newEmployee) 
               // if (int.Parse(cellValues[8]) == 0)  // if (newEmployee)
                {
                    no_of_new_employees++;
                    cmd = connection.CreateCommand();

                    //cmd.CommandText = "INSERT into amco_emp( empID,empName,empEmail,passWrd,location,site,floor,registereWithBeinge,BeingePersonID) Values(@cmdVar1,@cmdVar2,@cmdVar3,@cmdVar4,@cmdVar5,@cmdVar6,@cmdVar7,@cmdVar8,@cmdVar9)";
                    cmd.CommandText = "INSERT into amco_emp( empID,empName,empEmail,passWrd,location,site,floor,registereWithBeinge) Values(@cmdVar1,@cmdVar2,@cmdVar3,@cmdVar4,@cmdVar5,@cmdVar6,@cmdVar7,@cmdVar8)";
                    cmd.Parameters.AddWithValue("@cmdVar1", cellValues[1]);
                    cmd.Parameters.AddWithValue("@cmdVar2", cellValues[2]);
                    cmd.Parameters.AddWithValue("@cmdVar3", cellValues[3]);
                    cmd.Parameters.AddWithValue("@cmdVar4", cellValues[4]);
                    cmd.Parameters.AddWithValue("@cmdVar5", cellValues[5]);
                    cmd.Parameters.AddWithValue("@cmdVar6", cellValues[6]);
                    cmd.Parameters.AddWithValue("@cmdVar7", cellValues[7]);
                    cmd.Parameters.AddWithValue("@cmdVar8", 0);    // int.Parse(cellValues[8]));  // 0 for not registered  1 for registered
                    cmd.Parameters.AddWithValue("@cmdVar9", cellValues[9]);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    MessageBox.Show("Employee Id : " + cellValues[1].Trim() + " Already added.. !");
                }

            } // end of adding a row
            MessageBox.Show(" Number of new Employees to be registered" + no_of_new_employees);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            //connection.Close();
            return 1;

        }

        


      
        public int getListRegister2LogIn(List<Amco_Person> persons2login)
        {
            // get people who are registered with beinge but yet to login and get their BEINGE ID
            MySqlCommand cmd;
            MySqlDataReader reader;
            if (connection == null) connection.Open();
            cmd = connection.CreateCommand();

            cmd.CommandText = "SELECT  * FROM amco_hr.amco_emp where registereWithBeinge = 1 && BeingePersonID=\"\"";
            //cmd.ExecuteNonQuery();

            reader = cmd.ExecuteReader();
            int readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Amco_Person p = new Amco_Person();

                    p.empID = reader.GetString(0);
                    p.empName = reader.GetString(1);
                    p.empEmail = reader.GetString(2);
                    p.passWrd = reader.GetString(3);
                    p.location = reader.GetString(4);
                    p.site = reader.GetString(5);
                    p.floor = reader.GetString(6);

                    p.registereWithBeinge = reader.GetInt32(7);
                    p.Being_PersonID = reader.GetString(8).Trim();
                    persons2login.Add(p);

                    // MessageBox.Show("row " + p.empID);
                    readRow++;

                }
            }
            else
            {
                //MessageBox.Show("No rows found.");
            }
            reader.Close();

            //MessageBox.Show("No of rows read " + readRow.ToString());
            //connection.Close();
            return readRow;

        }
        public int getList2Register2Beinge(List<Amco_Person> persons2register, int registered) //Amco_Person[] persons)
        {
            // get the people who are not registered with Beinge.. where registereWithBeinge=0
            // registered = 0 means, NOT registered with BEIGNE    =1 means registered
           // Persons should be used as list elments and add
            MySqlCommand cmd;
            MySqlDataReader reader;
            if(connection==null )connection.Open();
            cmd = connection.CreateCommand();

            cmd.CommandText = "SELECT  * FROM amco_hr.amco_emp where registereWithBeinge = "+ registered.ToString();
            //cmd.ExecuteNonQuery();
            
            reader = cmd.ExecuteReader();
            int readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Amco_Person p = new Amco_Person();
                    
                    p.empID = reader.GetString(0);
                    p.empName = reader.GetString(1);
                    p.empEmail = reader.GetString(2);
                    p.passWrd = reader.GetString(3);
                    p.location = reader.GetString(4);
                    p.site = reader.GetString(5);
                    p.floor = reader.GetString(6);
                    
                    p.registereWithBeinge = reader.GetInt32(7);
                    if (p.registereWithBeinge == 0) { p.Being_PersonID = ""; }
                    else { p.Being_PersonID = reader.GetString(8); }
                    
                     
                    persons2register.Add(p);
                    
                   // MessageBox.Show("row " + p.empID);
                    readRow++;
                    
                }
            }
            else
            {
                //MessageBox.Show("No rows found.");
            }
            reader.Close();

            //MessageBox.Show("No of rows read " + readRow.ToString());
            //connection.Close();
            return readRow;
        }

        public int getList(List<Amco_Person> person2get, String queryString)
            {
            MySqlCommand cmd;
            MySqlDataReader reader;
            if (connection == null) connection.Open();
            cmd = connection.CreateCommand();

            cmd.CommandText = queryString;
            //cmd.ExecuteNonQuery();

            reader = cmd.ExecuteReader();
            int readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Amco_Person p = new Amco_Person();

                    p.empID = reader.GetString(0);
                    p.empName = reader.GetString(1);
                    p.empEmail = reader.GetString(2);
                    p.passWrd = reader.GetString(3);
                    p.location = reader.GetString(4);
                    p.site = reader.GetString(5);
                    p.floor = reader.GetString(6);

                    p.registereWithBeinge = reader.GetInt32(7);
                    p.Being_PersonID = reader.GetString(8).Trim();
                    person2get.Add(p);

                    // MessageBox.Show("row " + p.empID);
                    readRow++;

                }
            }
            else
            {
               // MessageBox.Show("No rows found.");
            }
            reader.Close();

            //MessageBox.Show("No of rows read " + readRow.ToString());
            //connection.Close();
            return readRow;

        }
            

    }

   

}
