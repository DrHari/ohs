﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amco_sample
{
    public class parameterResponse
    {
        
        public bool isSuccess { get; set; }
        public int statusCode { get; set; }
        public string message { get; set; }
        

        [JsonProperty("parameterValues")]
        public List<ParameterValues> parameterValues { get; set; }


        [JsonProperty("legendaryNames")]
        public List<string> legendarynames { get; set; }

        [JsonProperty("dates")]
        public List<string> dates { get; set; }


        [JsonProperty("patientId")]
        public int patientId { get; set; }

        public parameterResponse()
        {
            parameterValues = new List<ParameterValues>();
            dates = new List<String>();
            legendarynames = new List<String>();
        }
    }

    public class ParameterValues
    {

        public List<string> param { get; set; }


        public ParameterValues()
        {
            param = new List<string>();
        }
    }
}
