-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.6.32-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema amco_hr
--

CREATE DATABASE IF NOT EXISTS amco_hr;
USE amco_hr;

--
-- Definition of table `amco_emp`
--

DROP TABLE IF EXISTS `amco_emp`;
CREATE TABLE `amco_emp` (
  `empID` varchar(40) DEFAULT NULL,
  `empName` varchar(50) DEFAULT NULL,
  `empEmail` varchar(100) DEFAULT NULL,
  `passWrd` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `site` varchar(100) DEFAULT NULL,
  `floor` varchar(100) DEFAULT NULL,
  `registereWithBeinge` int(11) DEFAULT NULL,
  `BeingePersonID` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `amco_emp`
--

/*!40000 ALTER TABLE `amco_emp` DISABLE KEYS */;
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('710041','K. NAGARAJ','710041.nagaraj@amco.com','welcome123','MM','tpsd3','2w Ass',1,'3235'),
 ('710159','K SARAVANKUMAR','710159.saravanakumar@amco.com','welcome123','MM','tpsd3','2w Ass',1,'3236'),
 ('710255','S.SURESH KUMAR','710255.sureshkumar@amco.com','welcome123','MM','tpsd3','ASSEMBLY',1,'3237'),
 ('710160','P.HARIDOSS','710160@haridoss@amco.com','welcome123','MM','tpsd3','ASSEMBLY',1,'3238'),
 ('710040','A.G.MOHANAKRISHNAN ','710040.mohanakrishnan@amco.com','welcome123','MM','tpsd3','2w Ass',1,'3239'),
 ('710007','MMM PERUMAL ','710007.perumal@amco.com','welcome123','MM','tpsd3','Production',1,'3240'),
 ('710240','S.SUBASH    ','710240.subash@amco.com','welcome123','MM','tpsd3','Purchase',1,'3241'),
 ('710025','S.KARTHIKEYAN','710025.karthikeyan@amco.com','welcome123','MM','tpsd3','Maint',1,'3242'),
 ('710209','K SELVAKUMAR','710209.selvakumar@amco.com','welcome123','MM','tpsd3','Personnel',1,'3243'),
 ('7','S.BALASUBARMANIAN','7.balasubramanian@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3244');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('8','P.SELVAM','8.selvam@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3245'),
 ('49','R.IYANAR','49.iyanar@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3246'),
 ('126','S.LINGESAN','126.lingesan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3247'),
 ('74','K.SHENBAGAM','74.shenbagam@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3248'),
 ('78','E.MUTHUPANDI','78.muthupandi@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3249'),
 ('120','K.LOGANATHAN','120.loganathan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3250'),
 ('174','V.SHANMUGAM','174.shanmugham@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3251'),
 ('170','M.GANAPATHY','170.ganapathy@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3252'),
 ('710096','K.Ramesh','710096.ramesh@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3253'),
 ('710100','D.Paramanathan','710100.paramanathan@amco.com','welcome123','MM','tpsd2','CASTING',1,'3254'),
 ('710102','G.Chokkanathan','710102.chockanathan@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3255');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('710134','R. Ramachandran','710134.ramachandran@amco.com','welcome123','MM','tpsd2','FORK LIFT',1,'3256'),
 ('710162','S.Santosh Kumar','710162.santhoshkumar@amco.com','welcome123','MM','tpsd2','QC',1,'3257'),
 ('710242','R. GANESAN','710242,ganesan@amco.com','welcome123','MM','tpsd2','PASTING',1,'3258'),
 ('710133','C. VINOTHKUMAR','710133.vinothkumar@amco.com','welcome123','MM','tpsd2','Formation',1,'3259'),
 ('710092','P.SUNDARAJ','710092.sundaraj@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3260'),
 ('710055','L.SETHURAMALINGAM','710055.sethuramalingam@amco.com','welcome123','MM','tpsd2','Formation',1,'3261'),
 ('710219','B.Ramesh','710219.ramesh@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3262'),
 ('99999','Hari','hari@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3263'),
 ('710140','G THIYAGARAJAN','710140.thiyagarajan@amco.com','welcome123','MM','tpsd3','2w Ass',1,'3265'),
 ('710135','N.LOGANATHAN','710135.loganathan@amco.com','welcome123','MM','tpsd3','2w Ass',1,'3266');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('710222','R.RAJA','710222.raja@amco.com','welcome123','MM','tpsd3','2w Ass',1,'3267'),
 ('710120','K.ANANTHAN','710120.ananthan@amco.com','welcome123','MM','tpsd3','ASS-CHA',1,'3268'),
 ('710042','T.RAMESH','710042.ramesh@amco.com','welcome123','MM','tpsd3','VRLA',1,'3269'),
 ('710197','R.MOHAMMED RABEEK','710197.mohammedrabeek@amco.com','welcome123','MM','tpsd3','ELE MAIN',1,'3270'),
 ('710230','A.SILAMBARASAN','710230.silambarasan@amco.com','welcome123','MM','tpsd3','ELE MAIN',1,'3271'),
 ('710244','K.RAMESH KUMAR','710244.rameshkumar@amco.com','welcome123','MM','tpsd3','CASTING',1,'3272'),
 ('710254','M.ANANDHARAJAN','710254.anandharajan@amco.com','welcome123','MM','tpsd3','CASTING',1,'3273'),
 ('710079','J RAMKUMAR','710079.ramkumar@amco.com','welcome123','MM','tpsd3','QC',1,'3274'),
 ('710202','N MANAVALAN','710202.manavalan@amco.com','welcome123','MM','tpsd3','Elect',1,'3275'),
 ('710192','S. VAITHIYANATHAN','710192.vaithiyanathan@amco.com','welcome123','MM','tpsd3','Elect',1,'3276');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('5','R.RAMAR','5.ramar@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3277'),
 ('9','L.MUTHUKRISHNAN','9.muthukrishnan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3278'),
 ('12','P.MICHEAL BOSCO','12.micheal@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3279'),
 ('13','R.BALAMUTHUKUMAR','13.balamuthukumar@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3280'),
 ('14','T.VEERAPANDIAN','14.veerapandian@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3281'),
 ('15','G.MARIAPPAN','15.mariappan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3282'),
 ('21','S.SHANMUGA SUNDARAM','21.shanmugasundaran@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3283'),
 ('22','V.BHOOVARAGAMOORTHY','22.bhoovaragamoorthy@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3284'),
 ('17','K.RAMALINGAM','17.ramalingam@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3285'),
 ('26','K.KALAYANA SUNDARAM','26.kalyanasundarama@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3286');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('30','G.MURUGAN','30.murugan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3287'),
 ('31','V.MURUGAN','31.murugan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3288'),
 ('42','D.BALAJI','42.balaji@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3289'),
 ('24','S.NAGAHARIRAMASUBRAMANIAN','24.nagahariramasubramanian@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3290'),
 ('45','D.JAYAPARKASH','45.jayaprakash@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3291'),
 ('47','R.MURUGESAN','47.murugesan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3292'),
 ('52','S.INDIRATHI','52.indirathi@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3293'),
 ('53','C.RAMESH','53.ramesh@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3294'),
 ('60','M.SEETHARAMAN','60.seetharaman@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3295'),
 ('61','M.KARUPPASAMY','61.karupppasamy@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3296'),
 ('75','R.PARANJOTHI','75.paranjothi@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3297');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('76','S.SHANMUGANANTHAKRISHNAN','76.shanmugananthkrishnan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3298'),
 ('79','E.SENTHILVEL','79.senthilvel@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3299'),
 ('84','R.MURUGESAN','84.murugensan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3300'),
 ('88','M.SURESH KUMAR','88.sureshkumar@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3301'),
 ('89','K.SANKARAN','89.sankaran@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3302'),
 ('90','A.THIRUNAVUKARASU','90.thirunavukarasu@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3303'),
 ('91','K.SENTHILVEL','91.senthilvel@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3304'),
 ('96','N.THIRUMALAI','96.thirumalai@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3305'),
 ('99','K.S.NAGARAJAN','99.nagarajan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3306'),
 ('105','M.SURESH KUMAR','105.sureshkumar@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3307');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('107','M.SUNDAR','107.sundar@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3308'),
 ('153','J.SURYANARAYANAN','153.suryanarayanan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3309'),
 ('95','B.ASHOK KUMAR','95.ashokkumar@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3310'),
 ('101','V.HARIBABU','101.haribabu@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3311'),
 ('109','R.MURALI','109.murali@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3312'),
 ('110','V.MUTHURAJ','110.muthuraj@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3313'),
 ('111','N.SIVAKUMAR','111.sivakumar@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3314'),
 ('121','C.KOTHANDARAMAN','121.kothandaraman@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3315'),
 ('98','N.ANANTHARAMAKRISHNAN','98.anantharamakirshnan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3316'),
 ('113','S.GOPAL','113.gopal@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3317');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('118','N.KRISHNAN','118.krishnan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3318'),
 ('122','V.ELUMALAI','122.velumalai@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3319'),
 ('125','R.MEGANATHAN','125.meganatha@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3320'),
 ('163','P.SARAVANAN','163.saravanan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3321'),
 ('165','S.VELAYUDHAM','165.velayudham@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3322'),
 ('123','G.KRISHNAN','123.krishnan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3323'),
 ('124','K.MUNIAN','124.munian@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3324'),
 ('133','K.PANNEER SELVAN','133.panneerselvan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3325'),
 ('141','K.RAJA','141.raja@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3326'),
 ('142','T.RAVICHANDRAN','142.ravichandran@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3327'),
 ('146','D.DHARMAN','146.dharman@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3328');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('167','G.SELVAKUMAR','167.selvakumar@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3329'),
 ('169','H.JOHNSON','169.johnson@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3330'),
 ('37','V.SELVAMUTHUKUMAR','37.selvamuthukumar@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3331'),
 ('135','R.NAGARAJAN','135.nagarajan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3332'),
 ('143','R.MOHAN','143.mohan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3333'),
 ('162','V.DEVADOSS','162.devadoss@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3334'),
 ('164','K.RAJKUMAR','164.rajkumar@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3335'),
 ('168','P.RAJENDRAN','168.rajendran@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3336'),
 ('171','K.KANNAN','171.kannan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3337'),
 ('173','V.PRAKASH','173.prakash@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3338'),
 ('5003','G.Saravanan','5003.saravanan@amco.com','welcome123','MM','tpsd2','Stores',1,'3339');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('710216','M.Surendran ','710216.surendran@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3340'),
 ('710231','G.Haridoss ','710231.haridoss@amco.com','welcome123','MM','tpsd2','Stores',1,'3341'),
 ('710095','V.Janarthanan','710095.janarthanan@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3342'),
 ('710139','K.Magesh','710139.magesh@amco.com','welcome123','MM','tpsd2','CASTING',1,'3343'),
 ('6001','N.Ragunathan','6001.ragunathan@amco.com','welcome123','MM','tpsd2','PASTING',1,'3344'),
 ('710048','S.Arunachalam','710048.arunachalam@amco.com','welcome123','MM','tpsd2','Formation',1,'3345'),
 ('710087','T.M.Gokulakrishnan','710087.gokulakrishnan@amco.com','welcome123','MM','tpsd2','QC',1,'3346'),
 ('710137','D.Jaganathan','710137.jaganathan@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3347'),
 ('710136','R.Thirumal','710136.thirumal@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3348'),
 ('710094','S.Kaldoss','710094.kaldoss@amco.com','welcome123','MM','tpsd2','CASTING',1,'3349');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('710044','S.Sadiqbhasa','710044.sadiqbhasa@amco.com','welcome123','MM','tpsd2','PASTING',1,'3350'),
 ('710123','S.SANKAR ','710123.sankar@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3351'),
 ('710127','S.NARENDIRAN','710127.narendiran@amco.com','welcome123','MM','tpsd2','Formation',1,'3352'),
 ('710236','R.MOHAN KUMAR','710236.mohankumar@amco.com','welcome123','MM','tpsd2','2w Ass',1,'3353'),
 ('710049','M.ANANTHARAJ','710049.anantharaj@maco.com','welcome123','MM','tpsd2','CASTING',1,'3354'),
 ('710204','S.PALANIVEL','710204.palanivel@amco.com','welcome123','MM','tpsd2','Maint',1,'3355'),
 ('710178','P.NAGHUL SAMY','710178.naghulsamy@amco.com','welcome123','MM','tpsd2','R&D',1,'3356'),
 ('710220','V.SATHIYASEELAN','710220.sathiyaseelan@maco.com','welcome123','MM','tpsd2','CASTING',1,'3357'),
 ('710024','S.GOPU','710024.gopu@amco.com','welcome123','MM','tpsd2','CASTING',1,'3358'),
 ('710187','S.KRISHNA','710187.krishna@amco.com','welcome123','MM','tpsd2','Maint',1,'3359');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('710066','B.SATHISH KUMAR','710066.sathishkumar@amco.com','welcome123','MM','tpsd2','Stores',1,'3360');
/*!40000 ALTER TABLE `amco_emp` ENABLE KEYS */;


--
-- Definition of table `amco_hazard_param`
--

DROP TABLE IF EXISTS `amco_hazard_param`;
CREATE TABLE `amco_hazard_param` (
  `paramID` int(11) DEFAULT NULL,
  `paramName` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `amco_hazard_param`
--

/*!40000 ALTER TABLE `amco_hazard_param` DISABLE KEYS */;
INSERT INTO `amco_hazard_param` (`paramID`,`paramName`) VALUES 
 (278,'BLL');
/*!40000 ALTER TABLE `amco_hazard_param` ENABLE KEYS */;


--
-- Definition of table `amco_logger`
--

DROP TABLE IF EXISTS `amco_logger`;
CREATE TABLE `amco_logger` (
  `logtime` datetime DEFAULT NULL,
  `logby` varchar(50) DEFAULT NULL,
  `logMessage` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `amco_logger`
--

/*!40000 ALTER TABLE `amco_logger` DISABLE KEYS */;
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 08:39:37','hari','test message'),
 ('2016-11-26 08:53:04','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:01:24','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:03:27','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:04:52','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:08:23','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:13:30','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:25:38','hello','testing'),
 ('2016-11-26 09:27:13','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:28:03','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:29:28','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:31:16','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:32:54','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:33:18','hari1','UnAuthorized access 0'),
 ('2016-11-26 09:33:51','hari1','UnAuthorized access 1');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 09:34:14','hari1','UnAuthorized access 2'),
 ('2016-11-26 09:34:20','hari1','UnAuthorized access'),
 ('2016-11-26 09:34:57','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:35:04','root','Logged in ..'),
 ('2016-11-26 09:35:06','root','Form - setup Completed..'),
 ('2016-11-26 09:50:33','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:50:39','root','Logged in ..'),
 ('2016-11-26 09:50:40','root','Form - setup Completed..'),
 ('2016-11-26 09:50:54','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:51:00','root','Logged in ..'),
 ('2016-11-26 09:51:00','root','Form - setup Completed..'),
 ('2016-11-26 09:55:20','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 09:55:26','root','Logged in ..'),
 ('2016-11-26 09:55:26','root','Form - setup Completed..'),
 ('2016-11-26 10:10:05','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 10:10:12','root','Logged in ..'),
 ('2016-11-26 10:10:12','root','Form - setup Completed..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 10:14:06','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 10:14:13','root','Logged in ..'),
 ('2016-11-26 10:14:13','root','Form - setup Completed..'),
 ('2016-11-26 10:19:22','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 10:19:30','root','Logged in ..'),
 ('2016-11-26 10:19:30','root','Form - setup Completed..'),
 ('2016-11-26 10:34:58','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 10:35:07','root','Logged in ..'),
 ('2016-11-26 10:35:07','root','Form - setup Completed..'),
 ('2016-11-26 10:35:31','root','Entering More Analytics..'),
 ('2016-11-26 10:36:02','root','Entering View Reports..'),
 ('2016-11-26 10:36:28','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 10:36:41','amocuser','UnAuthorized access 0'),
 ('2016-11-26 10:36:58','amco','UnAuthorized access 1'),
 ('2016-11-26 10:37:05','amco','Logged in ..'),
 ('2016-11-26 10:37:05','amco','Form - setup Completed..'),
 ('2016-11-26 10:37:47','amco','Entering More Analytics..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 10:38:21','amco','Entering View Reports..'),
 ('2016-11-26 10:38:37','amco','Entering View Reports..'),
 ('2016-11-26 11:02:06','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 11:02:13','root','Logged in ..'),
 ('2016-11-26 11:04:46','root','AMCO -non-registered employees( new Users) Signing up with BEINGE..'),
 ('2016-11-26 11:05:30','root','entered Sign up Page. for 710140.thiyagarajan@amco.com'),
 ('2016-11-26 11:05:44','root','Sign up of 710140.thiyagarajan@amco.com is successfully DONE..'),
 ('2016-11-26 11:05:44','root','entered Log in Page..for.710140'),
 ('2016-11-26 11:05:45','root','This Person ID received from Beinge is 3265'),
 ('2016-11-26 11:06:02','root','entered Sign up Page. for 710135.loganathan@amco.com'),
 ('2016-11-26 11:06:03','root','Sign up of 710135.loganathan@amco.com is successfully DONE..'),
 ('2016-11-26 11:06:03','root','entered Log in Page..for.710135'),
 ('2016-11-26 11:06:04','root','This Person ID received from Beinge is 3266'),
 ('2016-11-26 11:06:09','root','entered Sign up Page. for 710222.raja@amco.com');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:06:11','root','Sign up of 710222.raja@amco.com is successfully DONE..'),
 ('2016-11-26 11:06:11','root','entered Log in Page..for.710222'),
 ('2016-11-26 11:06:12','root','This Person ID received from Beinge is 3267'),
 ('2016-11-26 11:06:28','root','entered Sign up Page. for 710120.ananthan@amco.com'),
 ('2016-11-26 11:06:31','root','Sign up of 710120.ananthan@amco.com is successfully DONE..'),
 ('2016-11-26 11:06:31','root','entered Log in Page..for.710120'),
 ('2016-11-26 11:06:31','root','This Person ID received from Beinge is 3268'),
 ('2016-11-26 11:07:37','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 11:07:42','root','Logged in ..'),
 ('2016-11-26 11:07:48','root','AMCO -non-registered employees( new Users) Signing up with BEINGE..'),
 ('2016-11-26 11:07:48','root','entered Sign up Page. for 710042.ramesh@amco.com'),
 ('2016-11-26 11:07:52','root','Sign up of 710042.ramesh@amco.com is successfully DONE..'),
 ('2016-11-26 11:07:52','root','entered Log in Page..for.710042');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:07:52','root','This Person ID received from Beinge is 3269'),
 ('2016-11-26 11:07:58','root','entered Sign up Page. for 710197.mohammedrabeek@amco.com'),
 ('2016-11-26 11:08:00','root','Sign up of 710197.mohammedrabeek@amco.com is successfully DONE..'),
 ('2016-11-26 11:08:00','root','entered Log in Page..for.710197'),
 ('2016-11-26 11:08:00','root','This Person ID received from Beinge is 3270'),
 ('2016-11-26 11:08:04','root','entered Sign up Page. for 710230.silambarasan@amco.com'),
 ('2016-11-26 11:08:05','root','Sign up of 710230.silambarasan@amco.com is successfully DONE..'),
 ('2016-11-26 11:08:05','root','entered Log in Page..for.710230'),
 ('2016-11-26 11:08:05','root','This Person ID received from Beinge is 3271'),
 ('2016-11-26 11:08:09','root','entered Sign up Page. for 710244.rameshkumar@amco.com'),
 ('2016-11-26 11:08:10','root','Sign up of 710244.rameshkumar@amco.com is successfully DONE..'),
 ('2016-11-26 11:08:10','root','entered Log in Page..for.710244');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:08:11','root','This Person ID received from Beinge is 3272'),
 ('2016-11-26 11:08:15','root','entered Sign up Page. for 710254.anandharajan@amco.com'),
 ('2016-11-26 11:08:16','root','Sign up of 710254.anandharajan@amco.com is successfully DONE..'),
 ('2016-11-26 11:08:16','root','entered Log in Page..for.710254'),
 ('2016-11-26 11:08:16','root','This Person ID received from Beinge is 3273'),
 ('2016-11-26 11:08:20','root','entered Sign up Page. for 710079.ramkumar@amco.com'),
 ('2016-11-26 11:08:21','root','Sign up of 710079.ramkumar@amco.com is successfully DONE..'),
 ('2016-11-26 11:08:21','root','entered Log in Page..for.710079'),
 ('2016-11-26 11:08:21','root','This Person ID received from Beinge is 3274'),
 ('2016-11-26 11:08:25','root','entered Sign up Page. for 710202.manavalan@amco.com'),
 ('2016-11-26 11:08:26','root','Sign up of 710202.manavalan@amco.com is successfully DONE..'),
 ('2016-11-26 11:08:26','root','entered Log in Page..for.710202'),
 ('2016-11-26 11:08:26','root','This Person ID received from Beinge is 3275');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:08:30','root','entered Sign up Page. for 710192.vaithiyanathan@amco.com'),
 ('2016-11-26 11:08:31','root','Sign up of 710192.vaithiyanathan@amco.com is successfully DONE..'),
 ('2016-11-26 11:08:31','root','entered Log in Page..for.710192'),
 ('2016-11-26 11:08:32','root','This Person ID received from Beinge is 3276'),
 ('2016-11-26 11:08:35','root','entered Sign up Page. for 5.ramar@amco.com'),
 ('2016-11-26 11:08:36','root','Sign up of 5.ramar@amco.com is successfully DONE..'),
 ('2016-11-26 11:08:36','root','entered Log in Page..for.5'),
 ('2016-11-26 11:08:37','root','This Person ID received from Beinge is 3277'),
 ('2016-11-26 11:08:59','root','entered Sign up Page. for 9.muthukrishnan@amco.com'),
 ('2016-11-26 11:09:00','root','Sign up of 9.muthukrishnan@amco.com is successfully DONE..'),
 ('2016-11-26 11:09:01','root','entered Log in Page..for.9'),
 ('2016-11-26 11:09:01','root','This Person ID received from Beinge is 3278'),
 ('2016-11-26 11:09:06','root','entered Sign up Page. for 12.micheal@amco.com');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:09:07','root','Sign up of 12.micheal@amco.com is successfully DONE..'),
 ('2016-11-26 11:09:07','root','entered Log in Page..for.12'),
 ('2016-11-26 11:09:07','root','This Person ID received from Beinge is 3279'),
 ('2016-11-26 11:09:12','root','entered Sign up Page. for 13.balamuthukumar@amco.com'),
 ('2016-11-26 11:09:14','root','Sign up of 13.balamuthukumar@amco.com is successfully DONE..'),
 ('2016-11-26 11:09:14','root','entered Log in Page..for.13'),
 ('2016-11-26 11:09:14','root','This Person ID received from Beinge is 3280'),
 ('2016-11-26 11:09:18','root','entered Sign up Page. for 14.veerapandian@amco.com'),
 ('2016-11-26 11:09:20','root','Sign up of 14.veerapandian@amco.com is successfully DONE..'),
 ('2016-11-26 11:09:20','root','entered Log in Page..for.14'),
 ('2016-11-26 11:09:20','root','This Person ID received from Beinge is 3281'),
 ('2016-11-26 11:09:24','root','entered Sign up Page. for 15.mariappan@amco.com'),
 ('2016-11-26 11:09:25','root','Sign up of 15.mariappan@amco.com is successfully DONE..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:09:25','root','entered Log in Page..for.15'),
 ('2016-11-26 11:09:25','root','This Person ID received from Beinge is 3282'),
 ('2016-11-26 11:09:29','root','entered Sign up Page. for 21.shanmugasundaran@amco.com'),
 ('2016-11-26 11:09:30','root','Sign up of 21.shanmugasundaran@amco.com is successfully DONE..'),
 ('2016-11-26 11:09:30','root','entered Log in Page..for.21'),
 ('2016-11-26 11:09:30','root','This Person ID received from Beinge is 3283'),
 ('2016-11-26 11:09:34','root','entered Sign up Page. for 22.bhoovaragamoorthy@amco.com'),
 ('2016-11-26 11:09:35','root','Sign up of 22.bhoovaragamoorthy@amco.com is successfully DONE..'),
 ('2016-11-26 11:09:35','root','entered Log in Page..for.22'),
 ('2016-11-26 11:09:36','root','This Person ID received from Beinge is 3284'),
 ('2016-11-26 11:09:43','root','entered Sign up Page. for 17.ramalingam@amco.com'),
 ('2016-11-26 11:09:44','root','Sign up of 17.ramalingam@amco.com is successfully DONE..'),
 ('2016-11-26 11:09:44','root','entered Log in Page..for.17');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:09:44','root','This Person ID received from Beinge is 3285'),
 ('2016-11-26 11:15:43','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 11:15:49','root','Logged in ..'),
 ('2016-11-26 11:15:52','root','AMCO -non-registered employees( new Users) Signing up with BEINGE..'),
 ('2016-11-26 11:15:52','root','entered Sign up Page. for 26.kalyanasundarama@amco.com'),
 ('2016-11-26 11:15:56','root','Sign up of 26.kalyanasundarama@amco.com is successfully DONE..'),
 ('2016-11-26 11:15:56','root','entered Log in Page..for.26'),
 ('2016-11-26 11:15:57','root','This Person ID received from Beinge is 3286'),
 ('2016-11-26 11:15:58','root','entered Sign up Page. for 30.murugan@amco.com'),
 ('2016-11-26 11:15:59','root','Sign up of 30.murugan@amco.com is successfully DONE..'),
 ('2016-11-26 11:15:59','root','entered Log in Page..for.30'),
 ('2016-11-26 11:16:00','root','This Person ID received from Beinge is 3287'),
 ('2016-11-26 11:16:01','root','entered Sign up Page. for 31.murugan@amco.com');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:16:02','root','Sign up of 31.murugan@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:02','root','entered Log in Page..for.31'),
 ('2016-11-26 11:16:02','root','This Person ID received from Beinge is 3288'),
 ('2016-11-26 11:16:03','root','entered Sign up Page. for 42.balaji@amco.com'),
 ('2016-11-26 11:16:04','root','Sign up of 42.balaji@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:04','root','entered Log in Page..for.42'),
 ('2016-11-26 11:16:05','root','This Person ID received from Beinge is 3289'),
 ('2016-11-26 11:16:05','root','entered Sign up Page. for 24.nagahariramasubramanian@amco.com'),
 ('2016-11-26 11:16:06','root','Sign up of 24.nagahariramasubramanian@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:06','root','entered Log in Page..for.24'),
 ('2016-11-26 11:16:07','root','This Person ID received from Beinge is 3290'),
 ('2016-11-26 11:16:07','root','entered Sign up Page. for 45.jayaprakash@amco.com'),
 ('2016-11-26 11:16:09','root','Sign up of 45.jayaprakash@amco.com is successfully DONE..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:16:09','root','entered Log in Page..for.45'),
 ('2016-11-26 11:16:09','root','This Person ID received from Beinge is 3291'),
 ('2016-11-26 11:16:10','root','entered Sign up Page. for 47.murugesan@amco.com'),
 ('2016-11-26 11:16:11','root','Sign up of 47.murugesan@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:11','root','entered Log in Page..for.47'),
 ('2016-11-26 11:16:11','root','This Person ID received from Beinge is 3292'),
 ('2016-11-26 11:16:12','root','entered Sign up Page. for 52.indirathi@amco.com'),
 ('2016-11-26 11:16:14','root','Sign up of 52.indirathi@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:14','root','entered Log in Page..for.52'),
 ('2016-11-26 11:16:14','root','This Person ID received from Beinge is 3293'),
 ('2016-11-26 11:16:15','root','entered Sign up Page. for 53.ramesh@amco.com'),
 ('2016-11-26 11:16:16','root','Sign up of 53.ramesh@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:16','root','entered Log in Page..for.53');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:16:16','root','This Person ID received from Beinge is 3294'),
 ('2016-11-26 11:16:17','root','entered Sign up Page. for 60.seetharaman@amco.com'),
 ('2016-11-26 11:16:18','root','Sign up of 60.seetharaman@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:18','root','entered Log in Page..for.60'),
 ('2016-11-26 11:16:19','root','This Person ID received from Beinge is 3295'),
 ('2016-11-26 11:16:19','root','entered Sign up Page. for 61.karupppasamy@amco.com'),
 ('2016-11-26 11:16:20','root','Sign up of 61.karupppasamy@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:20','root','entered Log in Page..for.61'),
 ('2016-11-26 11:16:21','root','This Person ID received from Beinge is 3296'),
 ('2016-11-26 11:16:21','root','entered Sign up Page. for 75.paranjothi@amco.com'),
 ('2016-11-26 11:16:22','root','Sign up of 75.paranjothi@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:23','root','entered Log in Page..for.75'),
 ('2016-11-26 11:16:23','root','This Person ID received from Beinge is 3297');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:16:24','root','entered Sign up Page. for 76.shanmugananthkrishnan@amco.com'),
 ('2016-11-26 11:16:25','root','Sign up of 76.shanmugananthkrishnan@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:25','root','entered Log in Page..for.76'),
 ('2016-11-26 11:16:25','root','This Person ID received from Beinge is 3298'),
 ('2016-11-26 11:16:26','root','entered Sign up Page. for 79.senthilvel@amco.com'),
 ('2016-11-26 11:16:27','root','Sign up of 79.senthilvel@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:27','root','entered Log in Page..for.79'),
 ('2016-11-26 11:16:28','root','This Person ID received from Beinge is 3299'),
 ('2016-11-26 11:16:29','root','entered Sign up Page. for 84.murugensan@amco.com'),
 ('2016-11-26 11:16:31','root','Sign up of 84.murugensan@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:31','root','entered Log in Page..for.84'),
 ('2016-11-26 11:16:31','root','This Person ID received from Beinge is 3300'),
 ('2016-11-26 11:16:32','root','entered Sign up Page. for 88.sureshkumar@amco.com');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:16:33','root','Sign up of 88.sureshkumar@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:33','root','entered Log in Page..for.88'),
 ('2016-11-26 11:16:33','root','This Person ID received from Beinge is 3301'),
 ('2016-11-26 11:16:34','root','entered Sign up Page. for 89.sankaran@amco.com'),
 ('2016-11-26 11:16:35','root','Sign up of 89.sankaran@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:35','root','entered Log in Page..for.89'),
 ('2016-11-26 11:16:35','root','This Person ID received from Beinge is 3302'),
 ('2016-11-26 11:16:36','root','entered Sign up Page. for 90.thirunavukarasu@amco.com'),
 ('2016-11-26 11:16:37','root','Sign up of 90.thirunavukarasu@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:37','root','entered Log in Page..for.90'),
 ('2016-11-26 11:16:38','root','This Person ID received from Beinge is 3303'),
 ('2016-11-26 11:16:38','root','entered Sign up Page. for 91.senthilvel@amco.com'),
 ('2016-11-26 11:16:39','root','Sign up of 91.senthilvel@amco.com is successfully DONE..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:16:40','root','entered Log in Page..for.91'),
 ('2016-11-26 11:16:40','root','This Person ID received from Beinge is 3304'),
 ('2016-11-26 11:16:41','root','entered Sign up Page. for 96.thirumalai@amco.com'),
 ('2016-11-26 11:16:42','root','Sign up of 96.thirumalai@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:42','root','entered Log in Page..for.96'),
 ('2016-11-26 11:16:42','root','This Person ID received from Beinge is 3305'),
 ('2016-11-26 11:16:43','root','entered Sign up Page. for 99.nagarajan@amco.com'),
 ('2016-11-26 11:16:44','root','Sign up of 99.nagarajan@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:44','root','entered Log in Page..for.99'),
 ('2016-11-26 11:16:45','root','This Person ID received from Beinge is 3306'),
 ('2016-11-26 11:16:46','root','entered Sign up Page. for 105.sureshkumar@amco.com'),
 ('2016-11-26 11:16:47','root','Sign up of 105.sureshkumar@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:47','root','entered Log in Page..for.105');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:16:47','root','This Person ID received from Beinge is 3307'),
 ('2016-11-26 11:16:48','root','entered Sign up Page. for 107.sundar@amco.com'),
 ('2016-11-26 11:16:49','root','Sign up of 107.sundar@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:49','root','entered Log in Page..for.107'),
 ('2016-11-26 11:16:49','root','This Person ID received from Beinge is 3308'),
 ('2016-11-26 11:16:50','root','entered Sign up Page. for 153.suryanarayanan@amco.com'),
 ('2016-11-26 11:16:51','root','Sign up of 153.suryanarayanan@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:51','root','entered Log in Page..for.153'),
 ('2016-11-26 11:16:51','root','This Person ID received from Beinge is 3309'),
 ('2016-11-26 11:16:52','root','entered Sign up Page. for 95.ashokkumar@amco.com'),
 ('2016-11-26 11:16:53','root','Sign up of 95.ashokkumar@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:53','root','entered Log in Page..for.95'),
 ('2016-11-26 11:16:53','root','This Person ID received from Beinge is 3310');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:16:54','root','entered Sign up Page. for 101.haribabu@amco.com'),
 ('2016-11-26 11:16:55','root','Sign up of 101.haribabu@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:55','root','entered Log in Page..for.101'),
 ('2016-11-26 11:16:55','root','This Person ID received from Beinge is 3311'),
 ('2016-11-26 11:16:56','root','entered Sign up Page. for 109.murali@amco.com'),
 ('2016-11-26 11:16:58','root','Sign up of 109.murali@amco.com is successfully DONE..'),
 ('2016-11-26 11:16:58','root','entered Log in Page..for.109'),
 ('2016-11-26 11:16:59','root','This Person ID received from Beinge is 3312'),
 ('2016-11-26 11:16:59','root','entered Sign up Page. for 110.muthuraj@amco.com'),
 ('2016-11-26 11:17:00','root','Sign up of 110.muthuraj@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:00','root','entered Log in Page..for.110'),
 ('2016-11-26 11:17:01','root','This Person ID received from Beinge is 3313'),
 ('2016-11-26 11:17:01','root','entered Sign up Page. for 111.sivakumar@amco.com');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:17:02','root','Sign up of 111.sivakumar@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:03','root','entered Log in Page..for.111'),
 ('2016-11-26 11:17:03','root','This Person ID received from Beinge is 3314'),
 ('2016-11-26 11:17:04','root','entered Sign up Page. for 121.kothandaraman@amco.com'),
 ('2016-11-26 11:17:05','root','Sign up of 121.kothandaraman@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:05','root','entered Log in Page..for.121'),
 ('2016-11-26 11:17:05','root','This Person ID received from Beinge is 3315'),
 ('2016-11-26 11:17:06','root','entered Sign up Page. for 98.anantharamakirshnan@amco.com'),
 ('2016-11-26 11:17:07','root','Sign up of 98.anantharamakirshnan@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:07','root','entered Log in Page..for.98'),
 ('2016-11-26 11:17:07','root','This Person ID received from Beinge is 3316'),
 ('2016-11-26 11:17:08','root','entered Sign up Page. for 113.gopal@amco.com'),
 ('2016-11-26 11:17:09','root','Sign up of 113.gopal@amco.com is successfully DONE..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:17:09','root','entered Log in Page..for.113'),
 ('2016-11-26 11:17:10','root','This Person ID received from Beinge is 3317'),
 ('2016-11-26 11:17:10','root','entered Sign up Page. for 118.krishnan@amco.com'),
 ('2016-11-26 11:17:12','root','Sign up of 118.krishnan@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:12','root','entered Log in Page..for.118'),
 ('2016-11-26 11:17:12','root','This Person ID received from Beinge is 3318'),
 ('2016-11-26 11:17:13','root','entered Sign up Page. for 122.velumalai@amco.com'),
 ('2016-11-26 11:17:14','root','Sign up of 122.velumalai@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:14','root','entered Log in Page..for.122'),
 ('2016-11-26 11:17:14','root','This Person ID received from Beinge is 3319'),
 ('2016-11-26 11:17:15','root','entered Sign up Page. for 125.meganatha@amco.com'),
 ('2016-11-26 11:17:16','root','Sign up of 125.meganatha@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:16','root','entered Log in Page..for.125');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:17:17','root','This Person ID received from Beinge is 3320'),
 ('2016-11-26 11:17:18','root','entered Sign up Page. for 163.saravanan@amco.com'),
 ('2016-11-26 11:17:19','root','Sign up of 163.saravanan@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:19','root','entered Log in Page..for.163'),
 ('2016-11-26 11:17:19','root','This Person ID received from Beinge is 3321'),
 ('2016-11-26 11:17:20','root','entered Sign up Page. for 165.velayudham@amco.com'),
 ('2016-11-26 11:17:21','root','Sign up of 165.velayudham@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:21','root','entered Log in Page..for.165'),
 ('2016-11-26 11:17:21','root','This Person ID received from Beinge is 3322'),
 ('2016-11-26 11:17:22','root','entered Sign up Page. for 123.krishnan@amco.com'),
 ('2016-11-26 11:17:23','root','Sign up of 123.krishnan@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:23','root','entered Log in Page..for.123'),
 ('2016-11-26 11:17:23','root','This Person ID received from Beinge is 3323');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:17:24','root','entered Sign up Page. for 124.munian@amco.com'),
 ('2016-11-26 11:17:25','root','Sign up of 124.munian@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:25','root','entered Log in Page..for.124'),
 ('2016-11-26 11:17:25','root','This Person ID received from Beinge is 3324'),
 ('2016-11-26 11:17:26','root','entered Sign up Page. for 133.panneerselvan@amco.com'),
 ('2016-11-26 11:17:27','root','Sign up of 133.panneerselvan@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:28','root','entered Log in Page..for.133'),
 ('2016-11-26 11:17:28','root','This Person ID received from Beinge is 3325'),
 ('2016-11-26 11:17:28','root','entered Sign up Page. for 141.raja@amco.com'),
 ('2016-11-26 11:17:29','root','Sign up of 141.raja@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:30','root','entered Log in Page..for.141'),
 ('2016-11-26 11:17:30','root','This Person ID received from Beinge is 3326'),
 ('2016-11-26 11:17:31','root','entered Sign up Page. for 142.ravichandran@amco.com');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:17:32','root','Sign up of 142.ravichandran@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:32','root','entered Log in Page..for.142'),
 ('2016-11-26 11:17:32','root','This Person ID received from Beinge is 3327'),
 ('2016-11-26 11:17:33','root','entered Sign up Page. for 146.dharman@amco.com'),
 ('2016-11-26 11:17:34','root','Sign up of 146.dharman@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:34','root','entered Log in Page..for.146'),
 ('2016-11-26 11:17:35','root','This Person ID received from Beinge is 3328'),
 ('2016-11-26 11:17:35','root','entered Sign up Page. for 167.selvakumar@amco.com'),
 ('2016-11-26 11:17:36','root','Sign up of 167.selvakumar@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:36','root','entered Log in Page..for.167'),
 ('2016-11-26 11:17:37','root','This Person ID received from Beinge is 3329'),
 ('2016-11-26 11:17:37','root','entered Sign up Page. for 169.johnson@amco.com'),
 ('2016-11-26 11:17:38','root','Sign up of 169.johnson@amco.com is successfully DONE..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:17:38','root','entered Log in Page..for.169'),
 ('2016-11-26 11:17:39','root','This Person ID received from Beinge is 3330'),
 ('2016-11-26 11:17:39','root','entered Sign up Page. for 37.selvamuthukumar@amco.com'),
 ('2016-11-26 11:17:40','root','Sign up of 37.selvamuthukumar@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:40','root','entered Log in Page..for.37'),
 ('2016-11-26 11:17:41','root','This Person ID received from Beinge is 3331'),
 ('2016-11-26 11:17:42','root','entered Sign up Page. for 135.nagarajan@amco.com'),
 ('2016-11-26 11:17:43','root','Sign up of 135.nagarajan@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:43','root','entered Log in Page..for.135'),
 ('2016-11-26 11:17:43','root','This Person ID received from Beinge is 3332'),
 ('2016-11-26 11:17:44','root','entered Sign up Page. for 143.mohan@amco.com'),
 ('2016-11-26 11:17:45','root','Sign up of 143.mohan@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:45','root','entered Log in Page..for.143');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:17:45','root','This Person ID received from Beinge is 3333'),
 ('2016-11-26 11:17:46','root','entered Sign up Page. for 162.devadoss@amco.com'),
 ('2016-11-26 11:17:47','root','Sign up of 162.devadoss@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:47','root','entered Log in Page..for.162'),
 ('2016-11-26 11:17:48','root','This Person ID received from Beinge is 3334'),
 ('2016-11-26 11:17:48','root','entered Sign up Page. for 164.rajkumar@amco.com'),
 ('2016-11-26 11:17:49','root','Sign up of 164.rajkumar@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:50','root','entered Log in Page..for.164'),
 ('2016-11-26 11:17:50','root','This Person ID received from Beinge is 3335'),
 ('2016-11-26 11:17:51','root','entered Sign up Page. for 168.rajendran@amco.com'),
 ('2016-11-26 11:17:52','root','Sign up of 168.rajendran@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:52','root','entered Log in Page..for.168'),
 ('2016-11-26 11:17:52','root','This Person ID received from Beinge is 3336');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:17:53','root','entered Sign up Page. for 171.kannan@amco.com'),
 ('2016-11-26 11:17:53','root','Sign up of 171.kannan@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:53','root','entered Log in Page..for.171'),
 ('2016-11-26 11:17:54','root','This Person ID received from Beinge is 3337'),
 ('2016-11-26 11:17:55','root','entered Sign up Page. for 173.prakash@amco.com'),
 ('2016-11-26 11:17:55','root','Sign up of 173.prakash@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:56','root','entered Log in Page..for.173'),
 ('2016-11-26 11:17:56','root','This Person ID received from Beinge is 3338'),
 ('2016-11-26 11:17:57','root','entered Sign up Page. for 5003.saravanan@amco.com'),
 ('2016-11-26 11:17:58','root','Sign up of 5003.saravanan@amco.com is successfully DONE..'),
 ('2016-11-26 11:17:59','root','entered Log in Page..for.5003'),
 ('2016-11-26 11:17:59','root','This Person ID received from Beinge is 3339'),
 ('2016-11-26 11:17:59','root','entered Sign up Page. for 710216.surendran@amco.com');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:18:01','root','Sign up of 710216.surendran@amco.com is successfully DONE..'),
 ('2016-11-26 11:18:01','root','entered Log in Page..for.710216'),
 ('2016-11-26 11:18:01','root','This Person ID received from Beinge is 3340'),
 ('2016-11-26 11:18:02','root','entered Sign up Page. for 710231.haridoss@amco.com'),
 ('2016-11-26 11:18:03','root','Sign up of 710231.haridoss@amco.com is successfully DONE..'),
 ('2016-11-26 11:18:03','root','entered Log in Page..for.710231'),
 ('2016-11-26 11:18:03','root','This Person ID received from Beinge is 3341'),
 ('2016-11-26 11:18:04','root','entered Sign up Page. for 710095.janarthanan@amco.com'),
 ('2016-11-26 11:18:05','root','Sign up of 710095.janarthanan@amco.com is successfully DONE..'),
 ('2016-11-26 11:18:05','root','entered Log in Page..for.710095'),
 ('2016-11-26 11:18:05','root','This Person ID received from Beinge is 3342'),
 ('2016-11-26 11:18:06','root','entered Sign up Page. for 710139.magesh@amco.com'),
 ('2016-11-26 11:18:07','root','Sign up of 710139.magesh@amco.com is successfully DONE..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:18:07','root','entered Log in Page..for.710139'),
 ('2016-11-26 11:18:07','root','This Person ID received from Beinge is 3343'),
 ('2016-11-26 11:18:08','root','entered Sign up Page. for 6001.ragunathan@amco.com'),
 ('2016-11-26 11:18:09','root','Sign up of 6001.ragunathan@amco.com is successfully DONE..'),
 ('2016-11-26 11:18:09','root','entered Log in Page..for.6001'),
 ('2016-11-26 11:18:10','root','This Person ID received from Beinge is 3344'),
 ('2016-11-26 11:18:10','root','entered Sign up Page. for 710048.arunachalam@amco.com'),
 ('2016-11-26 11:18:12','root','Sign up of 710048.arunachalam@amco.com is successfully DONE..'),
 ('2016-11-26 11:18:12','root','entered Log in Page..for.710048'),
 ('2016-11-26 11:18:12','root','This Person ID received from Beinge is 3345'),
 ('2016-11-26 11:18:13','root','entered Sign up Page. for 710087.gokulakrishnan@amco.com'),
 ('2016-11-26 11:18:14','root','Sign up of 710087.gokulakrishnan@amco.com is successfully DONE..'),
 ('2016-11-26 11:18:14','root','entered Log in Page..for.710087');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:18:14','root','This Person ID received from Beinge is 3346'),
 ('2016-11-26 11:19:29','root','Successfully registered with BEINGE.. ( Health on your HAND)  61 out of 61'),
 ('2016-11-26 11:19:29','root','Form - setup Completed..'),
 ('2016-11-26 11:22:26','root','Entering View Reports..'),
 ('2016-11-26 11:48:26','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 11:48:32','root','Logged in ..'),
 ('2016-11-26 11:48:36','root','Form - setup Completed..'),
 ('2016-11-26 11:52:36','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 11:52:45','','UnAuthorized access 0'),
 ('2016-11-26 11:52:48','','UnAuthorized access 1'),
 ('2016-11-26 11:52:50','','UnAuthorized access 2'),
 ('2016-11-26 11:52:52','','UnAuthorized access'),
 ('2016-11-26 11:54:29','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 11:54:34','root','Logged in ..'),
 ('2016-11-26 11:54:39','root','Form - setup Completed..'),
 ('2016-11-26 11:59:20','root','entered Sign up Page. for 710137.jaganathan@amco.com');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:59:32','root','Sign up of 710137.jaganathan@amco.com is successfully DONE..'),
 ('2016-11-26 11:59:32','root','entered Log in Page..for.710137'),
 ('2016-11-26 11:59:33','root','This Person ID received from Beinge is 3347'),
 ('2016-11-26 11:59:36','root','entered Sign up Page. for 710136.thirumal@amco.com'),
 ('2016-11-26 11:59:37','root','Sign up of 710136.thirumal@amco.com is successfully DONE..'),
 ('2016-11-26 11:59:37','root','entered Log in Page..for.710136'),
 ('2016-11-26 11:59:38','root','This Person ID received from Beinge is 3348'),
 ('2016-11-26 11:59:38','root','entered Sign up Page. for 710094.kaldoss@amco.com'),
 ('2016-11-26 11:59:39','root','Sign up of 710094.kaldoss@amco.com is successfully DONE..'),
 ('2016-11-26 11:59:39','root','entered Log in Page..for.710094'),
 ('2016-11-26 11:59:40','root','This Person ID received from Beinge is 3349'),
 ('2016-11-26 11:59:40','root','entered Sign up Page. for 710044.sadiqbhasa@amco.com'),
 ('2016-11-26 11:59:41','root','Sign up of 710044.sadiqbhasa@amco.com is successfully DONE..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:59:41','root','entered Log in Page..for.710044'),
 ('2016-11-26 11:59:42','root','This Person ID received from Beinge is 3350'),
 ('2016-11-26 11:59:42','root','entered Sign up Page. for 710123.sankar@amco.com'),
 ('2016-11-26 11:59:43','root','Sign up of 710123.sankar@amco.com is successfully DONE..'),
 ('2016-11-26 11:59:43','root','entered Log in Page..for.710123'),
 ('2016-11-26 11:59:43','root','This Person ID received from Beinge is 3351'),
 ('2016-11-26 11:59:44','root','entered Sign up Page. for 710127.narendiran@amco.com'),
 ('2016-11-26 11:59:44','root','Sign up of 710127.narendiran@amco.com is successfully DONE..'),
 ('2016-11-26 11:59:45','root','entered Log in Page..for.710127'),
 ('2016-11-26 11:59:45','root','This Person ID received from Beinge is 3352'),
 ('2016-11-26 11:59:45','root','entered Sign up Page. for 710236.mohankumar@amco.com'),
 ('2016-11-26 11:59:46','root','Sign up of 710236.mohankumar@amco.com is successfully DONE..'),
 ('2016-11-26 11:59:46','root','entered Log in Page..for.710236');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:59:46','root','This Person ID received from Beinge is 3353'),
 ('2016-11-26 11:59:46','root','entered Sign up Page. for 710049.anantharaj@maco.com'),
 ('2016-11-26 11:59:47','root','Sign up of 710049.anantharaj@maco.com is successfully DONE..'),
 ('2016-11-26 11:59:47','root','entered Log in Page..for.710049'),
 ('2016-11-26 11:59:47','root','This Person ID received from Beinge is 3354'),
 ('2016-11-26 11:59:48','root','entered Sign up Page. for 710204.palanivel@amco.com'),
 ('2016-11-26 11:59:48','root','Sign up of 710204.palanivel@amco.com is successfully DONE..'),
 ('2016-11-26 11:59:48','root','entered Log in Page..for.710204'),
 ('2016-11-26 11:59:49','root','This Person ID received from Beinge is 3355'),
 ('2016-11-26 11:59:49','root','entered Sign up Page. for 710178.naghulsamy@amco.com'),
 ('2016-11-26 11:59:49','root','Sign up of 710178.naghulsamy@amco.com is successfully DONE..'),
 ('2016-11-26 11:59:50','root','entered Log in Page..for.710178'),
 ('2016-11-26 11:59:50','root','This Person ID received from Beinge is 3356');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 11:59:50','root','entered Sign up Page. for 710220.sathiyaseelan@maco.com'),
 ('2016-11-26 11:59:51','root','Sign up of 710220.sathiyaseelan@maco.com is successfully DONE..'),
 ('2016-11-26 11:59:51','root','entered Log in Page..for.710220'),
 ('2016-11-26 11:59:51','root','This Person ID received from Beinge is 3357'),
 ('2016-11-26 11:59:52','root','entered Sign up Page. for 710024.gopu@amco.com'),
 ('2016-11-26 11:59:54','root','Sign up of 710024.gopu@amco.com is successfully DONE..'),
 ('2016-11-26 11:59:54','root','entered Log in Page..for.710024'),
 ('2016-11-26 11:59:55','root','This Person ID received from Beinge is 3358'),
 ('2016-11-26 11:59:55','root','entered Sign up Page. for 710187.krishna@amco.com'),
 ('2016-11-26 11:59:57','root','Sign up of 710187.krishna@amco.com is successfully DONE..'),
 ('2016-11-26 11:59:57','root','entered Log in Page..for.710187'),
 ('2016-11-26 11:59:57','root','This Person ID received from Beinge is 3359'),
 ('2016-11-26 11:59:58','root','entered Sign up Page. for 710066.sathishkumar@amco.com');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 12:00:00','root','Sign up of 710066.sathishkumar@amco.com is successfully DONE..'),
 ('2016-11-26 12:00:00','root','entered Log in Page..for.710066'),
 ('2016-11-26 12:00:01','root','This Person ID received from Beinge is 3360'),
 ('2016-11-26 12:07:18','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_BLL_Data_v2.0.xlsx'),
 ('2016-11-26 12:15:26','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 12:15:37','root','Logged in ..'),
 ('2016-11-26 12:15:51','root','Form - setup Completed..'),
 ('2016-11-26 12:16:18','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_BLL_Data_v2.0.xlsx'),
 ('2016-11-26 12:16:58','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 12:17:08','root','Logged in ..'),
 ('2016-11-26 12:17:11','root','Form - setup Completed..'),
 ('2016-11-26 12:17:33','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_BLL_Data_v2.0.xlsx'),
 ('2016-11-26 12:19:55','root','Amco Benge OHS Applicatino Started..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 12:20:10','root','Logged in ..'),
 ('2016-11-26 12:20:12','root','Form - setup Completed..'),
 ('2016-11-26 12:20:27','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_HR_v3.0.xlsx'),
 ('2016-11-26 12:20:30','root','File is not in the BLL format, kindly check the format for BLL..'),
 ('2016-11-26 12:23:48','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 12:23:54','root','Logged in ..'),
 ('2016-11-26 12:23:54','root','Form - setup Completed..'),
 ('2016-11-26 12:24:08','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_HR_v3.0.xlsx'),
 ('2016-11-26 12:29:20','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 12:29:39','root','Logged in ..'),
 ('2016-11-26 12:29:39','root','Form - setup Completed..'),
 ('2016-11-26 12:29:57','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_HR_v3.0.xlsx'),
 ('2016-11-26 12:34:03','root','Amco Benge OHS Applicatino Started..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 12:34:09','root','Logged in ..'),
 ('2016-11-26 12:34:10','root','Form - setup Completed..'),
 ('2016-11-26 12:34:23','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_HR_v3.0.xlsx'),
 ('2016-11-26 12:36:10','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_HR_v3.0.xlsx'),
 ('2016-11-26 12:36:30','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_HR_v3.0.xlsx'),
 ('2016-11-26 12:37:23','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 12:37:29','root','Logged in ..'),
 ('2016-11-26 12:37:29','root','Form - setup Completed..'),
 ('2016-11-26 12:37:44','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_HR_v3.0.xlsx'),
 ('2016-11-26 12:39:35','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 12:39:41','root','Logged in ..'),
 ('2016-11-26 12:39:41','root','Form - setup Completed..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 12:39:53','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_HR_v3.0.xlsx'),
 ('2016-11-26 12:40:42','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 12:41:04','root','Logged in ..'),
 ('2016-11-26 12:41:04','root','Form - setup Completed..'),
 ('2016-11-26 12:41:25','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_BLL_Data_v2.0.xlsx'),
 ('2016-11-26 12:43:42','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 12:43:50','root','Logged in ..'),
 ('2016-11-26 12:43:51','root','Form - setup Completed..'),
 ('2016-11-26 12:44:07','root','Opened XLS file for importing BLL data..F:LearningAmco_sampleAmco_sample_input_test_2AMCO_Test_HR_v2.0.xlsx'),
 ('2016-11-26 12:45:25','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 12:45:33','root','Logged in ..'),
 ('2016-11-26 12:45:34','root','Form - setup Completed..'),
 ('2016-11-26 12:46:32','root','Opened XLS file for importing BLL data..F:LearningAmco_WorkingAmco_sample_input_test_2BLL_Data_v2.0.xlsx');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 12:53:28','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 12:53:36','root','Logged in ..'),
 ('2016-11-26 12:53:37','root','Form - setup Completed..'),
 ('2016-11-26 12:53:50','root','Opened XLS file for importing BLL data..F:LearningAmco_WorkingAmco_sample_input_test_2BLL_Data_v2.0.xlsx'),
 ('2016-11-26 12:55:28','root','Entering More Analytics..'),
 ('2016-11-26 13:22:13','root','Entering More Analytics..'),
 ('2016-11-26 13:58:21','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 13:58:29','root','Logged in ..'),
 ('2016-11-26 13:58:29','root','Form - setup Completed..'),
 ('2016-11-26 14:01:34','root','Entering More Analytics..'),
 ('2016-11-26 14:48:32','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 14:48:41','root','Logged in ..'),
 ('2016-11-26 14:48:41','root','Form - setup Completed..'),
 ('2016-11-26 14:50:38','root','Entering More Analytics..'),
 ('2016-11-26 14:53:33','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 14:55:12','root','Logged in ..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 14:55:12','root','Form - setup Completed..'),
 ('2016-11-26 14:56:31','root','Entering More Analytics..'),
 ('2016-11-26 15:06:57','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 15:07:03','root','Logged in ..'),
 ('2016-11-26 15:07:04','root','Form - setup Completed..'),
 ('2016-11-26 15:08:31','root','Entering More Analytics..'),
 ('2016-11-26 15:11:38','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 15:11:44','root','Logged in ..'),
 ('2016-11-26 15:11:44','root','Form - setup Completed..'),
 ('2016-11-26 15:13:05','root','Entering More Analytics..'),
 ('2016-11-26 15:18:52','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 15:21:48','root','Logged in ..'),
 ('2016-11-26 15:21:49','root','Form - setup Completed..'),
 ('2016-11-26 15:25:34','root','Entering More Analytics..'),
 ('2016-11-26 15:38:08','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 15:38:14','','UnAuthorized access 0'),
 ('2016-11-26 15:38:17','','UnAuthorized access 1');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 15:38:20','','UnAuthorized access 2'),
 ('2016-11-26 15:38:21','','UnAuthorized access'),
 ('2016-11-26 16:10:44','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 16:10:52','root','Logged in ..'),
 ('2016-11-26 16:10:52','root','Form - setup Completed..'),
 ('2016-11-26 16:11:05','root','Entering View Reports..'),
 ('2016-11-26 16:11:24','root','Entering View Reports..'),
 ('2016-11-26 16:11:28','root','Entering View Reports..'),
 ('2016-11-26 16:11:32','root','Entering View Reports..'),
 ('2016-11-26 16:11:37','root','Entering View Reports..'),
 ('2016-11-26 16:11:40','root','Entering View Reports..'),
 ('2016-11-26 16:11:44','root','Entering View Reports..'),
 ('2016-11-26 16:11:55','root','Entering View Reports..'),
 ('2016-11-26 16:12:00','root','Entering View Reports..'),
 ('2016-11-26 16:13:07','root','Entering More Analytics..'),
 ('2016-11-26 16:19:36','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 16:19:44','root','Logged in ..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 16:19:45','root','Form - setup Completed..'),
 ('2016-11-26 16:21:02','root','Entering More Analytics..'),
 ('2016-11-26 16:22:23','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 16:22:30','root','Logged in ..'),
 ('2016-11-26 16:22:30','root','Form - setup Completed..'),
 ('2016-11-26 16:23:44','root','Entering More Analytics..'),
 ('2016-11-26 16:25:16','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 16:25:29','root','Logged in ..'),
 ('2016-11-26 16:25:29','root','Form - setup Completed..'),
 ('2016-11-26 16:26:39','root','Entering More Analytics..'),
 ('2016-11-26 16:55:50','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 16:55:59','root','Logged in ..'),
 ('2016-11-26 16:55:59','root','Form - setup Completed..'),
 ('2016-11-26 16:57:58','root','Entering More Analytics..'),
 ('2016-11-26 17:14:34','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 17:14:47','root','Logged in ..'),
 ('2016-11-26 17:14:48','root','Form - setup Completed..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 17:16:37','root','Entering More Analytics..'),
 ('2016-11-26 18:40:45','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 18:40:53','root','Logged in ..'),
 ('2016-11-26 18:40:54','root','Form - setup Completed..'),
 ('2016-11-26 18:41:56','root','Entering View Reports..'),
 ('2016-11-26 18:43:02','root','Entering More Analytics..'),
 ('2016-11-26 19:05:46','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 19:05:52','root','Logged in ..'),
 ('2016-11-26 19:05:53','root','Form - setup Completed..'),
 ('2016-11-26 19:06:26','root','Entering View Reports..'),
 ('2016-11-26 19:09:20','root','Entering View Reports..'),
 ('2016-11-26 19:18:05','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 19:18:12','root','Logged in ..'),
 ('2016-11-26 19:18:12','root','Form - setup Completed..'),
 ('2016-11-26 19:18:20','root','Entering View Reports..'),
 ('2016-11-26 19:19:42','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 19:19:49','root','Logged in ..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 19:19:49','root','Form - setup Completed..'),
 ('2016-11-26 19:19:59','root','Entering View Reports..'),
 ('2016-11-26 19:31:12','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 19:31:18','root','Logged in ..'),
 ('2016-11-26 19:31:19','root','Form - setup Completed..'),
 ('2016-11-26 19:31:28','root','Entering View Reports..'),
 ('2016-11-26 19:44:11','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 19:44:18','root','Logged in ..'),
 ('2016-11-26 19:44:18','root','Form - setup Completed..'),
 ('2016-11-26 19:44:23','root','Entering View Reports..'),
 ('2016-11-26 19:44:30','root','Entering View Reports..'),
 ('2016-11-26 19:44:44','root','Entering View Reports..'),
 ('2016-11-26 19:45:31','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 19:45:38','root','Logged in ..'),
 ('2016-11-26 19:45:38','root','Form - setup Completed..'),
 ('2016-11-26 19:45:46','root','Entering View Reports..'),
 ('2016-11-26 19:46:37','root','Amco Benge OHS Applicatino Started..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 19:46:46','root','Logged in ..'),
 ('2016-11-26 19:46:46','root','Form - setup Completed..'),
 ('2016-11-26 19:46:57','root','Entering View Reports..'),
 ('2016-11-26 19:47:03','root','Entering View Reports..'),
 ('2016-11-26 19:48:23','root','Entering View Reports..'),
 ('2016-11-26 19:49:06','root','Entering View Reports..'),
 ('2016-11-26 19:50:33','root','Entering View Reports..'),
 ('2016-11-26 19:50:45','root','Entering View Reports..'),
 ('2016-11-26 19:50:52','root','Entering View Reports..'),
 ('2016-11-26 19:51:17','root','Entering View Reports..'),
 ('2016-11-26 19:51:30','root','Entering View Reports..'),
 ('2016-11-26 19:51:59','root','Entering View Reports..'),
 ('2016-11-26 19:52:05','root','Entering View Reports..'),
 ('2016-11-26 20:15:40','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 20:15:47','root','Logged in ..'),
 ('2016-11-26 20:15:47','root','Form - setup Completed..'),
 ('2016-11-26 20:15:55','root','Entering View Reports..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 20:16:14','root','Entering View Reports..'),
 ('2016-11-26 20:16:24','root','Entering View Reports..'),
 ('2016-11-26 20:16:28','root','Entering View Reports..'),
 ('2016-11-26 20:18:39','root','Entering View Reports..'),
 ('2016-11-26 20:24:10','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 20:24:16','root','Logged in ..'),
 ('2016-11-26 20:24:17','root','Form - setup Completed..'),
 ('2016-11-26 20:24:41','root','Entering View Reports..'),
 ('2016-11-26 20:24:49','root','Entering View Reports..'),
 ('2016-11-26 20:24:55','root','Entering View Reports..'),
 ('2016-11-26 20:25:17','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 20:25:24','root','Logged in ..'),
 ('2016-11-26 20:25:24','root','Form - setup Completed..'),
 ('2016-11-26 20:25:32','root','Entering View Reports..'),
 ('2016-11-26 20:25:40','root','Entering View Reports..'),
 ('2016-11-26 20:26:13','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 20:26:20','root','Logged in ..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 20:26:20','root','Form - setup Completed..'),
 ('2016-11-26 20:26:28','root','Entering View Reports..'),
 ('2016-11-26 20:26:35','root','Entering View Reports..'),
 ('2016-11-26 20:27:13','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 20:27:20','root','Logged in ..'),
 ('2016-11-26 20:27:20','root','Form - setup Completed..'),
 ('2016-11-26 20:27:27','root','Entering View Reports..'),
 ('2016-11-26 20:27:33','root','Entering View Reports..'),
 ('2016-11-26 20:27:42','root','Entering View Reports..'),
 ('2016-11-26 20:27:50','root','Entering View Reports..'),
 ('2016-11-26 20:27:58','root','Entering View Reports..'),
 ('2016-11-26 20:30:58','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-26 20:31:05','root','Logged in ..'),
 ('2016-11-26 20:31:05','root','Form - setup Completed..'),
 ('2016-11-26 20:31:14','root','Entering View Reports..'),
 ('2016-11-26 20:31:22','root','Entering View Reports..'),
 ('2016-11-26 20:41:27','root','Entering View Reports..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-26 20:44:51','root','Entering More Analytics..'),
 ('2016-11-27 14:19:25','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 14:19:31','root','Logged in ..'),
 ('2016-11-27 14:19:31','root','Form - setup Completed..'),
 ('2016-11-27 14:21:05','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 14:21:12','root','Logged in ..'),
 ('2016-11-27 14:21:12','root','Form - setup Completed..'),
 ('2016-11-27 14:24:53','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 14:25:01','root','Logged in ..'),
 ('2016-11-27 14:25:01','root','Form - setup Completed..'),
 ('2016-11-27 14:54:37','root','Entering View Reports..'),
 ('2016-11-27 14:55:05','root','Entering View Reports..'),
 ('2016-11-27 14:58:49','root','Entering View Reports..'),
 ('2016-11-27 14:59:46','root','Entering View Reports..'),
 ('2016-11-27 15:01:13','root','Entering View Reports..'),
 ('2016-11-27 15:01:28','root','Entering View Reports..'),
 ('2016-11-27 15:04:13','root','Entering View Reports..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-27 15:04:26','root','Entering View Reports..'),
 ('2016-11-27 15:06:18','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 15:06:25','root','Logged in ..'),
 ('2016-11-27 15:06:25','root','Form - setup Completed..'),
 ('2016-11-27 15:07:01','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 15:08:44','root','Logged in ..'),
 ('2016-11-27 15:08:44','root','Form - setup Completed..'),
 ('2016-11-27 15:08:56','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 15:09:05','root','Logged in ..'),
 ('2016-11-27 15:09:06','root','Form - setup Completed..'),
 ('2016-11-27 15:11:01','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 15:11:08','root','Logged in ..'),
 ('2016-11-27 15:11:08','root','Form - setup Completed..'),
 ('2016-11-27 15:11:31','root','Entering View Reports..'),
 ('2016-11-27 15:54:09','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 15:54:16','root','Logged in ..'),
 ('2016-11-27 15:54:16','root','Form - setup Completed..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-27 15:54:47','root','Entering View Reports..'),
 ('2016-11-27 15:55:07','root','Entering View Reports..'),
 ('2016-11-27 15:55:13','root','Entering View Reports..'),
 ('2016-11-27 15:57:18','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 15:57:24','root','Logged in ..'),
 ('2016-11-27 15:57:24','root','Form - setup Completed..'),
 ('2016-11-27 15:58:11','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 15:58:18','root','Logged in ..'),
 ('2016-11-27 15:58:18','root','Form - setup Completed..'),
 ('2016-11-27 15:58:27','root','Entering View Reports..'),
 ('2016-11-27 15:59:15','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 15:59:22','root','Logged in ..'),
 ('2016-11-27 15:59:22','root','Form - setup Completed..'),
 ('2016-11-27 15:59:32','root','Entering View Reports..'),
 ('2016-11-27 16:00:32','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 16:00:39','root','Logged in ..'),
 ('2016-11-27 16:00:39','root','Form - setup Completed..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-27 16:00:50','root','Entering View Reports..'),
 ('2016-11-27 16:01:12','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 16:01:19','root','Logged in ..'),
 ('2016-11-27 16:01:19','root','Form - setup Completed..'),
 ('2016-11-27 16:01:27','root','Entering View Reports..'),
 ('2016-11-27 16:01:53','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 16:02:00','root','Logged in ..'),
 ('2016-11-27 16:02:00','root','Form - setup Completed..'),
 ('2016-11-27 16:02:09','root','Entering View Reports..'),
 ('2016-11-27 16:08:01','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 16:08:11','root','Logged in ..'),
 ('2016-11-27 16:08:11','root','Form - setup Completed..'),
 ('2016-11-27 16:08:20','root','Entering View Reports..'),
 ('2016-11-27 16:09:13','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 16:09:20','root','Logged in ..'),
 ('2016-11-27 16:09:21','root','Form - setup Completed..'),
 ('2016-11-27 16:09:31','root','Entering View Reports..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-27 16:10:09','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 16:10:16','root','Logged in ..'),
 ('2016-11-27 16:10:17','root','Form - setup Completed..'),
 ('2016-11-27 16:10:25','root','Entering View Reports..'),
 ('2016-11-27 16:12:21','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 16:12:30','root','Logged in ..'),
 ('2016-11-27 16:12:31','root','Form - setup Completed..'),
 ('2016-11-27 16:12:46','root','Entering View Reports..'),
 ('2016-11-27 16:13:54','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 16:14:01','root','Logged in ..'),
 ('2016-11-27 16:14:01','root','Form - setup Completed..'),
 ('2016-11-27 16:14:09','root','Entering View Reports..'),
 ('2016-11-27 16:22:42','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 16:22:49','root','Logged in ..'),
 ('2016-11-27 16:22:49','root','Form - setup Completed..'),
 ('2016-11-27 16:22:59','root','Entering View Reports..'),
 ('2016-11-27 16:54:46','root','Amco Benge OHS Applicatino Started..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-27 16:54:54','root','Logged in ..'),
 ('2016-11-27 16:54:54','root','Form - setup Completed..'),
 ('2016-11-27 16:56:39','root','Entering More Analytics..'),
 ('2016-11-27 17:09:34','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 17:09:40','root','Logged in ..'),
 ('2016-11-27 17:09:40','root','Form - setup Completed..'),
 ('2016-11-27 17:12:21','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 17:12:27','root','Logged in ..'),
 ('2016-11-27 17:12:27','root','Form - setup Completed..'),
 ('2016-11-27 17:14:12','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 17:14:17','root','Logged in ..'),
 ('2016-11-27 17:14:17','root','Form - setup Completed..'),
 ('2016-11-27 17:15:33','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 17:15:39','root','Logged in ..'),
 ('2016-11-27 17:15:39','root','Form - setup Completed..'),
 ('2016-11-27 17:28:10','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 17:28:16','root','Logged in ..');
INSERT INTO `amco_logger` (`logtime`,`logby`,`logMessage`) VALUES 
 ('2016-11-27 17:28:16','root','Form - setup Completed..'),
 ('2016-11-27 17:29:54','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 17:30:02','root','Logged in ..'),
 ('2016-11-27 17:30:02','root','Form - setup Completed..'),
 ('2016-11-27 17:36:20','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 17:36:25','root','Logged in ..'),
 ('2016-11-27 17:36:26','root','Form - setup Completed..'),
 ('2016-11-27 17:43:10','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 17:43:18','root','Logged in ..'),
 ('2016-11-27 17:43:18','root','Form - setup Completed..'),
 ('2016-11-27 17:58:20','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 17:58:29','root','Logged in ..'),
 ('2016-11-27 17:58:29','root','Form - setup Completed..'),
 ('2016-11-27 18:03:06','root','Amco Benge OHS Applicatino Started..'),
 ('2016-11-27 18:03:12','root','Logged in ..'),
 ('2016-11-27 18:03:13','root','Form - setup Completed..'),
 ('2016-11-27 18:05:12','root','Entering More Analytics..');
/*!40000 ALTER TABLE `amco_logger` ENABLE KEYS */;


--
-- Definition of table `amco_param`
--

DROP TABLE IF EXISTS `amco_param`;
CREATE TABLE `amco_param` (
  `paramID` int(11) DEFAULT NULL,
  `paramName` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `amco_param`
--

/*!40000 ALTER TABLE `amco_param` DISABLE KEYS */;
INSERT INTO `amco_param` (`paramID`,`paramName`) VALUES 
 (1,'Systolic - BP'),
 (2,'Diastolic-BP'),
 (278,'BLL'),
 (12,'Calcium'),
 (25,'Haemoglobin'),
 (190,'Serum Creatinine'),
 (227,'Haematocrit');
/*!40000 ALTER TABLE `amco_param` ENABLE KEYS */;


--
-- Definition of table `amco_result`
--

DROP TABLE IF EXISTS `amco_result`;
CREATE TABLE `amco_result` (
  `empID` varchar(40) DEFAULT NULL,
  `empName` varchar(50) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `site` varchar(100) DEFAULT NULL,
  `floor` varchar(100) DEFAULT NULL,
  `tempdate` varchar(40) DEFAULT NULL,
  `BLL` double DEFAULT NULL,
  `mydate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `amco_result`
--

/*!40000 ALTER TABLE `amco_result` DISABLE KEYS */;
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('710041','K. NAGARAJ','MM','tpsd3','2w Ass','25-04-2016',72.7,'2016-04-25'),
 ('710041','K. NAGARAJ','MM','tpsd3','2w Ass','30-05-2016',67.61,'2016-05-30'),
 ('710159','K SARAVANKUMAR','MM','tpsd3','2w Ass','14-01-2016',86,'2016-01-14'),
 ('710159','K SARAVANKUMAR','MM','tpsd3','2w Ass','15-02-2016',78.36,'2016-02-15'),
 ('710159','K SARAVANKUMAR','MM','tpsd3','2w Ass','29-02-2016',68.1,'2016-02-29'),
 ('710159','K SARAVANKUMAR','MM','tpsd3','2w Ass','30-05-2016',63.3,'2016-05-30'),
 ('710255','S.SURESH KUMAR','MM','tpsd3','ASSEMBLY','25-04-2016',58.27,'2016-04-25'),
 ('710160','P.HARIDOSS','MM','tpsd3','ASSEMBLY','14-01-2016',58.38,'2016-01-14'),
 ('710160','P.HARIDOSS','MM','tpsd3','ASSEMBLY','29-02-2016',50.51,'2016-02-29'),
 ('710160','P.HARIDOSS','MM','tpsd3','ASSEMBLY','30-05-2016',57.31,'2016-05-30'),
 ('710040','A.G.MOHANAKRISHNAN ','MM','tpsd3','2w Ass','25-04-2016',88.43,'2016-04-25'),
 ('710040','A.G.MOHANAKRISHNAN ','MM','tpsd3','2w Ass','19-08-2016',56.92,'2016-08-19');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('710007','MMM PERUMAL ','MM','tpsd3','Production','25-04-2016',61.45,'2016-04-25'),
 ('710007','MMM PERUMAL ','MM','tpsd3','Production','30-05-2016',59.51,'2016-05-30'),
 ('710240','S.SUBASH    ','MM','tpsd3','Purchase','14-01-2016',63.47,'2016-01-14'),
 ('710240','S.SUBASH    ','MM','tpsd3','Purchase','29-02-2016',51.41,'2016-02-29'),
 ('710240','S.SUBASH    ','MM','tpsd3','Purchase','30-05-2016',43.76,'2016-05-30'),
 ('710025','S.KARTHIKEYAN','MM','tpsd3','Maint','25-04-2016',40.68,'2016-04-25'),
 ('710025','S.KARTHIKEYAN','MM','tpsd3','Maint','23-06-2016',43.3,'2016-06-23'),
 ('710209','K SELVAKUMAR','MM','tpsd3','Personnel','25-04-2016',43.03,'2016-04-25'),
 ('7','S.BALASUBARMANIAN','MM','tpsd1','Floor 1','15-02-2016',21.87,'2016-02-15'),
 ('7','S.BALASUBARMANIAN','MM','tpsd1','Floor 1','19-08-2016',39.36,'2016-08-19'),
 ('8','P.SELVAM','MM','tpsd1','Floor 1','19-08-2016',27.4,'2016-08-19'),
 ('49','R.IYANAR','MM','tpsd1','Floor 1','14-01-2016',52.32,'2016-01-14');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('49','R.IYANAR','MM','tpsd1','Floor 1','29-02-2016',43.94,'2016-02-29'),
 ('49','R.IYANAR','MM','tpsd1','Floor 1','21-06-2016',45.55,'2016-06-21'),
 ('126','S.LINGESAN','MM','tpsd1','Floor 1','29-02-2016',44.87,'2016-02-29'),
 ('126','S.LINGESAN','MM','tpsd1','Floor 1','21-06-2016',52.57,'2016-06-21'),
 ('74','K.SHENBAGAM','MM','tpsd1','Floor 1','15-02-2016',49.27,'2016-02-15'),
 ('74','K.SHENBAGAM','MM','tpsd1','Floor 1','19-08-2016',43.17,'2016-08-19'),
 ('78','E.MUTHUPANDI','MM','tpsd1','Floor 1','15-02-2016',52.17,'2016-02-15'),
 ('78','E.MUTHUPANDI','MM','tpsd1','Floor 1','21-06-2016',57.81,'2016-06-21'),
 ('120','K.LOGANATHAN','MM','tpsd1','Floor 1','22-02-2016',47.72,'2016-02-22'),
 ('120','K.LOGANATHAN','MM','tpsd1','Floor 1','21-06-2016',50.53,'2016-06-21'),
 ('174','V.SHANMUGAM','MM','tpsd1','Floor 1','29-02-2016',49.79,'2016-02-29'),
 ('174','V.SHANMUGAM','MM','tpsd1','Floor 1','21-06-2016',44.67,'2016-06-21'),
 ('170','M.GANAPATHY','MM','tpsd1','Floor 1','29-02-2016',20.25,'2016-02-29');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('710096','K.Ramesh','MM','tpsd2','4w Ass','30-05-2016',87.2,'2016-05-30'),
 ('710096','K.Ramesh','MM','tpsd2','4w Ass','19-08-2016',74.01,'2016-08-19'),
 ('710100','D.Paramanathan','MM','tpsd2','CASTING','25-04-2016',77.76,'2016-04-25'),
 ('710100','D.Paramanathan','MM','tpsd2','CASTING','30-05-2016',76.94,'2016-05-30'),
 ('710102','G.Chokkanathan','MM','tpsd2','4w Ass','25-04-2016',73.23,'2016-04-25'),
 ('710102','G.Chokkanathan','MM','tpsd2','4w Ass','21-06-2016',81.27,'2016-06-21'),
 ('710134','R. Ramachandran','MM','tpsd2','FORK LIFT','30-05-2016',87.05,'2016-05-30'),
 ('710134','R. Ramachandran','MM','tpsd2','FORK LIFT','19-08-2016',77.75,'2016-08-19'),
 ('710162','S.Santosh Kumar','MM','tpsd2','QC','25-04-2016',75.95,'2016-04-25'),
 ('710162','S.Santosh Kumar','MM','tpsd2','QC','30-05-2016',82.93,'2016-05-30'),
 ('710162','S.Santosh Kumar','MM','tpsd2','QC','19-08-2016',62.33,'2016-08-19'),
 ('710242','R. GANESAN','MM','tpsd2','PASTING','25-04-2016',58.1,'2016-04-25');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('710242','R. GANESAN','MM','tpsd2','PASTING','30-05-2016',55.62,'2016-05-30'),
 ('710133','C. VINOTHKUMAR','MM','tpsd2','Formation','25-04-2016',56.69,'2016-04-25'),
 ('710133','C. VINOTHKUMAR','MM','tpsd2','Formation','30-05-2016',58.06,'2016-05-30'),
 ('710092','P.SUNDARAJ','MM','tpsd2','4w Ass','25-04-2016',55.19,'2016-04-25'),
 ('710092','P.SUNDARAJ','MM','tpsd2','4w Ass','21-06-2016',58.95,'2016-06-21'),
 ('710055','L.SETHURAMALINGAM','MM','tpsd2','Formation','25-04-2016',54.8,'2016-04-25'),
 ('710055','L.SETHURAMALINGAM','MM','tpsd2','Formation','30-05-2016',58.6,'2016-05-30'),
 ('710219','B.Ramesh','MM','tpsd2','4w Ass','25-04-2016',76.14,'2016-04-25'),
 ('710219','B.Ramesh','MM','tpsd2','4w Ass','30-05-2016',76.91,'2016-05-30'),
 ('710140','G THIYAGARAJAN','MM','tpsd3','2w Ass','30-05-2016',67.09,'2016-05-30'),
 ('710135','N.LOGANATHAN','MM','tpsd3','2w Ass','25-04-2016',67.88,'2016-04-25'),
 ('710135','N.LOGANATHAN','MM','tpsd3','2w Ass','30-05-2016',68.79,'2016-05-30');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('710222','R.RAJA','MM','tpsd3','2w Ass','30-05-2016',65.48,'2016-05-30'),
 ('710120','K.ANANTHAN','MM','tpsd3','ASS-CHA','25-04-2016',52.24,'2016-04-25'),
 ('710120','K.ANANTHAN','MM','tpsd3','ASS-CHA','30-05-2016',60.15,'2016-05-30'),
 ('710042','T.RAMESH','MM','tpsd3','VRLA','25-04-2016',53.21,'2016-04-25'),
 ('710042','T.RAMESH','MM','tpsd3','VRLA','30-05-2016',55.78,'2016-05-30'),
 ('710197','R.MOHAMMED RABEEK','MM','tpsd3','ELE MAIN','25-04-2016',54.75,'2016-04-25'),
 ('710197','R.MOHAMMED RABEEK','MM','tpsd3','ELE MAIN','30-05-2016',51.32,'2016-05-30'),
 ('710230','A.SILAMBARASAN','MM','tpsd3','ELE MAIN','25-04-2016',54.13,'2016-04-25'),
 ('710230','A.SILAMBARASAN','MM','tpsd3','ELE MAIN','30-05-2016',50.2,'2016-05-30'),
 ('710244','K.RAMESH KUMAR','MM','tpsd3','CASTING','25-04-2016',54.05,'2016-04-25'),
 ('710244','K.RAMESH KUMAR','MM','tpsd3','CASTING','30-05-2016',52,'2016-05-30'),
 ('710254','M.ANANDHARAJAN','MM','tpsd3','CASTING','30-05-2016',57.59,'2016-05-30');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('710079','J RAMKUMAR','MM','tpsd3','QC','25-04-2016',49.23,'2016-04-25'),
 ('710079','J RAMKUMAR','MM','tpsd3','QC','19-08-2016',44.7,'2016-08-19'),
 ('710202','N MANAVALAN','MM','tpsd3','Elect','25-04-2016',46.65,'2016-04-25'),
 ('710192','S. VAITHIYANATHAN','MM','tpsd3','Elect','25-04-2016',53.16,'2016-04-25'),
 ('710192','S. VAITHIYANATHAN','MM','tpsd3','Elect','30-05-2016',48.59,'2016-05-30'),
 ('5','R.RAMAR','MM','tpsd1','Floor 1','14-01-2016',41.5,'2016-01-14'),
 ('5','R.RAMAR','MM','tpsd1','Floor 1','29-02-2016',39.86,'2016-02-29'),
 ('5','R.RAMAR','MM','tpsd1','Floor 1','21-06-2016',44.55,'2016-06-21'),
 ('9','L.MUTHUKRISHNAN','MM','tpsd1','Floor 1','14-01-2016',46.16,'2016-01-14'),
 ('9','L.MUTHUKRISHNAN','MM','tpsd1','Floor 1','29-02-2016',41.61,'2016-02-29'),
 ('9','L.MUTHUKRISHNAN','MM','tpsd1','Floor 1','19-08-2016',40.68,'2016-08-19'),
 ('12','P.MICHEAL BOSCO','MM','tpsd1','Floor 1','15-02-2016',30.33,'2016-02-15'),
 ('12','P.MICHEAL BOSCO','MM','tpsd1','Floor 1','19-08-2016',30.77,'2016-08-19');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('13','R.BALAMUTHUKUMAR','MM','tpsd1','Floor 1','14-01-2016',25.78,'2016-01-14'),
 ('13','R.BALAMUTHUKUMAR','MM','tpsd1','Floor 1','19-08-2016',29.14,'2016-08-19'),
 ('14','T.VEERAPANDIAN','MM','tpsd1','Floor 1','14-01-2016',27.63,'2016-01-14'),
 ('14','T.VEERAPANDIAN','MM','tpsd1','Floor 1','19-08-2016',26.5,'2016-08-19'),
 ('15','G.MARIAPPAN','MM','tpsd1','Floor 1','14-01-2016',39.28,'2016-01-14'),
 ('15','G.MARIAPPAN','MM','tpsd1','Floor 1','19-08-2016',37.99,'2016-08-19'),
 ('21','S.SHANMUGA SUNDARAM','MM','tpsd1','Floor 1','14-01-2016',49.1,'2016-01-14'),
 ('21','S.SHANMUGA SUNDARAM','MM','tpsd1','Floor 1','29-02-2016',44.97,'2016-02-29'),
 ('21','S.SHANMUGA SUNDARAM','MM','tpsd1','Floor 1','21-06-2016',44.19,'2016-06-21'),
 ('22','V.BHOOVARAGAMOORTHY','MM','tpsd1','Floor 1','14-01-2016',35.35,'2016-01-14'),
 ('22','V.BHOOVARAGAMOORTHY','MM','tpsd1','Floor 1','19-08-2016',32.15,'2016-08-19'),
 ('17','K.RAMALINGAM','MM','tpsd1','Floor 1','15-02-2016',29.78,'2016-02-15');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('17','K.RAMALINGAM','MM','tpsd1','Floor 1','19-08-2016',27.02,'2016-08-19'),
 ('26','K.KALAYANA SUNDARAM','MM','tpsd1','Floor 1','14-01-2016',32.83,'2016-01-14'),
 ('26','K.KALAYANA SUNDARAM','MM','tpsd1','Floor 1','19-08-2016',32.62,'2016-08-19'),
 ('30','G.MURUGAN','MM','tpsd1','Floor 1','15-02-2016',24.53,'2016-02-15'),
 ('30','G.MURUGAN','MM','tpsd1','Floor 1','19-08-2016',24.4,'2016-08-19'),
 ('31','V.MURUGAN','MM','tpsd1','Floor 1','14-01-2016',32.91,'2016-01-14'),
 ('31','V.MURUGAN','MM','tpsd1','Floor 1','19-08-2016',32.6,'2016-08-19'),
 ('42','D.BALAJI','MM','tpsd1','Floor 1','14-01-2016',43.94,'2016-01-14'),
 ('42','D.BALAJI','MM','tpsd1','Floor 1','29-02-2016',38.23,'2016-02-29'),
 ('42','D.BALAJI','MM','tpsd1','Floor 1','21-06-2016',45.34,'2016-06-21'),
 ('24','S.NAGAHARIRAMASUBRAMANIAN','MM','tpsd1','Floor 1','15-02-2016',33.68,'2016-02-15'),
 ('24','S.NAGAHARIRAMASUBRAMANIAN','MM','tpsd1','Floor 1','19-08-2016',39.88,'2016-08-19'),
 ('45','D.JAYAPARKASH','MM','tpsd1','Floor 1','14-01-2016',25.49,'2016-01-14');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('45','D.JAYAPARKASH','MM','tpsd1','Floor 1','19-08-2016',21.1,'2016-08-19'),
 ('47','R.MURUGESAN','MM','tpsd1','Floor 1','14-01-2016',33.33,'2016-01-14'),
 ('47','R.MURUGESAN','MM','tpsd1','Floor 1','19-08-2016',38.19,'2016-08-19'),
 ('52','S.INDIRATHI','MM','tpsd1','Floor 1','15-02-2016',43.86,'2016-02-15'),
 ('52','S.INDIRATHI','MM','tpsd1','Floor 1','21-06-2016',46.49,'2016-06-21'),
 ('53','C.RAMESH','MM','tpsd1','Floor 1','14-01-2016',41.71,'2016-01-14'),
 ('53','C.RAMESH','MM','tpsd1','Floor 1','29-02-2016',35.86,'2016-02-29'),
 ('60','M.SEETHARAMAN','MM','tpsd1','Floor 1','14-01-2016',27.33,'2016-01-14'),
 ('60','M.SEETHARAMAN','MM','tpsd1','Floor 1','19-08-2016',28.38,'2016-08-19'),
 ('61','M.KARUPPASAMY','MM','tpsd1','Floor 1','15-02-2016',38.26,'2016-02-15'),
 ('61','M.KARUPPASAMY','MM','tpsd1','Floor 1','19-08-2016',39.68,'2016-08-19'),
 ('75','R.PARANJOTHI','MM','tpsd1','Floor 1','15-02-2016',34.46,'2016-02-15'),
 ('75','R.PARANJOTHI','MM','tpsd1','Floor 1','19-08-2016',36,'2016-08-19');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('76','S.SHANMUGANANTHAKRISHNAN','MM','tpsd1','Floor 1','15-02-2016',24.42,'2016-02-15'),
 ('76','S.SHANMUGANANTHAKRISHNAN','MM','tpsd1','Floor 1','19-08-2016',25.67,'2016-08-19'),
 ('79','E.SENTHILVEL','MM','tpsd1','Floor 1','22-02-2016',26.48,'2016-02-22'),
 ('79','E.SENTHILVEL','MM','tpsd1','Floor 1','19-08-2016',27.65,'2016-08-19'),
 ('84','R.MURUGESAN','MM','tpsd1','Floor 1','15-01-2016',33.33,'2016-01-15'),
 ('84','R.MURUGESAN','MM','tpsd1','Floor 1','15-02-2016',24.29,'2016-02-15'),
 ('84','R.MURUGESAN','MM','tpsd1','Floor 1','19-08-2016',24.08,'2016-08-19'),
 ('88','M.SURESH KUMAR','MM','tpsd1','Floor 1','15-02-2016',41.22,'2016-02-15'),
 ('88','M.SURESH KUMAR','MM','tpsd1','Floor 1','21-06-2016',39.51,'2016-06-21'),
 ('89','K.SANKARAN','MM','tpsd1','Floor 1','22-02-2016',33.09,'2016-02-22'),
 ('89','K.SANKARAN','MM','tpsd1','Floor 1','19-08-2016',36.37,'2016-08-19'),
 ('90','A.THIRUNAVUKARASU','MM','tpsd1','Floor 1','22-02-2016',37.07,'2016-02-22'),
 ('90','A.THIRUNAVUKARASU','MM','tpsd1','Floor 1','19-08-2016',36.8,'2016-08-19');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('91','K.SENTHILVEL','MM','tpsd1','Floor 1','22-02-2016',29.97,'2016-02-22'),
 ('91','K.SENTHILVEL','MM','tpsd1','Floor 1','19-08-2016',31.55,'2016-08-19'),
 ('96','N.THIRUMALAI','MM','tpsd1','Floor 1','22-02-2016',20.61,'2016-02-22'),
 ('96','N.THIRUMALAI','MM','tpsd1','Floor 1','19-08-2016',28.31,'2016-08-19'),
 ('99','K.S.NAGARAJAN','MM','tpsd1','Floor 1','22-02-2016',27.58,'2016-02-22'),
 ('99','K.S.NAGARAJAN','MM','tpsd1','Floor 1','19-08-2016',31.44,'2016-08-19'),
 ('105','M.SURESH KUMAR','MM','tpsd1','Floor 1','22-02-2016',36.43,'2016-02-22'),
 ('105','M.SURESH KUMAR','MM','tpsd1','Floor 1','19-08-2016',34.84,'2016-08-19'),
 ('107','M.SUNDAR','MM','tpsd1','Floor 1','22-02-2016',33.68,'2016-02-22'),
 ('107','M.SUNDAR','MM','tpsd1','Floor 1','19-08-2016',40.12,'2016-08-19'),
 ('153','J.SURYANARAYANAN','MM','tpsd1','Floor 1','29-02-2016',30.46,'2016-02-29'),
 ('95','B.ASHOK KUMAR','MM','tpsd1','Floor 1','22-02-2016',25.18,'2016-02-22'),
 ('95','B.ASHOK KUMAR','MM','tpsd1','Floor 1','19-08-2016',22.29,'2016-08-19');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('101','V.HARIBABU','MM','tpsd1','Floor 1','22-02-2016',39.99,'2016-02-22'),
 ('101','V.HARIBABU','MM','tpsd1','Floor 1','21-06-2016',47.76,'2016-06-21'),
 ('109','R.MURALI','MM','tpsd1','Floor 1','22-02-2016',35.11,'2016-02-22'),
 ('109','R.MURALI','MM','tpsd1','Floor 1','19-08-2016',33.36,'2016-08-19'),
 ('110','V.MUTHURAJ','MM','tpsd1','Floor 1','22-02-2016',26.1,'2016-02-22'),
 ('110','V.MUTHURAJ','MM','tpsd1','Floor 1','19-08-2016',24.03,'2016-08-19'),
 ('111','N.SIVAKUMAR','MM','tpsd1','Floor 1','22-02-2016',32.9,'2016-02-22'),
 ('111','N.SIVAKUMAR','MM','tpsd1','Floor 1','19-08-2016',36.02,'2016-08-19'),
 ('121','C.KOTHANDARAMAN','MM','tpsd1','Floor 1','29-02-2016',28.77,'2016-02-29'),
 ('98','N.ANANTHARAMAKRISHNAN','MM','tpsd1','Floor 1','22-02-2016',24.5,'2016-02-22'),
 ('98','N.ANANTHARAMAKRISHNAN','MM','tpsd1','Floor 1','19-08-2016',24.09,'2016-08-19'),
 ('113','S.GOPAL','MM','tpsd1','Floor 1','22-02-2016',30.69,'2016-02-22'),
 ('113','S.GOPAL','MM','tpsd1','Floor 1','19-08-2016',31.43,'2016-08-19');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('118','N.KRISHNAN','MM','tpsd1','Floor 1','29-02-2016',38.36,'2016-02-29'),
 ('118','N.KRISHNAN','MM','tpsd1','Floor 1','19-08-2016',46.87,'2016-08-19'),
 ('122','V.ELUMALAI','MM','tpsd1','Floor 1','22-02-2016',28.11,'2016-02-22'),
 ('122','V.ELUMALAI','MM','tpsd1','Floor 1','19-08-2016',26.73,'2016-08-19'),
 ('125','R.MEGANATHAN','MM','tpsd1','Floor 1','22-02-2016',43.46,'2016-02-22'),
 ('125','R.MEGANATHAN','MM','tpsd1','Floor 1','21-06-2016',40.4,'2016-06-21'),
 ('163','P.SARAVANAN','MM','tpsd1','Floor 1','29-02-2016',45.22,'2016-02-29'),
 ('163','P.SARAVANAN','MM','tpsd1','Floor 1','19-08-2016',40.79,'2016-08-19'),
 ('165','S.VELAYUDHAM','MM','tpsd1','Floor 1','29-02-2016',27.19,'2016-02-29'),
 ('123','G.KRISHNAN','MM','tpsd1','Floor 1','22-02-2016',32.48,'2016-02-22'),
 ('123','G.KRISHNAN','MM','tpsd1','Floor 1','19-08-2016',31.41,'2016-08-19'),
 ('124','K.MUNIAN','MM','tpsd1','Floor 1','22-02-2016',28.74,'2016-02-22'),
 ('133','K.PANNEER SELVAN','MM','tpsd1','Floor 1','29-02-2016',31.62,'2016-02-29');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('141','K.RAJA','MM','tpsd1','Floor 1','29-02-2016',28.42,'2016-02-29'),
 ('142','T.RAVICHANDRAN','MM','tpsd1','Floor 1','29-02-2016',32.68,'2016-02-29'),
 ('146','D.DHARMAN','MM','tpsd1','Floor 1','29-02-2016',28.33,'2016-02-29'),
 ('167','G.SELVAKUMAR','MM','tpsd1','Floor 1','29-02-2016',33.66,'2016-02-29'),
 ('169','H.JOHNSON','MM','tpsd1','Floor 1','29-02-2016',34.62,'2016-02-29'),
 ('37','V.SELVAMUTHUKUMAR','MM','tpsd1','Floor 1','15-02-2016',38.19,'2016-02-15'),
 ('37','V.SELVAMUTHUKUMAR','MM','tpsd1','Floor 1','19-08-2016',41.2,'2016-08-19'),
 ('135','R.NAGARAJAN','MM','tpsd1','Floor 1','29-02-2016',36.19,'2016-02-29'),
 ('143','R.MOHAN','MM','tpsd1','Floor 1','29-02-2016',34.49,'2016-02-29'),
 ('162','V.DEVADOSS','MM','tpsd1','Floor 1','29-02-2016',29.35,'2016-02-29'),
 ('164','K.RAJKUMAR','MM','tpsd1','Floor 1','29-02-2016',40.82,'2016-02-29'),
 ('164','K.RAJKUMAR','MM','tpsd1','Floor 1','21-06-2016',41.47,'2016-06-21'),
 ('168','P.RAJENDRAN','MM','tpsd1','Floor 1','29-02-2016',23.96,'2016-02-29');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('171','K.KANNAN','MM','tpsd1','Floor 1','29-02-2016',36.3,'2016-02-29'),
 ('173','V.PRAKASH','MM','tpsd1','Floor 1','29-02-2016',43.22,'2016-02-29'),
 ('173','V.PRAKASH','MM','tpsd1','Floor 1','21-06-2016',48.97,'2016-06-21'),
 ('5003','G.Saravanan','MM','tpsd2','Stores','25-04-2016',69.15,'2016-04-25'),
 ('5003','G.Saravanan','MM','tpsd2','Stores','30-05-2016',74.72,'2016-05-30'),
 ('710216','M.Surendran ','MM','tpsd2','4w Ass','25-04-2016',65.67,'2016-04-25'),
 ('710216','M.Surendran ','MM','tpsd2','4w Ass','21-06-2016',72.7,'2016-06-21'),
 ('710231','G.Haridoss ','MM','tpsd2','Stores','25-04-2016',60.28,'2016-04-25'),
 ('710231','G.Haridoss ','MM','tpsd2','Stores','30-05-2016',73.53,'2016-05-30'),
 ('710095','V.Janarthanan','MM','tpsd2','4w Ass','25-04-2016',61.29,'2016-04-25'),
 ('710095','V.Janarthanan','MM','tpsd2','4w Ass','30-05-2016',71.13,'2016-05-30'),
 ('710139','K.Magesh','MM','tpsd2','CASTING','30-05-2016',71.18,'2016-05-30'),
 ('6001','N.Ragunathan','MM','tpsd2','PASTING','30-05-2016',74.12,'2016-05-30');
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('710048','S.Arunachalam','MM','tpsd2','Formation','25-04-2016',67.42,'2016-04-25'),
 ('710087','T.M.Gokulakrishnan','MM','tpsd2','QC','25-04-2016',60.26,'2016-04-25'),
 ('710087','T.M.Gokulakrishnan','MM','tpsd2','QC','30-05-2016',62.98,'2016-05-30'),
 ('710137','D.Jaganathan','MM','tpsd2','4w Ass','30-05-2016',68.74,'2016-05-30'),
 ('710136','R.Thirumal','MM','tpsd2','4w Ass','30-05-2016',61.88,'2016-05-30'),
 ('710094','S.Kaldoss','MM','tpsd2','CASTING','30-05-2016',66.64,'2016-05-30'),
 ('710044','S.Sadiqbhasa','MM','tpsd2','PASTING','30-05-2016',67.98,'2016-05-30');
/*!40000 ALTER TABLE `amco_result` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
