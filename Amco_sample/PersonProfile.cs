﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amco_sample
{
    public class  profileResponse
    {
        /*
        public bool isSuccess { get; set; }
        public int statusCode { get; set; }
        public String message { get; set; }
        */
        [JsonProperty("profile")]
        public PersonProfile profile { get; set; }
        [JsonProperty("personalDetails")]
        PersonalDetails personalDetails { get; set; }

        [JsonProperty("lifeStyle")]
        LifeStyle lifeStyle { get; set; }


        [JsonProperty("medicalHistory")]
        MedicalHistory medicalHistory { get; set; }
        [JsonProperty("familyHistory")]
        FamilyHistory familyHistory { get; set; }

    }
    public class PersonProfile
    {
        public PersonProfile()
        {
            patientHospitals = new List<PatientHospitals>();
           // patientPreferredPharmacyDetails = new List<PatientPreferredPharmacyDetails>();
           // patientPreferredLabDetails = new List<PatientPreferredLabDetails>();


        }
        [JsonProperty("profileImageUrl")]
        public String profileImageUrl { get; set; }
        [JsonProperty("fullName")]
        public String fullName { get; set; }
        [JsonProperty("dateOfBirth")]
        public String dateOfBirth { get; set; }
       
        //public String firstName{ get; set;}
        [JsonProperty("emailId")]
        public String emailId { get; set; }
        [JsonProperty("mobileNumber")]
        public String mobileNumber { get; set; } // number ?
        [JsonProperty("state")]
        public String state { get; set; }
        [JsonProperty("city")]
        public String city { get; set; }
        [JsonProperty("firstName")]
        public String firstName { get; set; }
        [JsonProperty("lastName")]
        public String lastName { get; set; }
        [JsonProperty("address")]
        public String address { get; set; }

        [JsonProperty("patientPreferredPharmacyDetails")]
       PatientPreferredPharmacyDetails patientPreferredPharmacyDetails { get; set; }

        [JsonProperty("patientPreferredLabDetails")]
        PatientPreferredLabDetails patientPreferredLabDetails { get; set; }

        [JsonProperty("patientHospitals")]
        List<PatientHospitals> patientHospitals { get; set; }
      

    


    }
    public class PatientPreferredLabDetails
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("patientId")]
        public int patientId { get; set; }
        [JsonProperty("emailId1")]
        public String emailId1 { get; set; }
        [JsonProperty("emailId2")]
        public String emailId2 { get; set; }
        [JsonProperty("name1")]
        public String name1 { get; set; }
        [JsonProperty("name2")]
        public String name2 { get; set; }
        [JsonProperty("contactNumber1")]
        public String contactNumber1 { get; set; }
        [JsonProperty("contactNumber2")]
        public String contactNumber2 { get; set; }
    }
    public class PatientHospitals
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("patientId")]
        public int patientId { get; set; }
        [JsonProperty("hospitalId")]
        public int hospitalId { get; set; }
        [JsonProperty("hospitalName")]
        public String hospitalName { get; set; }
        [JsonProperty("hospitalEmailId")]
        public String hospitalEmailId { get; set; }
        [JsonProperty("hospitalBranch")]
        public String hospitalBranch { get; set; }
        [JsonProperty("hospitalCity")]
        public String hospitalCity { get; set; }
        [JsonProperty("isAllowed")]
        public bool isAllowed { get; set; }
        [JsonProperty("hospitalUrl ")]
        public String hospitalUrl { get; set; }
        [JsonProperty("hospitalUserName")]
        public String hospitalUserName { get; set; }
        [JsonProperty("hospitalPassword")]
        public String hospitalPassword { get; set; }
    }
    public class PersonalDetails
    {
        [JsonProperty("gender")]
        public String gender { get; set; }
        [JsonProperty("bloodGroup")]
        public String bloodGroup { get; set; }  // changed from int to string, due to the empty profile received
        [JsonProperty("maritalStatus")]
        public String maritalStatus { get; set; }
        [JsonProperty("height")]
        public String height { get; set; }
        [JsonProperty("weight")]
        public String weight { get; set; }

        [JsonProperty("noOfChildren")]
        public String noOfChildren { get; set; }   // changed from int to string, due to the empty profile received
        [JsonProperty("consanguinity")]
        public String consanguinity { get; set; }   // changed from int to string, due to the empty profile received
        [JsonProperty("insurance")]
        Insurance insurance { get; set; }


    }
    public class Insurance
    {
        [JsonProperty("provider")]
        public String provider { get; set; }
        [JsonProperty("policyNo")]
        public String policyNo { get; set; }
        [JsonProperty("policyEndDate")]
        public String policyEndDate { get; set; }  // is date ?
    }
    public class PatientPreferredPharmacyDetails
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("patientId")]
        public int patientId { get; set; }
        [JsonProperty("emailId1")]

        public String emailId1 { get; set; }
        [JsonProperty("emailId2")]
        public String emailId2 { get; set; }
        [JsonProperty("name1")]
        public String name1 { get; set; }
        [JsonProperty("name2")]
        public String name2 { get; set; }
        [JsonProperty("contactNumber1")]
        public String contactNumber1 { get; set; }
        [JsonProperty("contactNumber2")]
        public String contactNumber2 { get; set; }
        
    }

    public class FamilyDetails
    {
        public FamilyDetails()
        {
            chronicConditions = new List<Chronic>();
        }
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("memberName")]
        public String memberName { get; set; }
        [JsonProperty("relationshipType")]
        public String relationshipType { get; set; }
        [JsonProperty("status")]
        public String status { get; set; }
        [JsonProperty("chronicConditions")]
        List<Chronic> chronicConditions { get; set; }
    }
    public class FamilyHistory
    {
        public FamilyHistory()
        {
            familyDetails = new List<FamilyDetails>();
        }
        [JsonProperty("familyDetails")]
        public List<FamilyDetails> familyDetails { get; set; }

    }

    public class Chronic
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("chronicIllnessType")]
        public int chronicIllnessType { get; set; }
        [JsonProperty("sinceAge")]
        public int sinceAge { get; set; }
    }
    public class Drug
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("drugName")]
        public String drugName { get; set; }
        [JsonProperty("dosage")]
        public String dosage { get; set; }
    }
    public class MedicalHistory
    {
        public MedicalHistory ()
            {
            DrugHistory = new List<Drug>();
            chronicIllnessList = new List<Chronic>();
            }
        [JsonProperty("pastMedicalHistory")]
        public String pastMedicalHistory { get; set; }
        [JsonProperty("chronicIllnessList")]
        public List<Chronic> chronicIllnessList { get; set; }
        [JsonProperty("allergicTo")]
        public String AllergicTo { get; set; }
        [JsonProperty("lastAnnualMedicalCheckupMonth")]
        public String LastAnnualMedicalCheckupMonth { get; set; }
        [JsonProperty("lastAnnualMedicalCheckupYear")]
        public String LastAnnualMedicalCheckupYear { get; set; }
        [JsonProperty("drugHistory")]
        public List<Drug> DrugHistory { get; set; }   // is array[]
        
        [JsonProperty("lastDrugUpdatedDate")]
        public String lastDrugUpdatedDate { get; set; }  // this attribute is not shown in the sample
        
    }
    public class LifeStyle
    {
        [JsonProperty("exerciseType")]
        public int exerciseType { get; set; }

        [JsonProperty("dietType")]
        public int dietType { get; set; }

        [JsonProperty("smoking")]
        public bool smoking { get; set; }

        [JsonProperty("smokingLevel")]
        public int smokingLevel { get; set; }

        [JsonProperty("alcohol")]
        public bool alcohol { get; set; }

        [JsonProperty("alcoholLevel")]
        public int alcoholLevel { get; set; }
    }
}
