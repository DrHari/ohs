-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.6.32-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema amco_hr
--

CREATE DATABASE IF NOT EXISTS amco_hr;
USE amco_hr;

--
-- Definition of table `amco_emp`
--

DROP TABLE IF EXISTS `amco_emp`;
CREATE TABLE `amco_emp` (
  `empID` varchar(40) DEFAULT NULL,
  `empName` varchar(50) DEFAULT NULL,
  `empEmail` varchar(100) DEFAULT NULL,
  `passWrd` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `site` varchar(100) DEFAULT NULL,
  `floor` varchar(100) DEFAULT NULL,
  `registereWithBeinge` int(11) DEFAULT NULL,
  `BeingePersonID` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `amco_emp`
--

/*!40000 ALTER TABLE `amco_emp` DISABLE KEYS */;
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('710041','K. NAGARAJ','710041.nagaraj@amco.com','welcome123','MM','tpsd3','2w Ass',1,'3235'),
 ('710159','K SARAVANKUMAR','710159.saravanakumar@amco.com','welcome123','MM','tpsd3','2w Ass',1,'3236'),
 ('710255','S.SURESH KUMAR','710255.sureshkumar@amco.com','welcome123','MM','tpsd3','ASSEMBLY',1,'3237'),
 ('710160','P.HARIDOSS','710160@haridoss@amco.com','welcome123','MM','tpsd3','ASSEMBLY',1,'3238'),
 ('710040','A.G.MOHANAKRISHNAN ','710040.mohanakrishnan@amco.com','welcome123','MM','tpsd3','2w Ass',1,'3239'),
 ('710007','MMM PERUMAL ','710007.perumal@amco.com','welcome123','MM','tpsd3','Production',1,'3240'),
 ('710240','S.SUBASH    ','710240.subash@amco.com','welcome123','MM','tpsd3','Purchase',1,'3241'),
 ('710025','S.KARTHIKEYAN','710025.karthikeyan@amco.com','welcome123','MM','tpsd3','Maint',1,'3242'),
 ('710209','K SELVAKUMAR','710209.selvakumar@amco.com','welcome123','MM','tpsd3','Personnel',1,'3243'),
 ('7','S.BALASUBARMANIAN','7.balasubramanian@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3244');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('8','P.SELVAM','8.selvam@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3245'),
 ('49','R.IYANAR','49.iyanar@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3246'),
 ('126','S.LINGESAN','126.lingesan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3247'),
 ('74','K.SHENBAGAM','74.shenbagam@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3248'),
 ('78','E.MUTHUPANDI','78.muthupandi@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3249'),
 ('120','K.LOGANATHAN','120.loganathan@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3250'),
 ('174','V.SHANMUGAM','174.shanmugham@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3251'),
 ('170','M.GANAPATHY','170.ganapathy@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3252'),
 ('710096','K.Ramesh','710096.ramesh@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3253'),
 ('710100','D.Paramanathan','710100.paramanathan@amco.com','welcome123','MM','tpsd2','CASTING',1,'3254'),
 ('710102','G.Chokkanathan','710102.chockanathan@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3255');
INSERT INTO `amco_emp` (`empID`,`empName`,`empEmail`,`passWrd`,`location`,`site`,`floor`,`registereWithBeinge`,`BeingePersonID`) VALUES 
 ('710134','R. Ramachandran','710134.ramachandran@amco.com','welcome123','MM','tpsd2','FORK LIFT',1,'3256'),
 ('710162','S.Santosh Kumar','710162.santhoshkumar@amco.com','welcome123','MM','tpsd2','QC',1,'3257'),
 ('710242','R. GANESAN','710242,ganesan@amco.com','welcome123','MM','tpsd2','PASTING',1,'3258'),
 ('710133','C. VINOTHKUMAR','710133.vinothkumar@amco.com','welcome123','MM','tpsd2','Formation',1,'3259'),
 ('710092','P.SUNDARAJ','710092.sundaraj@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3260'),
 ('710055','L.SETHURAMALINGAM','710055.sethuramalingam@amco.com','welcome123','MM','tpsd2','Formation',1,'3261'),
 ('710219','B.Ramesh','710219.ramesh@amco.com','welcome123','MM','tpsd2','4w Ass',1,'3262'),
 ('99999','Hari','hari@amco.com','welcome123','MM','tpsd1','Floor 1',1,'3263');
/*!40000 ALTER TABLE `amco_emp` ENABLE KEYS */;


--
-- Definition of table `amco_hazard_param`
--

DROP TABLE IF EXISTS `amco_hazard_param`;
CREATE TABLE `amco_hazard_param` (
  `paramID` int(11) DEFAULT NULL,
  `paramName` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `amco_hazard_param`
--

/*!40000 ALTER TABLE `amco_hazard_param` DISABLE KEYS */;
INSERT INTO `amco_hazard_param` (`paramID`,`paramName`) VALUES 
 (278,'BLL');
/*!40000 ALTER TABLE `amco_hazard_param` ENABLE KEYS */;


--
-- Definition of table `amco_param`
--

DROP TABLE IF EXISTS `amco_param`;
CREATE TABLE `amco_param` (
  `paramID` int(11) DEFAULT NULL,
  `paramName` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `amco_param`
--

/*!40000 ALTER TABLE `amco_param` DISABLE KEYS */;
INSERT INTO `amco_param` (`paramID`,`paramName`) VALUES 
 (1,'Systolic - BP'),
 (2,'Diastolic-BP'),
 (278,'BLL'),
 (12,'Calcium'),
 (25,'Haemoglobin'),
 (190,'Serum Creatinine'),
 (227,'Haematocrit');
/*!40000 ALTER TABLE `amco_param` ENABLE KEYS */;


--
-- Definition of table `amco_result`
--

DROP TABLE IF EXISTS `amco_result`;
CREATE TABLE `amco_result` (
  `empID` varchar(40) DEFAULT NULL,
  `empName` varchar(50) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `site` varchar(100) DEFAULT NULL,
  `floor` varchar(100) DEFAULT NULL,
  `tempdate` varchar(40) DEFAULT NULL,
  `BLL` double DEFAULT NULL,
  `mydate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `amco_result`
--

/*!40000 ALTER TABLE `amco_result` DISABLE KEYS */;
INSERT INTO `amco_result` (`empID`,`empName`,`location`,`site`,`floor`,`tempdate`,`BLL`,`mydate`) VALUES 
 ('7','S.BALASUBARMANIAN','MM','tpsd1','Floor 1','19-08-2016',39.36,'2016-08-19'),
 ('8','P.SELVAM','MM','tpsd1','Floor 1','19-08-2016',27.4,'2016-08-19'),
 ('49','R.IYANAR','MM','tpsd1','Floor 1','21-06-2016',45.55,'2016-06-21'),
 ('126','S.LINGESAN','MM','tpsd1','Floor 1','21-06-2016',52.57,'2016-06-21'),
 ('74','K.SHENBAGAM','MM','tpsd1','Floor 1','19-08-2016',43.17,'2016-08-19'),
 ('78','E.MUTHUPANDI','MM','tpsd1','Floor 1','21-06-2016',57.81,'2016-06-21'),
 ('120','K.LOGANATHAN','MM','tpsd1','Floor 1','21-06-2016',50.53,'2016-06-21'),
 ('174','V.SHANMUGAM','MM','tpsd1','Floor 1','21-06-2016',44.67,'2016-06-21');
/*!40000 ALTER TABLE `amco_result` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
