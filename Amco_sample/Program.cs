﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Data;

namespace Amco_sample
{
    
    public class SignUp
    {
        public String emailId { get; set; }
        public String password { get; set; }
        public Boolean isDoctor { get; set; }
        public Boolean isPatient { get; set; }
        public Boolean isSocialLogin { get; set; }

        

        public static explicit operator HttpContent(SignUp v)
        {
            throw new NotImplementedException();
        }
    }

    class Variables
    {
        public static Logger amco_log;
    }
    
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]


        //Form2 frm2;
        
        static void Main()
        {
           // RESTART:
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);


                Form1 f1 = new Form1();

               // Variables.amco_log = new Logger();
               /// Variables.amco_log.openLog();
                if (!f1.setup(f1))
                {
                    return;
                    // f1.Close();
                }
                else
                    Application.Run(f1);

            }
            catch( Exception ex)
            {
                MessageBox.Show(ex.Message);

              //  MessageBox.Show(" Do not Click on Graph windows.. ");

                Application.Exit();
               // goto RESTART;
                
            }

        }
      
    }
}
