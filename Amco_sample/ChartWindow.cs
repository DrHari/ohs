﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Amco_sample
{
    public partial class ChartWindow : Form
    {
        String Title;
        public ChartWindow()
        {
            InitializeComponent();
        }

        public void setup(Series S1, String title )
        {
            Title = title;
            //chart1.Titles.Add(title);
            Title title1 = chart1.Titles.Add(title);
            title1.Font = new System.Drawing.Font("Arial", 14, FontStyle.Bold);
            title1.ForeColor = System.Drawing.Color.FromArgb(0, 0, 205);

            this.chart1.BorderlineColor = Color.Red;
            this.chart1.BorderlineWidth = 2;   
            this.chart1.ForeColor = Color.DarkViolet;


            S1.MarkerStyle = MarkerStyle.Circle;

            chart1.Series.Add(S1);
            chart1.Series[0].XValueType = ChartValueType.DateTime;
            S1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;


            chart1.ChartAreas[0].AxisX.IsMarginVisible = true; // change this , if you want, x-axi min , max values are close to the data points
            chart1.ChartAreas[0].AxisX.IsLabelAutoFit = true;
            //chart1.ChartAreas[0].AxisX.LabelStyle.Interval = 1;
            chart1.Invalidate();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "JPeg Image|*.jpg";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.OverwritePrompt = true;
            saveFileDialog1.FileName = Title;
            saveFileDialog1.DefaultExt = "jpg";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.

            if (saveFileDialog1.FileName != "")

            {
               chart1.SaveImage(saveFileDialog1.FileName, ChartImageFormat.Jpeg);
                            

                }
            

            }

            
        
    }
}
