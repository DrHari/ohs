﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amco_sample
{
    public partial class HazardSummary : Form
    {
        Form1 parent;
        Amco_HR myAmco;
        String selectedFloor = "", selectedLocation = "", selectedsite = "";
        amco_params p = new amco_params();
        DateTime aDay;
        TimeSpan daysOLD;
        MoreAnalysis analysisMore;
        public HazardSummary()
        {
            InitializeComponent();
        }
        public void Init(Form1 f, Amco_HR amco)
        {
            parent = f;
            myAmco = amco;
           
        }

        public void setListItems()
        {
            // set UI elements value
            radioButton1.Checked = true;
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            

            //add all locations 
            if (myAmco.connection == null) myAmco.connection.Open();

            // read Parameters
            MySqlCommand cmd;
            MySqlDataReader reader;

            cmd = myAmco.connection.CreateCommand();

            cmd.CommandText = "SELECT  * FROM amco_hr.Amco_Hazard_param ";
            //cmd.ExecuteNonQuery();

            reader = cmd.ExecuteReader();
            int readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {

                   // amco_params p = new amco_params();
                    p.paramID = reader.GetInt32(0);
                    p.paramName = reader.GetString(1);

                    listBox1.Items.Add(p.paramName);

                    // MessageBox.Show("row " + p.empID);
                    readRow++;

                }
            }
            else
            {
                MessageBox.Show("No rows found.for parameter Table..");
                Variables.amco_log.LogMessage(myAmco.amco_user, "No rows found.for parameter Table..in SetListItems of Hazard");
                return;
            }
            reader.Close();
            listBox2.Items.Clear();
            // now fill the list of locations

            cmd = myAmco.connection.CreateCommand();
            cmd.CommandText = "select distinct location from amco_emp order by location";
            reader = cmd.ExecuteReader();
            readRow = 0;
            String Location = "";
            if (reader.HasRows)
            {
                while (reader.Read())
                {


                    Location = reader.GetString(0);

                    listBox2.Items.Add(Location);

                    readRow++;

                }
            }
            else
            {
                MessageBox.Show("No rows found.for the locations....");
                Variables.amco_log.LogMessage(myAmco.amco_user, "No rows found.for the locations... of Hazard");
                return;
            }
            reader.Close();
            // now disable the listbox3 and 4...
            // later enable them and show the result, based on the item selected earlier
            listBox3.Items.Add("ALL Sites");
            listBox4.Items.Add("ALL Department");
            button2.Enabled = true;

            //ENSURE all first items are selected by default
            listBox1.SelectedIndex = 0;  // Hazard Param
            listBox2.SelectedIndex = 0;  // Location

            listBox3.SelectedIndex = 0;  //site
            listBox4.SelectedIndex = 0;  // dept
           




        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox3.Items.Clear();
            listBox3.Items.Add("ALL Sites");
            listBox3.SelectedIndex = 0;  //site
            listBox4.SelectedIndex = 0;  // dept
            // fill sites for the seleccted location
            selectedLocation = listBox2.SelectedItem.ToString();
            if (selectedLocation == "") { MessageBox.Show("select the location.."); return; }
            //now based on the location, select the site
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = myAmco.connection.CreateCommand();
            cmd.CommandText = "select distinct site from amco_emp where location = \"" + selectedLocation + "\" order by site;";
            reader = cmd.ExecuteReader();
            int readRow = 0;
            String site = "";
            if (reader.HasRows)
            {
                while (reader.Read())
                {


                    site = reader.GetString(0);

                    listBox3.Items.Add(site);

                    readRow++;

                }
            }
            else
            {
                MessageBox.Show("No rows found.for the locations....");
                Variables.amco_log.LogMessage(myAmco.amco_user, "No rows found.for the locations...listbox 2 of Hazard Summary");
                return;
            }
            reader.Close();

        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox4.Items.Clear();
            listBox4.Items.Add("ALL Department");
            listBox4.SelectedIndex = 0;  // dept
            // fill depatrments for the selected location & site
            selectedLocation = listBox2.SelectedItem.ToString();
            selectedsite = listBox3.SelectedItem.ToString();
            //if (selectedLocation == "") { MessageBox.Show("selcte the location first.."); return; }
            if (selectedsite == "") { MessageBox.Show("select the site .."); return; }
            //now based on the location, select the site
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = myAmco.connection.CreateCommand();
            if (selectedsite != "ALL Sites")
            {
                cmd.CommandText = "select distinct floor from amco_emp where location = \"" + selectedLocation + "\" && site = \"" + selectedsite + "\" order by floor;";

                reader = cmd.ExecuteReader();
                int readRow = 0;
                String site = "";
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {


                        site = reader.GetString(0);

                        listBox4.Items.Add(site);

                        readRow++;

                    }
                }
                else
                {
                    MessageBox.Show("No rows found.for the site....");
                    return;
                }
                reader.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           // MessageBox.Show("closing R graphs..& More Analysis");
            parent.Close_R_graphs();
            if(analysisMore != null) analysisMore.Close();
            Close();
            
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            aDay = DateTime.Now;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
             daysOLD = new System.TimeSpan(30, 0, 0, 0);
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            daysOLD = new System.TimeSpan(60, 0, 0, 0);
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            daysOLD = new System.TimeSpan(90, 0, 0, 0);
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            daysOLD = new System.TimeSpan(180, 0, 0, 0);
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            daysOLD = new System.TimeSpan(360, 0, 0, 0);
        }

        private int CountofPeopleInRegionOfInterest(String askquery)
        {
            int totalEmp = 0;
            if (myAmco.connection == null) myAmco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = myAmco.connection.CreateCommand();
            cmd.CommandText = askquery;  // "select count(empID) from amco_emp " + askquery;
            
            reader = cmd.ExecuteReader();
            int readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Amco_Person p = new Amco_Person();

                    totalEmp = reader.GetInt32(0);

                    readRow++;

                }
            }
            else
            {
                MessageBox.Show("No rows found.");
                
            }
            reader.Close();
            return totalEmp;
        }
        private int CountOfDistinctBLLdone(String askquery)
        {
            if (myAmco.connection == null) myAmco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = myAmco.connection.CreateCommand();
            cmd.CommandText =  "select count(distinct empID) from amco_result " + askquery;
            int countOfDistinctBLLdone = 0;
            reader = cmd.ExecuteReader();
            int readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Amco_Person p = new Amco_Person();

                    countOfDistinctBLLdone = reader.GetInt32(0);
                                    
                    readRow++;

                }
            }
            else
            {
                MessageBox.Show("No rows found.");
            }
            reader.Close();
            return countOfDistinctBLLdone;

        }
        private int CountOfBLLdone(String askquery)
        {
            if (myAmco.connection == null) myAmco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = myAmco.connection.CreateCommand();
            cmd.CommandText = "select count(empID) from amco_result " + askquery;
            int countOfBLLdone = 0;
            reader = cmd.ExecuteReader();
            int readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Amco_Person p = new Amco_Person();

                    countOfBLLdone = reader.GetInt32(0);

                    readRow++;

                }
            }
            else
            {
                MessageBox.Show("No rows found.");
            }
            reader.Close();
            return countOfBLLdone;

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            hazardRange.Text = "Enter "+ listBox1.SelectedItem.ToString() +" Range ";
            //based on the hazard parameter set the lower and higher values
            textBox1.Text ="0";
            textBox2.Text = "100";
        }

        private void MoreAnalysis(String fileNamewithPath, String askquery, String startDate, String endDate, List<Amco_Person> reportBasedOnPersons, List<Amco_Person> personDataNotavailable)
        {
            Variables.amco_log.LogMessage(myAmco.amco_user, "Entering More Analytics..");
            //askquery can give the lsit of person matching this location, site, dept
            // & for the given  bll range..
            if (myAmco.connection == null) myAmco.connection.Open();
            MySqlCommand cmd;
            //MySqlDataReader reader;

            cmd = myAmco.connection.CreateCommand();
           
            cmd.CommandText = " DELETE FROM amco_result";
            cmd.ExecuteNonQuery();


            cmd = myAmco.connection.CreateCommand();
            cmd.CommandText = "load data local infile \""+ fileNamewithPath + "\"  into table Amco_Result FIELDS TERMINATED BY ',' IGNORE 1 LINES ";
            cmd.ExecuteNonQuery();

            cmd = myAmco.connection.CreateCommand();
            cmd.CommandText = " update amco_result   set mydate = str_to_date(tempdate,'%d-%m-%Y')";
            cmd.ExecuteNonQuery();

            // AFTER THIS one can query, all the relevant data required  from amco_result TABLE
            // such as, last BLL check done, who are all BLL is high, which dept etc..

            // MessageBox.Show("More for analysis: Period ."+ startDate +".. TO.." + endDate + "last BLL check done, who are all BLL is high, which dept etc..");

            // Get the distinct number of people ,for whom BLL done in this period
            int cBLL = CountOfDistinctBLLdone(askquery);
            int cTotal_BLL = CountOfBLLdone(askquery);
            //Get the Total number of people in that selected location, site, dept..
            //TBD remember to change this.. bcos, below askquery, uses bll to get from amco_emp.. this part of string to be removed .. that is used of BLL range
            int total = CountofPeopleInRegionOfInterest("select count(empID) FROM amco_hr.amco_emp");
            double percentCovered = (double) cBLL /(double) total * 100;

            // Report 1 : No of people for whom BLL test done in the reporting period
           // MessageBox.Show("No of employees BLL done for the period  " + startDate + ".. TO.." + endDate  + " is = "+ cBLL);

          //  MessageBox.Show("No of employees in the selected Region for the period  " + startDate + ".. TO.." + endDate + " is = " + total);

           // MessageBox.Show("BLL test coverage for the period  " + startDate + ".. TO.." + endDate + " is = " + percentCovered.ToString() +"%");

            analysisMore = new MoreAnalysis();
           // passing the first level analysis
            List<String> information = new List<String>();
            information.Add("The Reporting Period is " +startDate + ".. TO.." + endDate + " from "+askquery);
            information.Add("No of of Times , test done for  BLL (during this period -Distinct employees) =  "  + cBLL);
            information.Add("Total number of employees(Registered with Beinge)  =  " + total);
            information.Add("BLL test coverage for the period  "+ percentCovered.ToString() + "%");
            information.Add("Total number of times, BLL test done (during this period) " + cTotal_BLL);
            
            analysisMore.setup(information, reportBasedOnPersons, personDataNotavailable,parent, myAmco);
            analysisMore.Enabled = true;
            analysisMore.Show();


            // report on, who are those people    USE : reportBasedOnPersons

            // can we show %age of people BLL done with respect to the location, site, dept
            // report of who is in which bucket of BLL range..  
            //atleast show people who are in higher range and do correlation analysis
            // report on, their trend  or can see in other screen

            // at the end here is where delete the file
            File.Delete(fileNamewithPath);

            
            
        }
        private void button2_Click(object sender, EventArgs e)
        {

            String hazardLower = textBox1.Text;
            String hazardHigher = textBox2.Text;

            selectedLocation = listBox2.SelectedItem.ToString() ;// @"MM";
            selectedsite =listBox3.SelectedItem.ToString();//@"tpsd1";
            selectedFloor = listBox4.SelectedItem.ToString();// @"f1";

            //  get from RadioButton values and convert to date now, 30days, 60 days, 3 months, 6 months, 1 year

            DateTime endDay = aDay.Subtract(daysOLD);
            aDay = DateTime.Now;
            
            String startDate = endDay.ToString("dd-MM-yyyy"); //"06-10-2015";
            String endDate = aDay.ToString("dd-MM-yyyy"); ; //"16-10-2016";
            String queryString;
            string askquery;
            List<Amco_Person> personDataNotavailable = new List<Amco_Person>();  //TBD make this as LIst
            List<Amco_Person> reportBasedOnPersons = new List<Amco_Person>();   //TBD make this as List
            // hard coded for BLL.. later pick for ListBox1 use param ID
            //getBLLParameterID()   from DB
            int whichParameter = 278; //  for bLL use 278;

            if (selectedsite == "ALL Sites" && selectedFloor == "ALL Department")
            {
                queryString = "select * from amco_emp where location = \"" + selectedLocation + "\" ";
                askquery = "  where location = \"" + selectedLocation + "\" ";
            }
            else if (selectedFloor == "ALL Department")
            {
                queryString = "select * from amco_emp where location = \"" + selectedLocation + "\" && site = \"" + selectedsite + "\" ";
                askquery = "  where location = \"" + selectedLocation + "\" && site = \"" + selectedsite + "\" ";
            }

            else
            {
                queryString = "select * from amco_emp where location = \"" + selectedLocation + "\" && site = \"" + selectedsite + "\" && floor= \"" + selectedFloor + "\"";
                askquery = "  where location = \"" + selectedLocation + "\" && site = \"" + selectedsite + "\" && floor= \"" + selectedFloor + "\"";
            }

            // add a condition  that, these list should be registered in Beinge.
            queryString += " && registereWithBeinge=1";
            // askquery += " && registereWithBeinge=1";

            //add hazard parameter range
            askquery += " && BLL >= '" + hazardLower + "' && BLL <= '" + hazardHigher + "'";
            List<Amco_Person> person2get = new List<Amco_Person>();
            int res = myAmco.getList(person2get, queryString);

           
           
            
            // MessageBox.Show("Number of persons in the selected to view .."+res);
            // String path = "F:/Learning/Amco_sample/Amco_sample/bin/Debug/";
            String path = "d:/";
            int result = 1;
             // checking only for BLL id.. now the data is stored in ID=7 @weight in VitalParam

            //if i get , then , whiel writing it use param name not param ID !! // TBD.. need to get this parameter ID from user dialog or use from a fixed list

            // TBD also have to get the period for which the reports requied.
            // even one can select here for the group or a person or can do as below for all the people..
            // if group, the file opening should come before the loop


            String fname = "amco_output_BLL_" + selectedLocation + selectedsite + selectedFloor + ".csv";
            String fileNamewithPath = path + fname;
            StreamWriter fs = new System.IO.StreamWriter(fileNamewithPath);
            //MessageBox.Show("Getting BLL Values for the Employee in " + selectedLocation + selectedsite + selectedFloor);
            String writeLine = "empid,empname,location,site,floor,Date,BLL";

            fs.WriteLine(writeLine);
            int atleastOnegot = 0;
            // disable the button2 .. so that, user will NOT click..know  change lable too..
            button2.Text = "Getting BLL data...";
            button2.Enabled = false;
            button1.Enabled = false;
            foreach (Amco_Person person in person2get)
            {

            

                result = parent.get_BLL_data(person, whichParameter, fs, startDate, endDate);
                if (result == 0)
                {
                   // MessageBox.Show("No data available on Parameter " + whichParameter + " - at this time with BEINGE for the EMP ID " + person.empID);
                    // fs.Close();
                     personDataNotavailable.Add(person);

                }
                else
                {
                    // fs.Close();
                    atleastOnegot++;
                    reportBasedOnPersons.Add(person); 
                    // now show the R analytics on this file for this person..

                }



            }// end of for loop
           // MessageBox.Show(" OutputFile stored in" + fileNamewithPath);
            fs.Close();
            // if result == 0 means, no data available for any one. so delete the file
            if (atleastOnegot == 0)
            {
                MessageBox.Show(" No Data available for the period "+ startDate + " TO "+ endDate + " ..on these people for this Parameter.. "+whichParameter);

            }
            else
            {
                //MessageBox.Show(" Analytics on BLL is based on the People  " + reportBasedOnPersons +")");
               // MessageBox.Show(" BLL data not available for this people :  " + personDataNotavailable + ")");
               // commented to avoid the R- grpah
               // parent.use_R_View_on_BLL(fileNamewithPath);

                //TBD .. here is where we have to use Windows Charting for BLL
                ChartWindowsBLL BLLwindow = new ChartWindowsBLL();
                BLLwindow.setup(myAmco);
                BLLwindow.Show();

                // MessageBox.Show(".. Here we have to show the List of Emp ids, with their BLL Range mapping..How to show");
                MoreAnalysis(fileNamewithPath, askquery, startDate,endDate, reportBasedOnPersons, personDataNotavailable);
                
            }

            button2.Enabled = true;
            button2.Text = "Show ";
            button1.Enabled = true;

        }
    }
}
