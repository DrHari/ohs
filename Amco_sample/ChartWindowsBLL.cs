﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Amco_sample
{
    public partial class ChartWindowsBLL : Form
    {
        Amco_HR amco;
        public ChartWindowsBLL()
        {
            InitializeComponent();
        }
        public void draw_donut_graph_BLLdata()
        {
            Series S1 = new Series();
            List<double> data = new List<double>();
           // String[] legendtexts = { "above 80", "60 to 80", "40 to 60", " less than 40" };
            String[] legendtexts = { " less than 40", "40 to 60", "60 to 80", "above 80" };
            if (amco.connection == null) amco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;

            cmd = amco.connection.CreateCommand();
           // cmd.CommandText = "select count(distinct empID) from amco_result where BLL>80";
           // cmd.CommandText = "select count(distinct empID) from amco_result where BLL>80 union select count(distinct empID) from amco_result where BLL >60 && BLL < 80 union select count(distinct empID) from amco_result where BLL > 40 && BLL < 60 union select count(distinct empID) from amco_result where BLL > 0 && BLL < 40";
            cmd.CommandText = " select count(distinct empID) from amco_result where BLL > 0 && BLL < 40 union select count(distinct empID) from amco_result where BLL > 40 && BLL < 60 union   select count(distinct empID) from amco_result where BLL >60 && BLL < 80  union  select count(distinct empID) from amco_result where BLL>80 ";
            reader = cmd.ExecuteReader();
            int readRow = 0;
            if (reader.HasRows)
            {
                 while (reader.Read())
                {
                    data.Add(reader.GetInt32(0));
                    readRow++;

                }
            }
            reader.Close();
            /*
            cmd = amco.connection.CreateCommand();
            cmd.CommandText = "select count(distinct empID) from amco_result where BLL>60 && BLL <80";
            reader = cmd.ExecuteReader();
            readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    data.Add(reader.GetInt32(0));
                    readRow++;

                }
            }
            reader.Close();

            cmd = amco.connection.CreateCommand();
            cmd.CommandText = "select count(distinct empID) from amco_result where BLL>40 && BLL <60";
            reader = cmd.ExecuteReader();
            readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    data.Add(reader.GetInt32(0));
                    readRow++;

                }
            }
            reader.Close();



            cmd = amco.connection.CreateCommand();
            cmd.CommandText = "select count(distinct empID) from amco_result where BLL>0 && BLL <40";
            reader = cmd.ExecuteReader();
            readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    data.Add(reader.GetInt32(0));
                    readRow++;

                }
            }
            reader.Close();
            */

            for (int i = 0; i < data.Count; i++)
            {
                S1.Points.AddXY(data[i].ToString(), data[i]);
                S1.Points[i].LegendText = legendtexts[i];

            }
            S1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;

          
            chart1.Titles.Add("BLOOD LEAD LEVEL (Location)");
            chart1.Series.Add(S1);
            chart1.Invalidate();


            


        }
        public void draw_Bar_graph_SiteWise_BLL()
        {
            String[] legendtexts = { " less than 40", "40 to 60", "60 to 80", "above 80" };

            String[] siteBLLQuery = { "BLL>0 && BLL<=40", "BLL>40 && BLL<=60", "BLL>60 && BLL<=80", "BLL>80" };
            List<String> siteList = new List<String>();
            chart2.Titles.Add("BLOOD LEAD LEVEL (Site wise) ");
            
            if (amco.connection == null) amco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = amco.connection.CreateCommand();
            // get the list of sites
            cmd.CommandText = "select distinct site from amco_result sort order by site";
            reader = cmd.ExecuteReader();
            int readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    siteList.Add(reader.GetString(0));
                    readRow++;

                }
            }
            reader.Close();
            int nSeries = 0;
            foreach (string query in siteBLLQuery)
             {
                Series S = new Series();
                List<double> data = new List<double>();

                S.Name = legendtexts[nSeries];
                foreach (string site in siteList)
                {
                    cmd = amco.connection.CreateCommand();

                    cmd.CommandText = "select count(empID)  from amco_result where " + query + "  && site='" + site + "'";
                    reader = cmd.ExecuteReader();
                    readRow = 0;
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            data.Add(reader.GetInt32(0));
                            readRow++;

                        }
                    }
                    reader.Close();
                }

                if (nSeries == 0)
                {
                    for (int i = 0; i < siteList.Count; i++)
                    {
                        S.Points.AddXY(siteList[i], data[i]);

                        S.Points[i].Label = data[i].ToString();
                    }

                }
                else
                {
                    for (int i = 0; i < siteList.Count; i++)
                    {
                        S.Points.AddY(data[i]);
                        S.Points[i].Label = data[i].ToString();
                    }

                }

                S.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                chart2.Series.Add(S);
                nSeries++;
            }
            chart2.Invalidate();
        
        }
        public void draw_Bar_graph_SiteWise_BLL_OLD()
        {
            
            List<String> siteList = new List<String>();
            
            Series S2 = new Series();
            
            Series S3 = new Series();
            Series S4 = new Series();
            Series S5 = new Series();

            String[] legendtexts = { " less than 40",  "40 to 60", "60 to 80", "above 80" };
            String[] siteBLLQuery = { "BLL>0 && BLL<=40", "BLL>40 && BLL<=60", "BLL>60 && BLL<=80", "BLL>80" };

            S2.Name = legendtexts[0];
            S3.Name = legendtexts[1];
            S4.Name = legendtexts[2];
            S5.Name = legendtexts[3];

            if (amco.connection == null) amco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = amco.connection.CreateCommand();
            // get the list of sites
            cmd.CommandText = "select distinct site from amco_result sort order by site";
            reader = cmd.ExecuteReader();
            int readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    siteList.Add(reader.GetString(0));
                    readRow++;

                }
            }
            reader.Close();

            // for each of the site, get the BLL values 0 to 40, 40-60, 60-80, >80 then store them in each series
            List<double> data_less40 = new List<double>();
            foreach (string site in siteList)
            {
                cmd = amco.connection.CreateCommand();

                cmd.CommandText = "select count(empID)  from amco_result where BLL>0 && BLL<40 && site='"+site+"'";
                reader = cmd.ExecuteReader();
                readRow = 0;
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        data_less40.Add(reader.GetInt32(0));
                        readRow++;

                    }
                }
                reader.Close();
            }

            List<double> data_40_60 = new List<double>();
            foreach (string site in siteList)
            {
                cmd = amco.connection.CreateCommand();

                cmd.CommandText = "select count(empID)  from amco_result where BLL>40 && BLL<60 && site='" + site + "'";
                reader = cmd.ExecuteReader();
                readRow = 0;
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        data_40_60.Add(reader.GetInt32(0));
                        readRow++;

                    }
                }
                reader.Close();
            }
            List<double> data_60_80 = new List<double>();
            foreach (string site in siteList)
            {
                cmd = amco.connection.CreateCommand();

                cmd.CommandText = "select count(empID)  from amco_result where BLL>60 && BLL<80 && site='" + site + "'";
                reader = cmd.ExecuteReader();
                readRow = 0;
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        data_60_80.Add(reader.GetInt32(0));
                        readRow++;

                    }
                }
                reader.Close();
            }

            List<double> data_above80 = new List<double>();
            foreach (string site in siteList)
            {
                cmd = amco.connection.CreateCommand();

                cmd.CommandText = "select count(empID)  from amco_result where BLL>80 && site='" + site + "'";
                reader = cmd.ExecuteReader();
                readRow = 0;
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        data_above80.Add(reader.GetInt32(0));
                        readRow++;

                    }
                }
                reader.Close();
            }
            for (int i = 0; i < siteList.Count; i++)
            {
                S2.Points.AddXY(siteList[i], data_less40[i]);
                S2.Points[i].Label = data_less40[i].ToString();
                S3.Points.AddY( data_40_60[i]);
                S3.Points[i].Label = data_40_60[i].ToString();
                S4.Points.AddY(data_60_80[i]);
                S4.Points[i].Label = data_60_80[i].ToString();
                S5.Points.AddY(data_above80[i]);
                S5.Points[i].Label = data_above80[i].ToString();

            }
            S2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            S3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            chart2.Titles.Add("BLOOD LEAD LEVEL (Site wise) ");
            chart2.Series.Add(S2);
            chart2.Series.Add(S3);
            chart2.Series.Add(S4);
            chart2.Series.Add(S5);
            chart2.Invalidate();

        }
        public void draw_Bar_graph_DeptWise_BLL()
        {
            String[] legendtexts = { " less than 40", "40 to 60", "60 to 80", "above 80" };

            String[] siteBLLQuery = { "BLL>0 && BLL<=40", "BLL>40 && BLL<=60", "BLL>60 && BLL<=80", "BLL>80" };
            List<String> deptList = new List<String>();
            chart3.Titles.Add("BLOOD LEAD LEVEL (Department wise) ");
            chart3.ChartAreas["ChartArea1"].AxisX.Interval = 1;

            if (amco.connection == null) amco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = amco.connection.CreateCommand();
            // get the list of depts
            cmd.CommandText = "select distinct floor from amco_result sort order by floor";
            reader = cmd.ExecuteReader();
            int readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    deptList.Add(reader.GetString(0));
                    readRow++;

                }
            }
            reader.Close();
            int nSeries = 0;
            foreach (string query in siteBLLQuery)
            {
                Series S = new Series();
                List<double> data = new List<double>();

                S.Name = legendtexts[nSeries];
                foreach (string site in deptList)
                {
                    cmd = amco.connection.CreateCommand();

                    cmd.CommandText = "select count(empID)  from amco_result where " + query + "  && floor='" + site + "'";
                    reader = cmd.ExecuteReader();
                    readRow = 0;
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            data.Add(reader.GetInt32(0));
                            readRow++;

                        }
                    }
                    reader.Close();
                }

                if (nSeries == 0)
                {
                    for (int i = 0; i < deptList.Count; i++)
                    {
                        S.Points.AddXY(deptList[i], data[i]);

                        S.Points[i].Label = data[i].ToString();
                    }

                }
                else
                {
                    for (int i = 0; i < deptList.Count; i++)
                    {
                        S.Points.AddY(data[i]);
                        S.Points[i].Label = data[i].ToString();
                    }

                }

                S.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                chart3.Series.Add(S);
                nSeries++;
            }
            chart3.Invalidate();

        }

        public void setup(Amco_HR myAmco)
        {
            // use this myAmco , to connect to db and get required data from amco_result table and use them to display graphs
            // for whom we got the BLL data
            amco = myAmco;
          
           
          
            //List<double> data = new List<double>();
            draw_donut_graph_BLLdata();
            
            draw_Bar_graph_SiteWise_BLL();
            draw_Bar_graph_DeptWise_BLL();

            
        }
        [Serializable]
        public class MyModel
        {
            public byte[][] Images { get; set; }
        }

      


private void button1_Click(object sender, EventArgs e)
        {

            String fname = "";
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "JPeg Image|*.jpg";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.OverwritePrompt = true;
            saveFileDialog1.FileName = @"BLoodLeadLevel_"+DateTime.Now.ToString("dd-MM-yyyy");
          //  saveFileDialog1.DefaultExt = "jpg";
           // saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName != "")

            {
               fname = saveFileDialog1.FileName;
            }
            else { return; }

            string fname1 = fname + "_Location" + ".jpg"; // @"d:\image1.jpg";
            string fname2 = fname + "_Site" + ".jpg"; //@"d:\image2.jpg";
            string fname3 = fname + "_Department" + ".jpg"; // @"d:\image3.jpg";
            // If the file name is not an empty string open it for saving.
            chart1.SaveImage(fname1, ChartImageFormat.Jpeg);
            chart2.SaveImage(fname2, ChartImageFormat.Jpeg);
            chart3.SaveImage(fname3, ChartImageFormat.Jpeg);
            MessageBox.Show("Save done successfully.. ");
            button1.Enabled = false;
            
            /*
            var model = new MyModel
            {
                Images = new[]
                {
                    System.IO.File.ReadAllBytes(fname1),
                    System.IO.File.ReadAllBytes(fname2),
                    System.IO.File.ReadAllBytes(fname3),
                   }
            };
            if (saveFileDialog1.FileName != "")

            {
                String fname = saveFileDialog1.FileName;
                //chart1.SaveImage(fname, ChartImageFormat.Jpeg);
                var formatter = new BinaryFormatter();
                using (var output = System.IO.File.Create(fname))
                {
                    formatter.Serialize(output, model);
                }


            }
            */
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
