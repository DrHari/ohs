﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amco_sample
{
    public partial class uploadOHS : Form
    {
        Form1 parent;
        Amco_HR myAmco;
        List<Amco_Person> pList;
        public uploadOHS()
        {
            InitializeComponent();
        }
        public void Init(Form1 f, Amco_HR amco)
        {
            parent = f;
            myAmco = amco;
            panel1.Enabled = false;
            panel1.Visible = false;
        }
        private void uploadHAZARDParametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(" List down Hazard Parameter.. pick form DB and upload them from excel. now only BLL..");
            panel1.Enabled = false;
            panel1.Visible = false;

        }

        private void uploadMedicalRecordsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(" Upload Medical Records to Beinge..");
            String query = "select * from amco_emp ";
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            panel1.Enabled = true;
            panel1.Visible = true;
            button1.Enabled = false;
            pList = new List<Amco_Person>();
            
            query += " where registereWithBeinge=1";
            
            int nEmp= myAmco.getList(pList, query);
            for (int l = 0; l < pList.Count; l++)
            {
                listBox1.Items.Add(pList[l].empID + "\t" + pList[l].empName);
                comboBox1.Items.Add(pList[l].empName +" ( "+pList[l].empID + " )"  );
               
             
            }
            listBox1.SelectedIndex = 0;
            comboBox1.SelectedIndex = 0;
            listBox1.Visible = false;
            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bLLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Upload BLL Parameter

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory =  @"F:/Learning/Amco_sample/Amco_sample/bin/Debug/";
            openFileDialog1.Filter = "xls files (*.xls)|*.xlsx";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    string fname = openFileDialog1.FileName;
                    //MessageBox.Show("Here we upload the BLL records of AMCO employee......");
                    int countBLL_uploaded = parent.read_BLL_Value_from_excel_and_Upload(fname);

                    MessageBox.Show("BLL is uploaded for " + countBLL_uploaded + "records");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(" will Upload Medical record of the Employee.." +listBox1.SelectedItem );
            //Select Emp id = selected id , document type, date of report
            // for the selected Emp iD, get his email id, pwd, Beingperosn iD etc..
            int empRowSelected = listBox1.SelectedIndex;
            String documentType = listBox2.SelectedItem.ToString();
            // combo box item select logi
            String selectedEmp = comboBox1.SelectedItem.ToString();
            // extract empId from the above string , after ( and until ) and trim that.
            int start= selectedEmp.IndexOf('(');
            int end = selectedEmp.IndexOf(')');
            string selectedEempID= selectedEmp.Substring(start + 1, end - start-1).Trim();
           // MessageBox.Show(selectedEempID);
            // for the  emp id, get the index from pList , for which pList[index] matches to empID
            int index = -1;
            for (index = 0; index < pList.Count; index++)
                if (pList[index].empID == selectedEempID) break;
            if(index== -1) { MessageBox.Show("something surprise happend..selected employee ID is not appearing.. ");return; }
            empRowSelected = index;
            Amco_Person p = pList[empRowSelected];
            DateTime dt1 = Convert.ToDateTime(dateTimePicker1.Text, System.Globalization.CultureInfo.InvariantCulture);
           
            String docDate = dt1.ToString("dd-MM-yyyy hh:mm tt");
            
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory =  @"F:/Learning/Amco_sample/Amco_sample/bin/Debug/";
            openFileDialog1.Filter = "pdf files (*.pdf)|*.pdf";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    string fname = openFileDialog1.FileName;
                   // pass Amco_person, document type, date of document, filename
                   int res=  parent.UpLoadMedicalRecord(pList[empRowSelected], documentType,docDate, fname);

                   if(res == 1)
                    {
                        // file uploaded successfully
                        MessageBox.Show(" File Upload SUCESSFULLY.." + fname);
                    }
                   else
                    {
                        MessageBox.Show(" File Upload NOT completed.. error in loading of file .." + fname);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }

            }

            // see how to handle better for the user interface..
           // button1.Enabled = false;



            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            comboBox1.Items.Clear();
            listBox2.Items.Clear();
            panel1.Enabled = false;
            panel1.Visible = false;
            Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.Enabled = true;

            // update this listBox2  with doucment types.
            listBox2.Items.Add("Laboratory Report");
            listBox2.Items.Add("Prescription");
            listBox2.Items.Add("Master Health Check Report");
            listBox2.SelectedIndex = 0;
            
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
