﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using RestSharp;
using MySql.Data.MySqlClient;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using RDotNet;
using RDotNet.NativeLibrary;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Windows.Forms.DataVisualization.Charting;


namespace Amco_sample
{

    public partial class Form1 : Form
    {
        static HttpClient client = new HttpClient();
        
        ViewReports viewReport;
        Registration regFrm;
        uploadOHS uploadForm;
        HazardSummary hazardSummary;
        REngine engine;
        Amco_HR amco;
        public bool validUserLoggedIn = false;
        //String initialDirectory = @"F:/Learning/Amco_sample/Amco_sample/bin/Debug/";
        public Form1()
        {
            InitializeComponent();
            
            viewReport = new ViewReports();
            regFrm = new Registration();
            uploadForm = new Amco_sample.uploadOHS();
            amco = new Amco_HR();
            hazardSummary = new HazardSummary();
           

        }

        public static void Setup_R_Path(string Rversion = "R-3.3.2")
        {
            var oldPath = System.Environment.GetEnvironmentVariable("PATH");
            var rPath = System.Environment.Is64BitProcess ?
                                   string.Format(@"C:\Program Files\R\{0}\bin\x64", Rversion) :
                                   string.Format(@"C:\Program Files\R\{0}\bin\i386", Rversion);
            var newPath = string.Format("{0}{1}{2}", rPath, System.IO.Path.PathSeparator, oldPath);
            /*
            if (!Directory.Exists(rPath))
                throw new DirectoryNotFoundException(string.Format(" R.dll not found in : {0}", rPath));
            var newPath = string.Format("{0}{1}{2}", rPath,
                                         System.IO.Path.PathSeparator, oldPath);
                                         */
            System.Environment.SetEnvironmentVariable("PATH", newPath);
        }

        public void check_Beinge_Sign_Log_in()
        {

            List<Amco_Person> persons2login = new List<Amco_Person>();
            int count2Log = amco.getListRegister2LogIn(persons2login);
            if (count2Log != 0)
            {
                //MessageBox.Show("AMCO employees registered with BEINGE but not yet signed with BEINGE..");
                Variables.amco_log.LogMessage(amco.amco_user, "AMCO employees registered with BEINGE but not yet signed with BEINGE..");
               // MessageBox.Show("going to login one by one of " + count2Log.ToString());
                int n2login = SignedUpButNotLogedIn(persons2login);
                MessageBox.Show("Successfully logged with BEINGE.. ( Health on your HAND)  " + n2login.ToString() + "out of " + count2Log.ToString());
                Variables.amco_log.LogMessage(amco.amco_user, "Successfully logged with BEINGE.. (Health on your HAND)  " + n2login.ToString() + "out of " + count2Log.ToString());

            }
            //get the list of employees, who are NOT registered with BEINGE, register them now.


            List<Amco_Person> persons2register = new List<Amco_Person>();
            int no2register =amco.getList2Register2Beinge(persons2register, 0);
            if (no2register != 0)
            {
                Variables.amco_log.LogMessage(amco.amco_user, "AMCO -non-registered employees( new Users) Signing up with BEINGE..");

               // MessageBox.Show("AMCO -non-registered employees( new Users) Signing up with BEINGE..");
               // MessageBox.Show("going to register one by one of " + no2register.ToString());
                // returns int value, which is number sign up successfully done
                int n =SignUp_withBeinge(persons2register);
                MessageBox.Show("Successfully registered with BEINGE.. ( Health on your HAND)  " + n.ToString() + " out of " + no2register.ToString());
                Variables.amco_log.LogMessage(amco.amco_user, "Successfully registered with BEINGE.. ( Health on your HAND)  " + n.ToString() + " out of " + no2register.ToString());
                // is this the place to update their profile
            }
        }
        public bool setup(Form1 f1)
        {
            if (!amco.validUser)
            {
                MessageBox.Show(" Not an authorized user.. Contact administrator");
                Variables.amco_log.LogMessage(amco.amco_user,"UnAuthorized access");
            }
           
            else {
                Variables.amco_log.LogMessage(amco.amco_user, "Logged in ..");
                // check is there any persons to be logged in and signed in with Beinge, from teh amco_local database
                check_Beinge_Sign_Log_in();

                viewReport.Init(f1);
            regFrm.Init(f1, amco);
            uploadForm.Init(f1, amco);
            hazardSummary.Init(f1, amco);

            //setting up the R environment
            // MessageBox.Show(" Setting R environment");
           
            // setting R
            Setup_R_Path();
            engine = REngine.GetInstance();
            engine.Initialize();
                Variables.amco_log.LogMessage(amco.amco_user, "Form - setup Completed..");
            }
            return amco.validUser;
        }

        public String LoginPage(Amco_Person p)
        {
            
            //MessageBox.Show("entered Log in Page..");
            Variables.amco_log.LogMessage(amco.amco_user,"entered Log in Page..for." + p.empID);
            
            var webAddr = "https://www.beinge.com/";
            RestClient _client;
            string _url = webAddr;
            _client = new RestClient(_url);
            var request = new RestRequest("beinge-webapp/api/user/login", Method.POST) { RequestFormat = DataFormat.Json };



            //adding header
            request.AddHeader("x_beinge_email_id", p.empEmail);
            MD5CryptoServiceProvider crypt = new MD5CryptoServiceProvider();
            UTF8Encoding encoder = new UTF8Encoding();
            Byte[] cryptPwd = crypt.ComputeHash(encoder.GetBytes(p.passWrd));

            string result = BitConverter.ToString(cryptPwd).Replace("-", "").ToLowerInvariant().Trim();

            request.AddHeader("x_beinge_password", result);
            request.AddHeader("Authorization", "pHoDOZkcmvvJhTn0M2d9ig==");
            //request.AddHeader("x_beinge_id", p.Being_PersonID);
            request.AddHeader("x_beinge_is_doctor", "false");
            request.AddHeader("x_beinge_is_social_login", "false");
            // adding json
            request.AddJsonBody(new
            {
                emailId = p.empEmail,
                password = result,
                isDoctor = false,
                isSocialLogin = false
            });

            IRestResponse logInResponse = _client.Execute(request);

            String beingeID = null;
            if (logInResponse.StatusCode == 0)
            {
                // return new ResponseResultDTO() { IsSuccess = false, Message = "Server not found", StatusCode = 2082 };
                MessageBox.Show("Server not found.. Log in is Not sccess");
                Variables.amco_log.LogMessage(amco.amco_user,"Server not found whild logging for " + p.empID);
               beingeID = "";

            }
            else
            {
                //show the responses received..

                // TBD  ERROR at this point .. not sure, why response is Unsupported media or something ??
               // MessageBox.Show(logInResponse.Content);

                dynamic data = JObject.Parse(logInResponse.Content);
                // data has all the personal profile data.. can use that later.. now need only userDetails.id
                //TBD for profile related display..

                beingeID = data.userDetails.id.ToString();



            }
            return beingeID;   // returns the Beinge ID if success.
        }
        public String SignUpPage(String emailId, string pwd)
        {
            //MessageBox.Show("entered Sign up Page..");
            Variables.amco_log.LogMessage(amco.amco_user,"entered Sign up Page. for " + emailId);
            
            var webAddr = "https://www.beinge.com/beinge-webapp/api/user/signup";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";

            MD5CryptoServiceProvider crypt = new MD5CryptoServiceProvider();
            UTF8Encoding encoder = new UTF8Encoding();
            Byte[] cryptPwd = crypt.ComputeHash(encoder.GetBytes(pwd));

            string result = BitConverter.ToString(cryptPwd).Replace("-", "").ToLowerInvariant().Trim();//System.Text.Encoding.UTF8.GetString(cryptPwd);


            var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream());

            string json = "{\"emailId\": \"" + emailId + "\"" + ", \"password\":\"";
            json += result;
            json += "\", \"isDoctor\": \"false\",\"ignoreEmailVerification\": \"true\",\"invite\":\" \" }";

            streamWriter.Write(json);
            //MessageBox.Show(json);
            //streamWriter.Flush();
            streamWriter.Close();


            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            var streamReader = new StreamReader(httpResponse.GetResponseStream());

            var response = streamReader.ReadToEnd();

            //MessageBox.Show(response);
            // return the response string success status
            return response.ToString();




        }
        public String insert_BeingeID2amcoTable(Amco_Person p)
        {
            // String personID = LoginPage(p);
            //insert this personID, against the empID of AMCO into the table
            MySqlCommand cmd;
            cmd = amco.connection.CreateCommand();

            cmd.CommandText = "UPDATE amco_emp SET BeingePersonID=@cmdVar1 where empID=" + p.empID;
            cmd.Parameters.AddWithValue("@cmdVar1", p.Being_PersonID);
            cmd.ExecuteNonQuery();
            return p.Being_PersonID;

        }
        private void updateBeingeRegistrationStatus(Amco_Person p)
        {
            // statu update to 1 from 0, for this person in AMCO db
            MySqlCommand cmd;
            cmd = amco.connection.CreateCommand();

            cmd.CommandText = "UPDATE amco_emp SET registereWithBeinge=1 where empID=" + p.empID;

            cmd.ExecuteNonQuery();
        }
        public int SignedUpButNotLogedIn(List<Amco_Person> persons2login)
        {
            // persons registered with BEINGE but not logged in first time and have not received the BEINGE ID
            int countofLoged = 0;

            foreach (Amco_Person person in persons2login)
            {

                // MessageBox.Show("logging into Beinge for  " + person.empEmail );
                Variables.amco_log.LogMessage(amco.amco_user,"logging into Beinge for  " + person.empEmail);
                string loginResponse = LoginPage(person);
                if (loginResponse != null)
                {
                    person.Being_PersonID = loginResponse;
                    String beingeID = insert_BeingeID2amcoTable(person);
                    //MessageBox.Show("This Person ID received from Beinge is " + beingeID);
                    Variables.amco_log.LogMessage(amco.amco_user,"This Person ID received from Beinge is " + beingeID);
                    countofLoged++;
                }
            }



            return countofLoged;
        }
        public int SignUp_withBeinge(List<Amco_Person> persons2register)
        {
            int count = 0;

            foreach (Amco_Person person in persons2register)
            {
                string signupResponse = SignUpPage(person.empEmail, person.passWrd);
               // MessageBox.Show("Sign up of " + person.empEmail + " is successfully DONE..");
               
                Variables.amco_log.LogMessage(amco.amco_user,"Sign up of " + person.empEmail + " is successfully DONE..");
                //TBD on succesful sign up  update the AMCO db with registeredWithBeinge=1 for that person
                updateBeingeRegistrationStatus(person);
                //for the new person signed up, login now and get his BeingeID and insert into the db
                string loginResponse = LoginPage(person);
                // for each of this successful sign, we should login to Beinge, and the response has 
                /*
                 * isSuccess" : {Boolean},
                    "statusCode": { Integer},
                "message" : { String},
                “patientDetails” : {
                            “id” : { Integer},
                            ..
                            ...
                        */
                // we should use  patientDetails.ID and store into mySQL amco_emp table into the last column
                person.Being_PersonID = loginResponse;
                String beingeID = insert_BeingeID2amcoTable(person);

                // MessageBox.Show("This Person ID received from Beinge is " + beingeID);
                Variables.amco_log.LogMessage(amco.amco_user,"This Person ID received from Beinge is " + beingeID);
                // on successful sign in, then login, then have to update the Name of the person using get profile and set profile

                profileResponse pRes = getProfileInfo(person);
                // parse the json respons and create personProfile object, set the values into person
                // then call upDateProfile(person, personProfile)
                pRes.profile.fullName = person.empName;
                uploadProfileInfo(person, pRes);
            
            count++;
            }

            // returns no of successfull registration done
            return count;
        }
        private void button1_Click(object sender, EventArgs e)
        {
          
            regFrm.ShowDialog();
            

        }

        private void button7_Click(object sender, EventArgs e)
        {
            //  Application.Exit();
            Close();

        }
        public int UpLoadMedicalRecord(Amco_Person p, String docType, String docDate, String fileNamewithPath)
        {
            // getting Amco_person, document type, date of document, filename
            // remember to change the doc type and values
            //  Prescription 1
            //  Laboratory Report 2
            //  Master Health Check Report 15

            int documentTypeValue = 2;
            int fileUploadStatus = 0;
            var webAddr = "https://www.beinge.com/";
            RestClient _client;
            string _url = webAddr;
            _client = new RestClient(_url);

            var request = new RestRequest("beinge-webapp/api/patient/uploadMedicalRecordFile", Method.POST) { RequestFormat = DataFormat.Json };
            request.AddHeader("x_beinge_email_id", p.empEmail);

            MD5CryptoServiceProvider crypt = new MD5CryptoServiceProvider();
            UTF8Encoding encoder = new UTF8Encoding();
            Byte[] cryptPwd = crypt.ComputeHash(encoder.GetBytes(p.passWrd));

            string result = BitConverter.ToString(cryptPwd).Replace("-", "").ToLowerInvariant().Trim();//System.Text.Encoding.UTF8.GetString(cryptPwd);
            request.AddHeader("x_beinge_password", result);
            request.AddHeader("Authorization", "pHoDOZkcmvvJhTn0M2d9ig==");
            request.AddHeader("x_beinge_id", p.Being_PersonID);
            request.AddHeader("x_beinge_is_doctor", "false");
            request.AddHeader("x_beinge_is_social_login", "false");

            request.AddFile("file", fileNamewithPath);

            IRestResponse response = _client.Execute(request);




           // MessageBox.Show(response.Content);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                //upload successfull
               // MessageBox.Show(fileNamewithPath + "is Upload completed succesfully...\n" + response.Content);

                // is this , where i have to use API 15
                //http://www.beinge.com/beinge-webapp/api/patient/encounter

                dynamic data = JObject.Parse(response.Content);
                String serverFile = data.filePath;
                String serverThumbnail = data.thumbNailPath;
                // create REST request
                request = new RestRequest("beinge-webapp/api/patient/encounter", Method.POST) { RequestFormat = DataFormat.Json };
                request.AddHeader("x_beinge_email_id", p.empEmail);

                cryptPwd = crypt.ComputeHash(encoder.GetBytes(p.passWrd));

                result = BitConverter.ToString(cryptPwd).Replace("-", "").ToLowerInvariant().Trim();//System.Text.Encoding.UTF8.GetString(cryptPwd);
                request.AddHeader("x_beinge_password", result);
                request.AddHeader("Authorization", "pHoDOZkcmvvJhTn0M2d9ig==");
                request.AddHeader("x_beinge_id", p.Being_PersonID);
                request.AddHeader("x_beinge_is_doctor", "false");
                request.AddHeader("x_beinge_is_social_login", "false");

                request.AddJsonBody(new
                {
                    urls =   new String[] { serverFile },
                    startDate = docDate ,
                    documentType = documentTypeValue,   // 16  what are the codes ?  16 is for pdf file.. 
                    episode = new { name ="AMCO_test", startDate=docDate},
                    thumbNailPath= serverThumbnail
                });

                response = _client.Execute(request);
                //MessageBox.Show(response.Content);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    //upload successfull
                    fileUploadStatus =1;  // file uploaded successfully..
                }



                }
                else
                {
                    //error ocured during upload
                    MessageBox.Show(response.StatusCode + "\n" + response.StatusDescription);
                Variables.amco_log.LogMessage(amco.amco_user,response.StatusCode + "\n" + response.StatusDescription);
                fileUploadStatus = 0;  // failed .. not able to upload the document..
                }


            return fileUploadStatus;

        }

        private int UpLoad_BLL_Record(Amco_Person p , String date, String ParamValue)
        {
            int BLL_ID = 278;  //TO CHANGE .. currently using 7 "Weight",
            String ParameterName = @"BLL";  
            String episodeName = @"AMCO 2 Episode";

            int returnCode = 0;   // 0 on failuer    1 is on success
                                  //here connect to beinge and use REST api call to insert the BLL
            DateTime dt1 = Convert.ToDateTime(date); //, System.Globalization.CultureInfo.InvariantCulture);
           
            string recordDate = dt1.ToString("dd-MM-yyyy hh:mm tt"); // + " 00:01 PM";
           
            var webAddr = "https://www.beinge.com/";
            RestClient _client;
            string _url = webAddr;
            _client = new RestClient(_url);
            var request = new RestRequest("beinge-webapp/api/general/vital", Method.POST) { RequestFormat = DataFormat.Json }; ;
            
            //adding header
            request.AddHeader("x_beinge_email_id", p.empEmail);
            MD5CryptoServiceProvider crypt = new MD5CryptoServiceProvider();
            UTF8Encoding encoder = new UTF8Encoding();
            Byte[] cryptPwd = crypt.ComputeHash(encoder.GetBytes(p.passWrd));

            string result = BitConverter.ToString(cryptPwd).Replace("-", "").ToLowerInvariant().Trim();

            request.AddHeader("x_beinge_password", result);
            request.AddHeader("Authorization", "pHoDOZkcmvvJhTn0M2d9ig==");
            request.AddHeader("x_beinge_id", p.Being_PersonID);
            request.AddHeader("x_beinge_is_doctor", "false");
            request.AddHeader("x_beinge_is_social_login", "false");
            // adding json
            
            request.AddJsonBody(new
            {
                vitalId = BLL_ID,
                startDate = recordDate,      //"14-01-2016 05:28 PM" ,
                vitalParams = new System.Collections.Generic.List<Object>()
                {
                    new
                        {
                            name = ParameterName,    
                            value = ParamValue  
                        }
                },
                episode = new
                {
                    name = episodeName,   //"AMCO Episode",
                    startDate = recordDate //"14-01-2016 05:28 PM"  
                }
            });


            IRestResponse vitalAddResponse = _client.Execute(request);

           
            if (vitalAddResponse.StatusCode == 0)
            {
                  MessageBox.Show("Server not found to insert BLL value for "+p.empID+ ".. Log in is Not sccess");

                Variables.amco_log.LogMessage(amco.amco_user,"Server not found to insert BLL value for " + p.empID + ".. Log in is Not sccess");
            }
            else
            {
                //show the responses received..
                //if vitalAddResponse.StatusCode==OK
               
                //MessageBox.Show(vitalAddResponse.Content);

                dynamic data = JObject.Parse(vitalAddResponse.Content);
                // data has all the response  content is   {"isSuccess":true,"statusCode":200,"message":"OK"}
                //TBD 
                if(data.isSuccess == true)
                        returnCode = 1;  // successfully inserted the param value, recieved the response..


            }
          
            return returnCode;
        }

        public int read_BLL_Value_from_excel_and_Upload(String fname)
        {
            Microsoft.Office.Interop.Excel.Application xlApp;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            //TBD TBD
            xlApp = new Microsoft.Office.Interop.Excel.Application();


            //xlWorkBook = xlApp.Workbooks.Open(fname, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, true, 0, true, 1, 0);
            xlWorkBook = xlApp.Workbooks.Open(fname, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            int columns = 0, rows = 0;
            if (xlWorkSheet != null)
            {
                columns = xlWorkSheet.UsedRange.Columns.Count;
                rows = xlWorkSheet.UsedRange.Rows.Count; ;
            }
            // MessageBox.Show("Opened XLS file for importing BLL data.."+ fname);
            Variables.amco_log.LogMessage(amco.amco_user,"Opened XLS file for importing BLL data.." + fname);
            //MessageBox.Show("No of rows in the data " + rows.ToString());
            // MessageBox.Show("No of columns in the data " + columns.ToString());


            int startNo = 64;
            string endRange;
            // Range range;
            //int status_update_column = 5;
            string[] cellValues = new string[100];
            int no_loaded = 0;
            //Verify the excel file is in the format that we need. check on reading the first row.
            //if it not maches, return, after displaying the error message
            //empid	Date	BLL	status	FileName


            String[] ColumnNames = { "Empid", "Emp Name", "Date", "BLL" }; // "status",  "FileName" };
            //Empid	Emp Name	Date	BLL
            /*
            for (int ii = 1; ii <= columns; ii++)
            {
                
               cellValues[ii] = xlWorkSheet.Cells[1, ii].Text.ToString();
                if (cellValues[ii].Trim() != ColumnNames[ii - 1])
                {

                    MessageBox.Show(" File is not in the BLL format, kindly check the format for BLL..");
                    Variables.amco_log.LogMessage(amco.amco_user,"File is not in the BLL format, kindly check the format for BLL..");
                    xlWorkBook.Save();
                    xlWorkBook.Close(true, misValue, misValue);
                    xlApp.Quit();
                    return 0;

                }


            }
            */

            // file is ok on the format .. continue reading..

            
            for (int jRow = 2; jRow <= rows; jRow++)
            {
                // String rowValues = null;
                for (int i = 1; i <= columns; i++)
                {

                    endRange = Convert.ToChar(startNo + i).ToString() + jRow.ToString();
                    // MessageBox.Show(endRange);

                    cellValues[i] = xlWorkSheet.Cells[jRow, i].Text.ToString();
                    // each column is empid,	Date,	BLL,	status  ( after upload, set to 1. initially ZERO),pdf file


                    //rowValues += cellValues[i] + ",";
                }


                    // for the empID ( picked from Column1), get the person details
                  //  if (cellValues[status_update_column] == "0")
                    {
                        // BLL data yet to be updated..
                        // so get person details from AMCO db and call 


                        MySqlCommand cmd;
                        MySqlDataReader reader;
                        if (amco.connection == null) amco.connection.Open();
                        cmd = amco.connection.CreateCommand();

                        cmd.CommandText = "SELECT  * FROM amco_hr.amco_emp where  empID=" + cellValues[1];
                        //cmd.ExecuteNonQuery();

                        reader = cmd.ExecuteReader();
                        int readRow = 0;
                        int ret = 0;
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Amco_Person p = new Amco_Person();

                                p.empID = reader.GetString(0);
                                p.empName = reader.GetString(1);
                                p.empEmail = reader.GetString(2);
                                p.passWrd = reader.GetString(3);
                                p.location = reader.GetString(4);
                                p.site = reader.GetString(5);
                                p.floor = reader.GetString(6);

                                p.registereWithBeinge = reader.GetInt32(7);
                                p.Being_PersonID = reader.GetString(8).Trim();

                                // MessageBox.Show("row " + p.empID);
                                readRow++;
                                ret = UpLoad_BLL_Record(p, cellValues[3], cellValues[4]);  // person, date and BLL value
                                                                                           //TBD keeping excel file opened and writing is an error sometime,  so, keep this array values , close the
                                                                                           //xls file , then reopen & update the values from the array
                                if (ret == 1)
                                {
                                    //xlWorkSheet.Cells[jRow, status_update_column] = "1";
                                    no_loaded++;
                                }
                                else
                                {
                                    MessageBox.Show("Data is not inserted for " + p.empID);
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("No " + cellValues[1] + " such employees found in your record");
                        }
                        reader.Close();




                    }

                 

            } // one row reading done, uploaded into Beinge server
            MessageBox.Show(" Number of BLL records uploaded" + no_loaded);
            xlWorkBook.Save();
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
                 

            //Marshal.ReleaseComObject(xlApp);
            return no_loaded;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            // upload the BLL record of employee ( to test, now we use VITAL PARAMETER uplodae, later test for other parameters too
            uploadForm.ShowDialog();
            // the below code moved into upload Form
           

        }
        private static IEnumerable<JToken> AllChildren(JToken json)
        {
            foreach (var c in json.Children())
            {
                yield return c;
                foreach (var cc in AllChildren(c))
                {
                    yield return cc;
                }
            }
        }

        public int get_BLL_data(Amco_Person p, int parameterID, StreamWriter fs, String startDate, String endDate)
        {
            // get BLL data from BEINGE, on return for each of the recors, write,
            // data along with location, site, floor etc. with date
            //TBD
            int status_code = 0;
            var webAddr = "https://www.beinge.com/";
            RestClient _client;
            string _url = webAddr;
            _client = new RestClient(_url);
            /*  to test on Muthu laptop to address date issue
            DateTime dt1 = Convert.ToDateTime(startDate);
            DateTime dt2 = Convert.ToDateTime(endDate);
            string fromDate = dt1.ToString("dd-MM-yyyy");        //+ " 00:10 PM";
            string toDate = dt2.ToString("dd-MM-yyyy");         //+ " 00:10 PM";

            */
            String fromDate = startDate;
            String toDate = endDate;

            //var request = new RestRequest("beinge-webapp/api/user/trend/values?parameterId=" + parameterID + "&fromDate=06-10-2010&toDate=16-10-2016&isKeyTrend=false&patientId", Method.GET) { RequestFormat = DataFormat.Json };
            var request = new RestRequest("beinge-webapp/api/user/trend/values?parameterId=" + parameterID + "&fromDate=" + fromDate + "&toDate=" + toDate + "&isKeyTrend=false&patientId", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddHeader("x_beinge_email_id", p.empEmail);//"sundar@beinge.com");
            MD5CryptoServiceProvider crypt = new MD5CryptoServiceProvider();
            UTF8Encoding encoder = new UTF8Encoding();
            Byte[] cryptPwd = crypt.ComputeHash(encoder.GetBytes(p.passWrd));

            string result = BitConverter.ToString(cryptPwd).Replace("-", "").ToLowerInvariant().Trim();//System.Text.Encoding.UTF8.GetString(cryptPwd);
            request.AddHeader("x_beinge_password", result);
            request.AddHeader("Authorization", "pHoDOZkcmvvJhTn0M2d9ig==");
            request.AddHeader("x_beinge_id", p.Being_PersonID);
            request.AddHeader("x_beinge_is_doctor", "false");
            request.AddHeader("x_beinge_is_social_login", "false");


            IRestResponse response = _client.Execute(request);
            // MessageBox.Show(response.Content);

            if (response.StatusCode == 0)
            {
                // return new ResponseResultDTO() { IsSuccess = false, Message = "Server not found", StatusCode = 2082 };
                MessageBox.Show("Server not found");
                status_code = 0;

            }
            else
            {
                //show the responses received..
                if (response.Content == "")
                {
                    MessageBox.Show("Data on" + parameterID + " is EMPTY for " + p.empID);
                    return 0;
                }
                dynamic data = JObject.Parse(response.Content);
                // if data has value then proceed further

                string writeLine = "";

                JArray paramValues = data["parameterValues"];
                if (paramValues.First != null)
                {
                    JArray dateValue = data["dates"];
                    // MessageBox.Show("No of parameters "+paramValues.Count.ToString());
                    //TBD  do i need the columns name to be written here, when it comes to more persons..
                    // here i am getting it by one file per person.. is that ok ??

                    // for BLL we need as below
                    //empid	empname	location	site	floor	Date	BLL
                    /*
                    writeLine = "empid,empname,location,site,floor,Date";
                   // writeLine = "EmpID,Date,Time,Noon";
                    string temp = ""; ;
                    int k;
                    for (k = 0; k < paramValues.Count; k++)
                        temp += ",BLL" + k.ToString();
                    writeLine += temp;
                    fs.WriteLine(writeLine);
                    */
                    int k;
                   // writeLine = "";
                    //writeLine += p.empID + ","+p.empName+","+p.location+","+p.site+","+p.floor+",";
                    int nData = paramValues.First.Count();
                    for (int j = 0; j < nData; j++)
                    {
                        writeLine = "";
                        writeLine += p.empID + "," + p.empName + "," + p.location + "," + p.site + "," + p.floor + ",";

                        //MessageBox.Show(paramValues[0][j].ToString());

                        // writeLine += dateValue[j].ToString();
                        //here is what i have split this stirng using 

                        Char delimiter = ' ';
                        String[] substrings = dateValue[j].ToString().Split(delimiter);
                        writeLine += substrings[0]; //   if we want time, minute use this     + "," + substrings[1] + "," + substrings[2];

                        ///
                        for (k = 0; k < paramValues.Count; k++)
                            writeLine += "," + paramValues[k][j].ToString();


                        // MessageBox.Show(" status ="+data.isSuccess.ToString() + "Parameter Values ="+data.parameterValues.ToString());
                        fs.WriteLine(writeLine);
                        writeLine = p.empID + ",";
                    }

                    status_code = 1;
                }
            }


            return status_code;
        }
        int get_DisplayParameterData(Amco_Person p, amco_params ap, StreamWriter fs, String startDate, String endDate, parameterResponse pr)
        {

            // get a particular patient parameter values
            // MessageBox.Show(" Going to get Person Parameter- .." + parameterID.ToString());
            int status_code = 0;
            var webAddr = "https://www.beinge.com/";
            RestClient _client;
            string _url = webAddr;
            _client = new RestClient(_url);
            
            DateTime dt1 = Convert.ToDateTime(startDate);
            DateTime dt2 = Convert.ToDateTime(endDate );
            string fromDate=dt1.ToString("dd-MM-yyyy") ;        //+ " 00:10 PM";
            string toDate= dt2.ToString("dd-MM-yyyy") ;         //+ " 00:10 PM";
            

            //var request = new RestRequest("beinge-webapp/api/user/trend/values?parameterId=" + parameterID + "&fromDate=06-10-2010&toDate=16-10-2016&isKeyTrend=false&patientId", Method.GET) { RequestFormat = DataFormat.Json };
           var request = new RestRequest("beinge-webapp/api/user/trend/values?parameterId=" + ap.paramID + "&fromDate=" + fromDate + "&toDate=" + toDate + "&isKeyTrend=false&patientId", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddHeader("x_beinge_email_id", p.empEmail);//"sundar@beinge.com");
            MD5CryptoServiceProvider crypt = new MD5CryptoServiceProvider();
            UTF8Encoding encoder = new UTF8Encoding();
            Byte[] cryptPwd = crypt.ComputeHash(encoder.GetBytes(p.passWrd));

            string result = BitConverter.ToString(cryptPwd).Replace("-", "").ToLowerInvariant().Trim();//System.Text.Encoding.UTF8.GetString(cryptPwd);
            request.AddHeader("x_beinge_password", result);
            request.AddHeader("Authorization", "pHoDOZkcmvvJhTn0M2d9ig==");
            request.AddHeader("x_beinge_id", p.Being_PersonID);
            request.AddHeader("x_beinge_is_doctor", "false");
            request.AddHeader("x_beinge_is_social_login", "false");
           

            IRestResponse response = _client.Execute(request);
           // MessageBox.Show(response.Content);

            if (response.StatusCode == 0)
            {
                // return new ResponseResultDTO() { IsSuccess = false, Message = "Server not found", StatusCode = 2082 };
                MessageBox.Show("Server not found");
                status_code = 0;

            }
            else
            {
                //show the responses received..
               if(response.Content == "")
                {
                    MessageBox.Show("Data on" + ap.paramID + " is EMPPTY for " +p.empID);
                    return 0;
                        } 
                dynamic data = JObject.Parse(response.Content);
                // if data has value then proceed further
                
                string writeLine = "";
              
                JArray paramValues= data["parameterValues"];
                // begin change setup values in  parameterResponse pr for c# based charting. Call er should send parameterResponse
                JArray legendNames = data["legendaryNames"];
                //parameterResponse pr = new parameterResponse(); //JsonConvert.DeserializeObject<parameterResponse>(response.Content);
                JArray recdDate = data["dates"];
                if (paramValues.First != null)
                {
                    for (int k = 0; k < paramValues.First.Count(); k++)
                {

                    pr.dates.Add( recdDate.ElementAt(k).ToString());
                }
                
                int nParam = paramValues.Count(); // as many as parameter i.  Count () gives number of parameters.
                for (int j = 0; j < nParam; j++)
                {
                    
                    ParameterValues p1 = new ParameterValues();
                    pr.legendarynames.Add(legendNames[j].ToString());
                    for (int k = 0; k < paramValues.First.Count(); k++)
                    {
                       // MessageBox.Show(paramValues[j][k].ToString());
                        p1.param.Add(paramValues[j][k].ToString());
                       // p1.param[k] = paramValues[k][j].ToString();
                    }
                    pr.parameterValues.Add(p1);
                }
                
                //MessageBox.Show(pr.ToString());
                // end change
                status_code = 1;
            }

            // begin comment here , if we need to use parameter class. Below steps to write the parameter valuse into a file
            if (paramValues.First != null)
                {
                    JArray dateValue = data["dates"];
                    // MessageBox.Show("No of parameters "+paramValues.Count.ToString());
                    //TBD  do i need the columns name to be written here, when it comes to more persons..
                    // here i am getting it by one file per person.. is that ok ??

                    // for parameter 1 , we get, empid, date, time, noon, value0(High), value1(low)
                    // below code is for parameter 1 only.. for other parameter have to check the return format
                    writeLine = "EmpID,Date,Time,Noon";
                    string temp = ""; ;
                    int k;
                    for (k = 0; k < paramValues.Count; k++)
                        temp += ", Value" + k.ToString();
                    writeLine += temp;
                    fs.WriteLine(writeLine);
                    writeLine = "";
                    writeLine += p.empID + ",";
                    int i = paramValues.First.Count(); // as many as parameter i.  Count () gives number of parameters
                    for (int j = 0; j < i; j++)
                    {
                        //MessageBox.Show(paramValues[0][j].ToString());

                        // writeLine += dateValue[j].ToString();
                        //here is what i have split this stirng using 

                        Char delimiter = ' ';
                        String[] substrings = dateValue[j].ToString().Split(delimiter);
                        int splitCount = substrings.Length;
                        writeLine += substrings[0];
                        for (int cl = 1; cl < splitCount; cl++)
                            writeLine += "," + substrings[cl];   // + "," + substrings[2];
                        if (splitCount == 1) writeLine += "," + " " + "," + " ";   // is to set time, 
                        ///
                        for (k = 0; k < paramValues.Count; k++)
                            writeLine += "," + paramValues[k][j].ToString();


                        // MessageBox.Show(" status ="+data.isSuccess.ToString() + "Parameter Values ="+data.parameterValues.ToString());
                        fs.WriteLine(writeLine);
                        writeLine = p.empID + ",";
                    }

                    status_code = 1;
                }
                // end here, if we need to use Parameter class
            }
            

            return status_code;

        }
        public void uploadProfileInfo(Amco_Person p, profileResponse pupdate)
        {
           // MessageBox.Show(" Going to Update PROFILE INFO ..");
            var webAddr = "https://www.beinge.com/";
            RestClient _client;
            string _url = webAddr;
            _client = new RestClient(_url);

            var request = new RestRequest("beinge-webapp/api/patient/profile", Method.PUT) { RequestFormat = DataFormat.Json };
            request.AddHeader("x_beinge_email_id", p.empEmail);
            MD5CryptoServiceProvider crypt = new MD5CryptoServiceProvider();
            UTF8Encoding encoder = new UTF8Encoding();
            Byte[] cryptPwd = crypt.ComputeHash(encoder.GetBytes(p.passWrd));

            string result = BitConverter.ToString(cryptPwd).Replace("-", "").ToLowerInvariant().Trim();//System.Text.Encoding.UTF8.GetString(cryptPwd);
            request.AddHeader("x_beinge_password", result);
            request.AddHeader("Authorization", "pHoDOZkcmvvJhTn0M2d9ig==");
            request.AddHeader("x_beinge_id", p.Being_PersonID);
            request.AddHeader("x_beinge_is_doctor", "false");
            request.AddHeader("x_beinge_is_social_login", "false");


            
            String output = JsonConvert.SerializeObject(pupdate);
            request.AddParameter("application/json",output,ParameterType.RequestBody);
           // dynamic reqObject = JsonConvert.DeserializeObject(output);
         // output = request.AddBody(pupdate)
          //  request.AddJsonBody(pupdate);
          //  request.AddJsonBody(reqObject);


            IRestResponse response = _client.Execute(request);
            //MessageBox.Show(response.Content);

            dynamic data = JObject.Parse(response.Content);

        }

            public profileResponse getProfileInfo(Amco_Person p)
        {
            // need to get  email ID, PersonID, pwd
            // get a particular patient profile
           // MessageBox.Show(" Going to get PROFILE INFO ..");
            var webAddr = "https://www.beinge.com/";
            RestClient _client;
            string _url = webAddr; 
            _client = new RestClient(_url);

            var request = new RestRequest("beinge-webapp/api/patient/profile", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddHeader("x_beinge_email_id", p.empEmail); 
            MD5CryptoServiceProvider crypt = new MD5CryptoServiceProvider();
            UTF8Encoding encoder = new UTF8Encoding();
            Byte[] cryptPwd = crypt.ComputeHash(encoder.GetBytes(p.passWrd));  

            string result = BitConverter.ToString(cryptPwd).Replace("-", "").ToLowerInvariant().Trim();//System.Text.Encoding.UTF8.GetString(cryptPwd);
            request.AddHeader("x_beinge_password", result);
            request.AddHeader("Authorization", "pHoDOZkcmvvJhTn0M2d9ig==");
            request.AddHeader("x_beinge_id", p.Being_PersonID);
            request.AddHeader("x_beinge_is_doctor", "false");
            request.AddHeader("x_beinge_is_social_login", "false");


            IRestResponse response = _client.Execute(request);
           // profileResponse pf = _client.Execute< profileResponse>(request);
            //MessageBox.Show(response.Content);
            if (response != null)
            {
                dynamic data = JObject.Parse(response.Content);
                /*
                var settings = new JsonSerializerSettings
                {
                    Error = (sender, args) =>
                    {
                        if (System.Diagnostics.Debugger.IsAttached)
                        {
                            System.Diagnostics.Debugger.Break();
                        }
                    }
                };
                */

                // JsonSerializerSettings serSettings = new JsonSerializerSettings();
                //serSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                profileResponse pf = JsonConvert.DeserializeObject<profileResponse>(response.Content); //, serSettings);    //: new PersonProfile();
               // MessageBox.Show(response.Content);
               // MessageBox.Show(pf.profile.fullName + pf.profile.emailId + pf.profile.mobileNumber + pf.profile.dateOfBirth);
                return pf;

            }
            else { 
                // return new ResponseResultDTO() { IsSuccess = false, Message = "Server not found", StatusCode = 2082 };
                MessageBox.Show("Server not found");
                return null;
            }
            

            // ResponseResultDTO.cs    

           

        }
        public void Close_R_graphs( )
        {
            engine.Evaluate("source(\"Close_R.R\")");
        }
        public void use_R_BLL_view_ONE_Parameter(string fileNamewithPath, string parameterName, string personName)
        {
            if (File.Exists(fileNamewithPath) == false) { MessageBox.Show("File Error" + fileNamewithPath); return; }
            //REngine engine = REngine.GetInstance();
            try
            {
                engine.Initialize();
                char[] arr = fileNamewithPath.ToCharArray();
                CharacterVector fname = engine.CreateCharacterVector(new[] { fileNamewithPath });
                // string PARAM = parameterName;  // set the Parameter Name
                // char[] param = PARAM.ToCharArray();
                CharacterVector param_name = engine.CreateCharacterVector(new[] { parameterName });
                CharacterVector person_name = engine.CreateCharacterVector(new[] { personName });
                engine.SetSymbol("fname", fname);
                engine.Evaluate("fname");
                engine.SetSymbol("Ylable", param_name);
                engine.SetSymbol("PersonName", person_name);
                //engine.Evaluate("source(\"F:/Learning/Amco_sample/Amco_sample/bin/Debug/AMCO_SinglePlot.R\")");
                //engine.Evaluate("source(\"c:/amco_sample/AMCO_SinglePlot.R\")");
                engine.Evaluate("source(\"AMCO_BLL_SinglePlot.R\")");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + "  " + ex.Message);
                // can we close the engine
                engine.Dispose();
                MessageBox.Show("R Engine closing and restarting..");
                engine.Initialize();
            }
        }

        public void use_R_view_ONE_Parameter(string fileNamewithPath, string parameterName, string personName)
            {
            //REngine engine = REngine.GetInstance();
            try
            {
                engine.Initialize();
                char[] arr = fileNamewithPath.ToCharArray();
                CharacterVector fname = engine.CreateCharacterVector(new[] { fileNamewithPath });
                // string PARAM = parameterName;  // set the Parameter Name
                // char[] param = PARAM.ToCharArray();
                CharacterVector param_name = engine.CreateCharacterVector(new[] { parameterName });
                CharacterVector person_name = engine.CreateCharacterVector(new[] { personName });
                engine.SetSymbol("fname", fname);
                engine.Evaluate("fname");
                engine.SetSymbol("Ylable", param_name);
                engine.SetSymbol("PersonName", person_name);
                //engine.Evaluate("source(\"F:/Learning/Amco_sample/Amco_sample/bin/Debug/AMCO_SinglePlot.R\")");
                //engine.Evaluate("source(\"c:/amco_sample/AMCO_SinglePlot.R\")");
                engine.Evaluate("source(\"AMCO_SinglePlot.R\")");
                // engine.Evaluate("print(mychart)");
                // MessageBox.Show("Completed Showing the Analytics..");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + "  " + ex.Message);
            }

        }

        public void use_R_View_on_BP(string fileNamewithPath, string personName)
        {
            // use R enviornment and read this csv filename, and do the analysis, show graphs
            //TBD
            //string rLib = @"C:\\Users\\hari\\Documents\\R\\win-library\\3.3";

            /*...
            string rLibPath = NativeUtility.FindRPath();
            MessageBox.Show("NativeUtility.FindRPath returns "+ rLibPath);
            string rHomePath = NativeUtility.FindRHome(rLibPath);
            MessageBox.Show("NativeUtility.FindRHome returns "+rHomePath);

            using System;
            using RDotNet.NativeLibrary;


            string rHome = null;
            string rPath = null;
            if (args.Length > 0)
                rPath = args[0];
            if (args.Length > 1)
                rHome = args[1];

            var logInfo = NativeUtility.FindRPaths(ref rPath, ref rHome);

            Console.WriteLine("Is this process 64 bits? {0}", System.Environment.Is64BitProcess);
            Console.WriteLine(logInfo);
  
            */

            // REngine.SetEnvironmentVariables("R_LIBS = \"C:\\Users\\hari\\Documents\\R\\win-library\\3.3\"");

            //REngine engine = REngine.GetInstance();
            try
            {
                engine.Initialize();
                char[] arr = fileNamewithPath.ToCharArray();
                CharacterVector fname = engine.CreateCharacterVector(new[] { fileNamewithPath });
                engine.SetSymbol("fname", fname);
                engine.Evaluate("fname");

                CharacterVector person_name = engine.CreateCharacterVector(new[] { personName });
                engine.SetSymbol("PersonName", person_name);
                // engine.Evaluate("source(\"F:/Learning/Amco_sample/Amco_sample/bin/Debug/AMCO_BP_Multiplot1.R\")");
                engine.Evaluate("source(\"AMCO_BP_Multiplot1.R\")");
                // engine.Evaluate("print(mychart)");
                // MessageBox.Show("Completed Showing the Analytics..");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + "  " + ex.Message);
            }






        }
       
        public void displayAnalyticsReport_on_Parameter(amco_params ap, String startDate, String endDate, String queryString)
        {

            // use this query string to get the list of employees matching this string

            List<Amco_Person> person2get = new List<Amco_Person>();
            
            int res=  amco.getList(person2get, queryString);
            if (res == 0)
            {
                MessageBox.Show(" Number of Employees whose records will be fetched = " + res);
                return;
            }
            else
            {
                MessageBox.Show(" Number of Employees whose records will be fetched = " + res);
                // String path = "F:/Learning/Amco_sample/Amco_sample/bin/Debug/";
                String path = "d:/"; // "C:/Amco_sample/";


                int result = 1;
                int whichParameter = ap.paramID;

                // if group, the file opening should come before the loop

                string fname = "amco_output_emp_group_analytics.csv";
                string fileNamewithPath = path + fname;
                MessageBox.Show(fileNamewithPath);
                StreamWriter fs = new System.IO.StreamWriter(fileNamewithPath);

                // String startDate = "06-10-2010";
                 //String endDate = "16-10-2016";
                foreach (Amco_Person person in person2get)
                {
                    /*
                    string fname = "amco_output_emp_" + person.empID + "param_" + whichParameter + ".csv";
                    string fileNamewithPath = path + fname;
                    StreamWriter fs = new System.IO.StreamWriter(fileNamewithPath);
                    */
                    //MessageBox.Show("Getting Values on Parameter " + whichParameter + " for the Employee " + person.empID);
                    // BP =1    gly  =68

              //      result += get_DisplayParameterData(person, ap, fs, startDate, endDate);
                }
                if (result != res)
                {
                    MessageBox.Show("Data received for " + result + " Employees out of " + res);
                    //fs.Close();

                }

                fs.Close();
                MessageBox.Show(" OutputFile stored in" + fileNamewithPath);
                // now show the R analytics on this file for this person..
                switch (whichParameter)
                {
                    case 1:
                        // case for two value graphs
                        use_R_View_on_BP(fileNamewithPath," ");
                        break;

                    default:
                        // case for single parameter graph
                        use_R_view_ONE_Parameter(fileNamewithPath,"paramaeter 1","Name");
                        //MessageBox.Show(" Currently Analytic is shown only for BP.. in proces toshow the analytics for the parameter id " + whichParameter);
                        break;

                }
            }
                }
        
        public void ShowGraph_BLL_Trend_View(Amco_Person p,amco_params ap, String startdate, String enddate, parameterResponse pr)
        {
            // use windows charting lib to show, trend values of BLL
            // pr contains the response for the period
            System.Windows.Forms.DataVisualization.Charting.Series S1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            // System.Windows.Forms.DataVisualization.Charting.Series y = new System.Windows.Forms.DataVisualization.Charting.Series();
            ParameterValues pval = pr.parameterValues[0];

            for(int i=0; i<pr.dates.Count; i++)
            {
                DateTime dt = Convert.ToDateTime(pr.dates[i]);//, System.Globalization.CultureInfo.InvariantCulture);
                double valueY = Convert.ToDouble(pval.param[i]);
                S1.Points.AddXY(dt.ToOADate() + i, valueY);
            }
            S1.LegendText = "Blood Lead Level ";
            ChartWindow cw = new ChartWindow();
            cw.setup(S1, "BLL trend for"+p.empName);
            cw.ShowDialog();

        }
        public void displayAnalytics_on_Person(Amco_Person  p, amco_params ap, String  startdate, String enddate)
        {

            //String path = "F:/Learning/Amco_sample/Amco_sample/bin/Debug/";
            String path = "d:/"; // "C:/Amco_sample/";

            string fname = "amco_output_"+p.empID+"on_param_"+ap.paramID+"_data.csv";
            string fileNamewithPath = path + fname;
           // MessageBox.Show(fileNamewithPath);

          StreamWriter fs = new System.IO.StreamWriter(fileNamewithPath);
            parameterResponse pr = new parameterResponse();
            int result = get_DisplayParameterData(p, ap, fs, startdate, enddate, pr);
                // if result== 0, no data in the file.. have to delete this file     
            fs.Close();
            if (result == 0)
            {
                //TBD
                // the file created is empty.. need to be deleted.
                MessageBox.Show(" No data available for."+ p.empID+ ".. on this parameter : "+ap.paramID );
            }
            else
            {
                //MessageBox.Show(" OutputFile stored in" + fileNamewithPath);
                // now show the R analytics on this file for this person..

                switch (ap.paramID)
                {
                    case 1:
                        // case for two value graphs
                       // use_R_View_on_BP(fileNamewithPath,p.empName);
                        
                        break;
                    case 278:  // case for BLL specifically for that horizontal line
                       // use_R_BLL_view_ONE_Parameter(fileNamewithPath, ap.paramName, p.empName);
                        //use windows charting tool - in below api
                        ShowGraph_BLL_Trend_View(p, ap, startdate, enddate, pr);
                        break;
                    default:
                        // case for single parameter graph
                        //use_R_view_ONE_Parameter(fileNamewithPath,ap.paramName,p.empName);
                        ShowGraph_BLL_Trend_View(p, ap, startdate, enddate, pr);
                        break;

                }
               
            }
            // is this place to delete the file
            File.Delete(fileNamewithPath);
        }



        private void button4_Click(object sender, EventArgs e)
        {
            // view Medical reports of selected employees for the period for that parameter.. 
            // MessageBox.Show("view Medical data of selected employees for the period on the BLL....");
            // TBD here is what we have to select location, site, floor or employee or age wise , exp wise etc..
            // select parameters
            
            viewReport.setListItems(amco);
            viewReport.ShowDialog();
          
            
           
        }
        private int Upload_PDF_for_BLL(String fname)
        {
            Microsoft.Office.Interop.Excel.Application xlApp;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            //TBD TBD
            xlApp = new Microsoft.Office.Interop.Excel.Application();


            xlWorkBook = xlApp.Workbooks.Open(fname, 0, false, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);


            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            int columns = 0, rows = 0;
            if (xlWorkSheet != null)
            {
                columns = xlWorkSheet.UsedRange.Columns.Count;
                rows = xlWorkSheet.UsedRange.Rows.Count; ;
            }
            //MessageBox.Show("Opened XLS file for importing BLL data.." + fname);
            //MessageBox.Show("No of rows in the data " + rows.ToString());
            //MessageBox.Show("No of columns in the data " + columns.ToString());


            int startNo = 64;
            string endRange;
            // Range range;
            int status_update_column = 4;
            string[] cellValues = new string[100];
            int no_loaded = 0;
            for (int jRow = 2; jRow <= rows; jRow++)
            {
                // String rowValues = null;
                for (int i = 1; i <= columns; i++)
                {

                    endRange = Convert.ToChar(startNo + i).ToString() + jRow.ToString();
                    // MessageBox.Show(endRange);

                    cellValues[i] = xlWorkSheet.Cells[jRow, i].Text.ToString();
                    // each column is empid,	Date,	BLL,	status  ( after upload, set to 1. initially ZERO)


                    //rowValues += cellValues[i] + ",";

                }

                // for the empID ( picked from Column1), get the person details
                if (cellValues[status_update_column] == "0")
                {
                    // pdf yet to be updated..
                    // so get person details from AMCO db and call 
                    MySqlCommand cmd;
                    MySqlDataReader reader;
                    if (amco.connection == null) amco.connection.Open();
                    cmd = amco.connection.CreateCommand();

                    cmd.CommandText = "SELECT  * FROM amco_hr.amco_emp where  empID=" + cellValues[1];
                    //cmd.ExecuteNonQuery();

                    reader = cmd.ExecuteReader();
                    int readRow = 0;
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Amco_Person p = new Amco_Person();

                            p.empID = reader.GetString(0);
                            p.empName = reader.GetString(1);
                            p.empEmail = reader.GetString(2);
                            p.passWrd = reader.GetString(3);
                            p.location = reader.GetString(4);
                            p.site = reader.GetString(5);
                            p.floor = reader.GetString(6);

                            p.registereWithBeinge = reader.GetInt32(7);
                            p.Being_PersonID = reader.GetString(8).Trim();

                            // MessageBox.Show("row " + p.empID);
                            readRow++;
                            //TBD
                            //here is what we have to create mail cient, mail to beinge

                           
                            //TBD keeping excel file opened and writing is an error sometime,  so, keep this array values , close the
                            //xls file , then reopen & update the values from the array
                            xlWorkSheet.Cells[jRow, status_update_column] = "1";
                            no_loaded++;
                        }
                    }
                    else
                    {
                        MessageBox.Show("No " + cellValues[1] + " such employees found to update the record");
                    }
                    reader.Close();




                }



            }
            MessageBox.Show(" Number of pdf files uploaded" + no_loaded);
            xlWorkBook.Save();
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();


            //Marshal.ReleaseComObject(xlApp);
            return no_loaded;
        }
        private void button5_Click(object sender, EventArgs e)
        {
            
            MessageBox.Show("OHS operations..- a View & Analyisis Report to OHS and Managment ...");
            //Try Upload PDF files for BLL of AMCO employees
            //String fname = @"F:/Learning/Amco_sample/Amco_sample/bin/Debug/AMCO_BLL_data_to_Import2Beinge.xlsx";
           // int countuploaded = Upload_PDF_for_BLL(fname);

        }
      

        private void button6_Click(object sender, EventArgs e)
        {
            //Doctor Consulation screen, record update, view of records of Employee ...
            //TBD use this for Medical File Upload NOW..
            // later bring a new button and integrate with CPMS

            MessageBox.Show("Doctor Consulation screen, record update, view of records of Employee ...");

            // testing for charts
            
            ChartWindowsBLL BLLwindow = new ChartWindowsBLL();
            BLLwindow.setup(amco);
            BLLwindow.Show();
            
            
            

        }

        /*
        static String LoginPage( )
        {
            MessageBox.Show("entered LoginPage method..");
            //how to se the values and pass the values and to which api ?

            var webAddr = "https://www.beinge.com/beinge-webapp/api/user/login";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";

            MD5CryptoServiceProvider crypt = new MD5CryptoServiceProvider();
            UTF8Encoding encoder = new UTF8Encoding();
            Byte[]cryptPwd = crypt.ComputeHash(encoder.GetBytes("test123"));
            
            string result = BitConverter.ToString(cryptPwd).Replace("-", "").ToLowerInvariant().Trim();//System.Text.Encoding.UTF8.GetString(cryptPwd);
            
            //string result = "098f6bcd4621d373cade4e832627b4f6";
            // MessageBox.Show(result);
            var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream());
            
                string json = "{\"emailId\":\"sundar@beinge.com\", \"password\":\"";
                json+=result;
                json += "\",\"isPatient\":\"true\", \"isDoctor\": \"false\",\"isSocialLogin\":\"false\" }";

                streamWriter.Write(json);
               // MessageBox.Show(json);
                //streamWriter.Flush();
                streamWriter.Close();
            

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
           
            var streamReader = new StreamReader(httpResponse.GetResponseStream());
            
                var response = streamReader.ReadToEnd();
                
               // MessageBox.Show(response);
                return response.ToString();

            // on successful signup, we get the personID and that needs to be inserted into the respective Employee id
            // agsinst the column    BeingePersonID   in AMCO_HR DB .. table amco_emp 


        }
        static async Task RunAsync()
        {
            SignUp person = new SignUp();
            person.emailId = "sundar@beinge.com";
            person.password = "test";
            person.isPatient = true;
            person.isDoctor = false;
            person.isSocialLogin = false;

            client.BaseAddress = new Uri("https://www.beinge.com/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

           // String signResult = await LoginPage();
            MessageBox.Show("entered Runasync method..");
           // MessageBox.Show(signResult);
        }
        */
        public void use_R_View_on_BLL(string fileNamewithPath)
        {

            //string rLib = @"C:\\Users\\hari\\Documents\\R\\win-library\\3.3";

            try
            {

                //  REngine engine = REngine.GetInstance();
                engine.Initialize();
                char[] arr = fileNamewithPath.ToCharArray();
                CharacterVector fname = engine.CreateCharacterVector(new[] { fileNamewithPath });
                engine.SetSymbol("fname", fname);
                engine.Evaluate("fname");

                // engine.Evaluate("source(\"F:/Learning/Amco_sample/Amco_sample/bin/Debug/AMCO_BLL_Analytics.R\")");
                // engine.Evaluate("source(\"c:/amco_sample/AMCO_BLL_Analytics.R\")");
                engine.Evaluate("source(\"AMCO_BLL_Analytics.R\")");
                // engine.Evaluate("print(mychart)");
                // MessageBox.Show("Completed Showing the Analytics..");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + "  " + ex.Message);
            }




        }
        private void button3_Click(object sender, EventArgs e)
        {
            // where are we today wrt to BLL  parameters,etc..
            hazardSummary.setListItems();
            hazardSummary.ShowDialog();
            

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            Close();
        }
    }

}
