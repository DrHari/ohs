library(ggplot2)
library(reshape2)
#fname="F:/Test_BLL.csv"
m3 = read.csv(fname)
m3

BLL_classification=c("Normal", "High","Very High","Alarming")
xx=c("0 - 40", "40-60","60-80","80-100")
BLL_breaks = c(0,40,60,80,100)
colors=c("green","yellow","red","orange")
# insert here
# location wise is Donut graph
#number of locations wise

locations=factor(m3$location)

no_of_locations=length(levels(locations))
dev.new()
par(mfrow=c(2,2))
#for each site, draw pie graph
for (i in 1:no_of_locations) {
  
  locationNow= levels(locations)[i]
  locationData=m3[grep(locationNow, m3$location, ignore.case=T),]
  locationCut = cut( locationData$BLL,breaks=BLL_breaks,labels=BLL_classification)
  locationSpread=table(locationCut)
  c=as.vector(locationSpread)
  dat = data.frame(count=c, category=c(paste(c[1]," in Normal (0-40)"), paste(c[2]," in High (40-60)"), paste(c[3]," in Very High(60-80)"),paste(c[4], " in Alarming( 80 above) ")))
  # Add addition columns, needed for drawing with geom_rect.
  dat$fraction = dat$count / sum(dat$count)
  dat = dat[order(dat$fraction), ]
  dat$ymax = cumsum(dat$fraction)
  dat$ymin = c(0, head(dat$ymax, n=-1))
  
  # Make the plot
  
  p1 = ggplot(dat, aes(fill=category, ymax=ymax, ymin=ymin, xmax=4, xmin=3)) +
    geom_rect() +
    coord_polar(theta="y") +
    xlim(c(0, 4)) +
    theme(panel.grid=element_blank()) +
    theme(axis.text=element_blank()) +
    theme(axis.ticks=element_blank()) +
    annotate("text", x = 0, y = 0, label = paste("Blood Lead Level- in", locationNow)) +
    labs(title="")
  
  print(p1)
  
  
  
  
  #  pie(locationSpread, col=colors,labels = BLL_classification,main= paste("BLL Spread -AMCO",locationNow))
}

#  site wise can be pie


# number of sites wise
sites=factor(m3$site)

no_of_sites=length(levels(sites))
dev.new()
par(mfrow=c(2,2))
#for each site, draw pie graph
for (i in 1:no_of_sites) {
  
  siteNow= levels(sites)[i]
  siteData=m3[grep(siteNow, m3$site, ignore.case=T),]
  siteCut = cut( siteData$BLL,breaks=BLL_breaks,labels=BLL_classification)
  siteSpread=table(siteCut)
  pie(siteSpread, col=colors,labels = BLL_classification,main= paste("BLL Spread -AMCO",siteNow))
}

## floor/Deptwise

# dept wise can be bar

dept=factor(m3$floor)

dat1 <- data.frame(
  sites,dept,m3$BLL)

BLL_Level = cut( dat1$m3.BLL,breaks=BLL_breaks,labels=BLL_classification)
dat2 = data.frame(sites,dept,BLL_Level)
dev.new()
graphTitle= "Spread of BLL- sites & Department for the period"
p2= ggplot(data=dat1, aes(x=sites, y=BLL_Level, fill=dept)) +ggtitle(graphTitle)+
  geom_bar(stat="identity", position=position_dodge(), colour="black")
print(p2)
  