﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Amco_sample
{
    public partial class newUserAdd : Form
    {
        Form1 parent;
        Amco_HR myAmco;
        String selectedLocation = "", selectedSite = "", selectedDept = "";
        bool invalidMail = false;
        public newUserAdd()
        {
            InitializeComponent();
        }

        public void Init(Form1 f, Amco_HR amco)
        {
            parent = f;
            myAmco = amco;
            //initialize the list of locations
            // set the sex
            // disable the button1
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            listBox1.Items.Clear();
            // now fill the list of locations
            if (myAmco.connection == null) myAmco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = myAmco.connection.CreateCommand();
            cmd.CommandText = "select distinct location from amco_emp order by location";
            reader = cmd.ExecuteReader();
            int readRow = 0;
            String Location = "";
            if (reader.HasRows)
            {
                while (reader.Read())
                {


                    Location = reader.GetString(0);

                    listBox1.Items.Add(Location);

                    readRow++;

                }
            }
            else
            {
                MessageBox.Show("No Locations defined......");
                return;
            }
            reader.Close();
           
            listBox1.SelectedIndex = 0;
            listBox2.Enabled = true;
            listBox3.Enabled = false;
            button1.Enabled = false;

            
           

        }
        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        public bool IsValidEmail(string strIn)
        {
            invalidMail = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalidMail)
                return false;

            // Return true if strIn is in valid e-mail format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalidMail = true;
            }
            return match.Groups[1].Value + domainName;
        }
        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox3.Items.Clear();
            listBox3.Enabled = true;
            //string text = listBox1.GetItemText(listBox1.SelectedItem);
            selectedLocation = listBox1.SelectedItem.ToString();
            selectedSite = listBox2.SelectedItem.ToString();
            //if (selectedLocation == "") { MessageBox.Show("selcte the location first.."); return; }
            if (selectedSite == "") { MessageBox.Show("select the site .."); return; }
            //now based on the location, select the site
            if (myAmco.connection == null) myAmco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = myAmco.connection.CreateCommand();

            cmd.CommandText = "select distinct floor from amco_emp where location = \"" + selectedLocation + "\" && site = \"" + selectedSite + "\" order by floor;";
            reader = cmd.ExecuteReader();
            int readRow = 0;
            String floor = "";
            if (reader.HasRows)
            {
                while (reader.Read())
                {


                    floor = reader.GetString(0);

                    listBox3.Items.Add(floor);

                    readRow++;

                }
            }
            else
            {
                MessageBox.Show("No rows found.for the site....");
            }
            reader.Close();
            listBox3.SelectedIndex = 0;
        }
        int verify_input()
        {
            int ret = 0;
            // check emp no  is not already existing. if existing, check the registration status & show
            List<Amco_Person> person2get = new List<Amco_Person>();
            String queryString = "SELECT  * FROM amco_hr.amco_emp where empID= "+textBox1.Text.Trim();
            int count = myAmco.getList(person2get, queryString);
            if(count == 0)
            {
                ret = 1;   // no such emp id exist.. can add an employee
            }
            else
            {
                MessageBox.Show("The Emp iD \t"+textBox1.Text +"  already existing.. YOu can not Add an already registered Employee..");
                textBox1.Focus();
                return 0;
            }

            // check email is in the form required
            if (IsValidEmail(textBox3.Text))
            {
                ret = 1;  //  valid email id   Console.WriteLine("Valid: {0}", emailAddress);
            }
            else
            {
                //Console.WriteLine("Invalid: {0}", emailAddress);
                MessageBox.Show("INvlaid email Format.. Please correct email id..");
                textBox3.Focus();
                return 0;
            }

            // check mobile number is of digits only 10 digit

            ret = 1;


                return ret;  //  if all values are as per required type, return 1  else return 0;

        }
        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedDept = listBox3.SelectedItem.ToString();
            button1.Enabled = true;
            
        }

        private int addPersonToDB(Amco_Person p, int regStatus)
        {
            int ret = 0;
            if (myAmco.connection == null) myAmco.connection.Open();
            MySqlCommand cmd;
            //MySqlDataReader reader;
            cmd = myAmco.connection.CreateCommand();

            cmd.CommandText = "INSERT into amco_emp( empID,empName,empEmail,passWrd,location,site,floor,registereWithBeinge,BeingePersonID) Values(@cmdVar1,@cmdVar2,@cmdVar3,@cmdVar4,@cmdVar5,@cmdVar6,@cmdVar7,@cmdVar8,@cmdVar9)";
            cmd.Parameters.AddWithValue("@cmdVar1",p.empID);
            cmd.Parameters.AddWithValue("@cmdVar2",p.empName);
            cmd.Parameters.AddWithValue("@cmdVar3",p.empEmail);
            cmd.Parameters.AddWithValue("@cmdVar4",p.passWrd);
            cmd.Parameters.AddWithValue("@cmdVar5", p.location);
            cmd.Parameters.AddWithValue("@cmdVar6", p.site);
            cmd.Parameters.AddWithValue("@cmdVar7", p.floor);
            cmd.Parameters.AddWithValue("@cmdVar8", regStatus);  // 0 for not registered  1 for registered
            cmd.Parameters.AddWithValue("@cmdVar9", p.Being_PersonID);
            cmd.ExecuteNonQuery();
            // if above query exectued correctly.. check this value
            ret = 1;
            return ret; // succesfully inserted the data
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
        

        private void textBox4_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //the input is validated and 
           
            if (verify_input() != 1)
            {
               // MessageBox.Show(" all values are not correctly set ");

                button1.Enabled = false;
                return;
            }
            
            // if all input is correct then proceed to add a person
            Amco_Person person = new Amco_Person();
            person.empID = textBox1.Text.Trim();
            person.empName = textBox2.Text;
            person.empEmail = textBox3.Text;//  get from email TextBox.
            person.passWrd = "welcome123"; // should use default pwd for all amco employees or allow them to set by the creator
            String mobileNumber = textBox4.Text;
            /* if( selectedLocation == "ADD A NEW LOCATION") {
             
                show a textbox and accept a value
                selectedLocation=   textBox4.text;       
                 
              }


            */
            person.location = selectedLocation;
            person.site = selectedSite;
            /* if( selectedSite == "ADD A NEW SITE") {
             
                show a textbox and accept a value
                selectedSite=   textBox5.text;       
                 
              }


            */
            person.floor = selectedDept;
            //update Amco_emp database


            //person details inserted into Database

            //sign in with Beinge using email id, pwd
            

           


            String signupResponse = parent.SignUpPage(person.empEmail, person.passWrd);
            MessageBox.Show("Sign up of " + person.empEmail + " is successfully DONE..");
            // TBD - if error, such as email already exist etc.. thro message and correct the email id

            if (signupResponse != "")
            {
                MessageBox.Show("logging into Beinge for  " + person.empEmail);
                // login , get Beinge_Perosn ID

                string loginResponse = parent.LoginPage(person);
                if (loginResponse != "")
                {
                    person.Being_PersonID = loginResponse;  // log in response returns BEING ID
                    MessageBox.Show("This Person ID received from Beinge is " + person.Being_PersonID);
                    if (addPersonToDB(person,1) != 1)
                    {
                        MessageBox.Show(" Error in inserting details into DB after signin(  Being ID not inserted).."); return;
                    }
                    else
                    {
                        
                        //sign in success,  log in success,  database insertion success
                        MessageBox.Show("Signed, Logged in and inserted into table.. "+ person.empID+"\t"+person.empName+"\t"+ person.Being_PersonID);
                        // now update the profile info ( atleast partly, name, dob, mobile, contact ) into Beinge
                        profileResponse pRes= parent.getProfileInfo(person);
                        // parse the json respons and create personProfile object, set the values into person
                        // then call upDateProfile(person, personProfile)
                        pRes.profile.fullName = person.empName;
                        parent.uploadProfileInfo(person, pRes);
                        Close();
                    }



                }
                else
                {
                    // sign in success but not login
                    if (addPersonToDB(person, 0) != 1)
                    {
                        MessageBox.Show(" Error in inserting details into DB after signin(  Being ID not inserted).."); return;
                    }
                    else
                    {

                    }
                    MessageBox.Show("Sign in sucess.. but not login.. ..");
                }
            }
            else
            {
                MessageBox.Show("Not able to Add user into Beinge now.. Failure in Sign IN.. try sometime later..");
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.SelectedIndex = 0;
            selectedLocation = listBox1.SelectedItem.ToString();
            listBox2.Items.Clear();
            listBox2.Enabled = true;

           
            if (selectedLocation == "") { MessageBox.Show("select the location.."); return; }
           
            //now based on the location, select the site
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = myAmco.connection.CreateCommand();
            cmd.CommandText = "select distinct site from amco_emp where location = \"" + selectedLocation + "\" order by site;";
            reader = cmd.ExecuteReader();
            int readRow = 0;
            String site = "";
            if (reader.HasRows)
            {
                while (reader.Read())
                {


                    site = reader.GetString(0);

                    listBox2.Items.Add(site);

                    readRow++;

                }
            }
            else
            {
                MessageBox.Show("No rows found.for the sites....");
                listBox2.Items.Add("ADD A NEW SITE");
            }
            reader.Close();
            
            // on click of this, display a text and insert into db
           // 
            listBox2.SelectedIndex = 0;

            listBox3.Enabled = true;


        }
    }
}
