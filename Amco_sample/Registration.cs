﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amco_sample
{
    public partial class Registration : Form
    {
        Form1 parent;
        Amco_HR myAmco;
        newUserAdd userAdd;
        public Registration()
        {
            InitializeComponent();
            userAdd = new newUserAdd();
        }
        public void Init(Form1 f, Amco_HR amco)
        {
            parent = f;
            myAmco = amco;
        }
        private void uploadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (myAmco.init_data() == 0) return;
            List<Amco_Person> persons2login = new List<Amco_Person>();
            int count2Log = myAmco.getListRegister2LogIn(persons2login);
            if (count2Log != 0)
            {
                MessageBox.Show("AMCO employees registered with BEINGE but not yet signed with BEINGE..");
                MessageBox.Show("going to login one by one of " + count2Log.ToString());
                int n2login = parent.SignedUpButNotLogedIn(persons2login);
                MessageBox.Show("Successfully logged with BEINGE.. ( Health on your HAND)  " + n2login.ToString() + "out of " + count2Log.ToString());
            }
            //get the list of employees, who are NOT registered with BEINGE, register them now.


            List<Amco_Person> persons2register = new List<Amco_Person>();
            int no2register = myAmco.getList2Register2Beinge(persons2register, 0);
            if (no2register != 0)
            {
                MessageBox.Show("AMCO -non-registered employees( new Users) Signing up with BEINGE..");
                MessageBox.Show("going to register one by one of " + no2register.ToString());
                // returns int value, which is number sign up successfully done
                int n = parent.SignUp_withBeinge(persons2register);
                MessageBox.Show("Successfully registered with BEINGE.. ( Health on your HAND)  " + n.ToString() + " out of " + no2register.ToString());
                // is this the place to update their profile
            }
              
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" AMCO Employees Registered  with Beinge ..");
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" EDIT a Registered person to change location, site, dept only.. ..Work in progress");
        }

        private void registerNewUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(" New EMployee - Registration with Beinge");
            userAdd.Init(parent, myAmco);
            userAdd.ShowDialog();

        }
    }
}
