﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amco_sample
{

    public partial class ViewReports : Form
    {
        public Form1 parent;
       // int groupID = 0;
        int selectedItem = 0;
        String selectedFloor="", selectedLocation="", selectedsite="";
        int selectedEmprow;
        String paramName = "";
        Amco_HR myAmco;
        List<Amco_Person> person2show;
        List<amco_params> paramList; 

        public ViewReports()
        {
            InitializeComponent();
        }

        public void Init(Form1 f)
        {
            parent = f;
        }
        
        public void setListItems( Amco_HR amco)
        {
            // add the parameters list.. get from table
            myAmco = amco;
            paramList = new List<amco_params>();
            if (myAmco.connection == null) myAmco.connection.Open();
            listBox1.Items.Clear();
            // read Parameters
            MySqlCommand cmd;
            MySqlDataReader reader;
            
            cmd = myAmco.connection.CreateCommand();

            cmd.CommandText = "SELECT  * FROM amco_hr.amco_param ";
            //cmd.ExecuteNonQuery();

            reader = cmd.ExecuteReader();
            int readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                   
                    //amco_params p = new amco_params();
                    int pID = reader.GetInt32(0);
                    String pName = reader.GetString(1);
                    paramList.Add (new amco_params(pID, pName)); 
                   
                    
                    listBox1.Items.Add(paramList[readRow].paramName);

                    // MessageBox.Show("row " + p.empID);
                    readRow++;

                }
            }
            else
            {
                MessageBox.Show("No rows found.for parameter Table..");
            }
            reader.Close();
            listBox2.Items.Clear();
            // now fill the list of locations

            cmd = amco.connection.CreateCommand();
            cmd.CommandText = "select distinct location from amco_emp order by location";
            reader = cmd.ExecuteReader();
            readRow = 0;
            String Location="";
            if (reader.HasRows)
            {
                while (reader.Read())
                {

                    
                    Location  = reader.GetString(0);

                    listBox2.Items.Add(Location);

                   readRow++;

                }
            }
            else
            {
                MessageBox.Show("No rows found.for the locations....");
            }
            reader.Close();
            // now disable the listbox3 and 4...
            // later enable them and show the result, based on the item selected earlier
            listBox1.SelectedIndex = 0;
            listBox2.SelectedIndex = 0;
            listBox3.Enabled = true;
            listBox4.Enabled = false;
            button1.Enabled = false;
           


        }

        private void button2_Click(object sender, EventArgs e)
        {
            // clear the list items
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            listBox5.Items.Clear();
            //MessageBox.Show("closing R graphs..");
            parent.Close_R_graphs();

            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedItem = listBox1.SelectedIndex;
            paramName=listBox1.SelectedItem.ToString();
           
        }
        void updateEmployeeList()
        {
            //list box5 is updated with the employee list
            /*
            DateTime dt1 = Convert.ToDateTime(dateTimePicker1.Text);
            DateTime dt2 = Convert.ToDateTime(dateTimePicker2.Text);
            String startdate = dt1.Date.ToString();
            String enddate = dt2.Date.ToString();
            */
            int paramID = selectedItem ;
            // paramID starts from 1, so increment 1 on slectedItem
            // below is forming a query string to get the EMp ids matching the query, location, site, floor etc..
            String queryString = "select * from amco_emp where location = \"" + selectedLocation + "\" && site = \"" + selectedsite + "\" && floor= \"" + selectedFloor + "\"";
            person2show = new List<Amco_Person>();
            queryString += " && registereWithBeinge=1";
            
            int res = myAmco.getList(person2show, queryString);
            for (int y=0; y< person2show.Count;y++)
                listBox5.Items.Add(person2show[y].empID +"\t"+ person2show[y].empName );
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Variables.amco_log.LogMessage(myAmco.amco_user, "Entering View Reports..");
            //Hari added on 27th November for date cultural invariant
            DateTime dt1 = Convert.ToDateTime(dateTimePicker1.Text); //, System.Globalization.CultureInfo.InvariantCulture);
            DateTime dt2 = Convert.ToDateTime(dateTimePicker2.Text); //, System.Globalization.CultureInfo.InvariantCulture);
            String startdate = dt1.ToString("dd-MM-yyyy");
            String enddate = dt2.ToString("dd-MM-yyyy");
           // String startdate = dt1.Date.ToString();
           // String enddate = dt2.ToString();
            int paramID = selectedItem;

            
                //..now use for one person
                // now disable the buttons so that, user will not click any
                button1.Text = " Getting data...";
                button1.Enabled = false;
                button2.Enabled = false;

                amco_params ap = new amco_params(paramList[paramID].paramID, paramList[paramID].paramName);
                Amco_Person p = new Amco_Person(person2show[selectedEmprow]);
                
                parent.displayAnalytics_on_Person(p,ap, startdate, enddate);
                // use this for group analysitcs
                // enable again..
                button1.Text = " Show ";
                button1.Enabled = true;
                button2.Enabled = true;
            // setting this to address the mouse move issues in R graph window
            this.Focus();
            //shoudl I close this Report form ? will this close R? or how to close the R window
            
                
        }
       
        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox3.Items.Clear();
            listBox3.Enabled = true;

            //string text = listBox1.GetItemText(listBox1.SelectedItem);
            selectedLocation = listBox2.SelectedItem.ToString();
            if (selectedLocation == "") { MessageBox.Show("select the location.."); return; }
            //now based on the location, select the site
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = myAmco.connection.CreateCommand();
            cmd.CommandText = "select distinct site from amco_emp where location = \"" + selectedLocation +"\" order by site;";
                 reader = cmd.ExecuteReader();
            int readRow = 0;
            String site = "";
            if (reader.HasRows)
            {
                while (reader.Read())
                {


                    site = reader.GetString(0);

                    listBox3.Items.Add(site);

                    readRow++;

                }
            }
            else
            {
                MessageBox.Show("No rows found.for the locations....");
            }
            reader.Close();
            listBox3.SelectedIndex = 0;
            listBox4.Enabled = true;
        }

        private void listBox5_SelectedIndexChanged(object sender, EventArgs e)
        {

            selectedEmprow = listBox5.SelectedIndex;
            button1.Enabled = true;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox4.Items.Clear();
            listBox4.Enabled = true;
            //string text = listBox1.GetItemText(listBox1.SelectedItem);
            selectedLocation = listBox2.SelectedItem.ToString();
            selectedsite = listBox3.SelectedItem.ToString();
            //if (selectedLocation == "") { MessageBox.Show("selcte the location first.."); return; }
            if (selectedsite == "") { MessageBox.Show("select the site .."); return; }
            //now based on the location, select the site
            MySqlCommand cmd;
            MySqlDataReader reader;
            cmd = myAmco.connection.CreateCommand();
            cmd.CommandText = "select distinct floor from amco_emp where location = \"" + selectedLocation + "\" && site = \""+selectedsite+"\" order by floor;";
            reader = cmd.ExecuteReader();
            int readRow = 0;
            String floor = "";
            if (reader.HasRows)
            {
                while (reader.Read())
                {


                    floor = reader.GetString(0);

                    listBox4.Items.Add(floor);

                    readRow++;

                }
            }
            else
            {
                MessageBox.Show("No rows found.for the site....");
            }
            reader.Close();
            listBox4.SelectedIndex = 0;
        }

        private void listBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedFloor = listBox4.SelectedItem.ToString();
            listBox1.SelectedIndex = 0;
            listBox5.Items.Clear();
            updateEmployeeList();
        }
    }

}

