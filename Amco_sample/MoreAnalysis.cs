﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amco_sample
{
    public partial class MoreAnalysis : Form
    {
        public List<String> reportMessage;
        int messageRow = 0;
        public List<String> moreMessages;
        public String selectedID = "";
        int CountofAnalysisDone = 10;
        List<Amco_Person> usedPerson, nonUsedPerson;
        Form1 parent;
        Amco_HR amco;
        public MoreAnalysis()
        {
            InitializeComponent();
            reportMessage = new List<String>();
            moreMessages = new List<String>();
        }

        public String analysis_6_BLL_Not_done_on()
        {
            String response = null;  // = new String(); 
            String title = "Test NOT done on BLL for the following persons \n\n";
            response += title;
            for (int kk = 0; kk < nonUsedPerson.Count; kk++)
                response += nonUsedPerson[kk].empID + "\t\t" + nonUsedPerson[kk].empName + "\n";
            
            //show in Grid Table
            /*
            if (amco.connection == null) amco.connection.Open();
            MySqlDataAdapter daAmcoadapter;
            DataSet dsEMpSet;

            daAmcoadapter = new MySqlDataAdapter(sql[i], amco.connection);
            MySqlCommandBuilder cb = new MySqlCommandBuilder(daAmcoadapter);

            dsEMpSet = new DataSet();
            daAmcoadapter.Fill(dsEMpSet, "amco_emp");
            dataGridView1.DataSource = dsEMpSet;
            dataGridView1.DataMember = "amco_emp";
            */
            dataGridView1.DataSource = nonUsedPerson;
            return response;
        }
        public String analysis_5_BLL_spread_0_40()
        {
            // talk to the table, result and get the 

            String response = null;
            String title = "The following persons have BLL  between 0 to 40  \n\n";
            response += title;
            if (amco.connection == null) amco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;

            cmd = amco.connection.CreateCommand();
            cmd.CommandText = "select * from amco_result where BLL >=0 && BLL <=40";
            reader = cmd.ExecuteReader();
            response += prepareResponse(reader);
            
            return response;
        }
        public String analysis_4_BLL_spread_40_60()
        {
            // talk to the table, result and get the 

            String response = null;
            String title = "The following persons have BLL  between 40 to 60  \n\n";
            response += title;
            if (amco.connection == null) amco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;

            cmd = amco.connection.CreateCommand();
            cmd.CommandText = "select * from amco_result where BLL >=40 && BLL <=60";
            reader = cmd.ExecuteReader();
            response += prepareResponse(reader);
          
            return response;
        }
        public String analysis_3_BLL_spread_60_80()
        {
            // talk to the table, result and get the 

            String response = null;
            String title = "The following persons have BLL  between 60 to 80  \n\n";
            response += title;
            if (amco.connection == null) amco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;

            cmd = amco.connection.CreateCommand();
            cmd.CommandText = "select * from amco_result where BLL >=60 && BLL <=80";
            reader = cmd.ExecuteReader();
            response += prepareResponse(reader);
           
            return response;
        }
        public String prepareResponse(MySqlDataReader reader)
        {
            String response="";
            int readRow = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    /*
                    int sentenceBegin = response.Length;
                    int len = response.Length;
                    for (int i = 1; i <= 80 - (len-sentenceBegin); i++)
                        response +="-";
                        */
                    response += reader.GetString(7) + "\t\t"; // date
                    response += reader.GetString(0) + "\t\t";  //emp id

                    response += reader.GetString(6) + "\t\t";    // BLL

                    response += reader.GetString(1) + "\n";// name

                    readRow++;

                }
            }
            else
            {
                response += "None has BLL Greater than 80 \n";
            }
            reader.Close();
            
            return response;

        }
        public String analysis_2_BLL_spread_above80()
        {
            // talk to the table, result and get the 

            String response = null;

            String title = "The following persons have BLL higher than 80   \n\n";
            response += title;

            if (amco.connection == null) amco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;

            cmd = amco.connection.CreateCommand();
            cmd.CommandText = "select * from amco_result where BLL >=80";
            reader = cmd.ExecuteReader();

            response += prepareResponse(reader);
            
            return response;
        }
        public String analysis_1_BLL_done_on()
        {
            String response = null;  // = new String(); 
            String title = "Test done on BLL for the following persons \n\n";
            response += title;
            for (int kk = 0; kk < usedPerson.Count; kk++)
                response += usedPerson[kk].empID + "\t\t" + usedPerson[kk].empName + "\n";
            
            return response;
        }
        public void showInfoOnGrid(int i)
        {
            if (i >= 0 && i <= 4)
            {
                string[] sql = {
                            "select distinct empID as 'Employee ID' ,empName as 'Name',BLL as 'Blod Lead level' ,mydate  as 'Test taken on' ,location as 'Location' ,site as 'Site', floor as 'Department' from amco_result",
                            "select distinct empID as 'Employee ID' ,empName as 'Name',BLL as 'Blod Lead level' ,mydate  as 'Test taken on' ,location as 'Location' ,site as 'Site', floor as 'Department' from amco_result where BLL >=80",
                            "select distinct empID as 'Employee ID' ,empName as 'Name',BLL as 'Blod Lead level' ,mydate  as 'Test taken on' ,location as 'Location' ,site as 'Site', floor as 'Department' from amco_result where BLL >=60 && BLL <=80",
                            "select distinct empID as 'Employee ID' ,empName as 'Name',BLL as 'Blod Lead level' ,mydate  as 'Test taken on' ,location as 'Location' ,site as 'Site', floor as 'Department' from amco_result where BLL >=40 && BLL <=60",
                            "select distinct empID as 'Employee ID' ,empName as 'Name',BLL as 'Blod Lead level' ,mydate  as 'Test taken on' ,location as 'Location' ,site as 'Site', floor as 'Department' from amco_result where BLL >=0 && BLL <=40"
                            };

                //string connStr = "Server= localhost ;Database=amco_hr ;Uid=root;pwd=hari;";
                // MySqlConnection conn = new MySqlConnection(connStr);
                if (amco.connection == null) amco.connection.Open();
                MySqlDataAdapter daAmcoadapter;
                DataSet dsEMpSet;

                daAmcoadapter = new MySqlDataAdapter(sql[i], amco.connection);
                MySqlCommandBuilder cb = new MySqlCommandBuilder(daAmcoadapter);

                dsEMpSet = new DataSet();
                daAmcoadapter.Fill(dsEMpSet, "amco_emp");
                dataGridView1.DataSource = dsEMpSet;
                dataGridView1.DataMember = "amco_emp";
            }
            // case where we can direclty show the non used person from class object
            if(i==5) dataGridView1.DataSource = nonUsedPerson;
            //this will give, BLL not done list..
            //TBD show only , few fields, name, id, location, site, 
            // on selection , have to plan for booking for test, tracking, followup and show, when was the last test done, trend etc..
        }
        public void doMoreAnalysis()
        {
            //here is where, you do more analysis and store the result in moreMessages. using reult table query and analyis using R etc & sing transition table of employees
            CountofAnalysisDone = 0;
            // find the Unique Number of employees for whom BLL done for this period
            

            //for ( k = 0; k < CountofAnalysisDone; k++)
            moreMessages.Add(analysis_1_BLL_done_on());
            CountofAnalysisDone++;
            // repeat the above two line for every new analysis gets added..
            
            moreMessages.Add(analysis_2_BLL_spread_above80());
            CountofAnalysisDone++;

            moreMessages.Add(analysis_3_BLL_spread_60_80());
            CountofAnalysisDone++;

            moreMessages.Add(analysis_4_BLL_spread_40_60());
            CountofAnalysisDone++;

            moreMessages.Add(analysis_5_BLL_spread_0_40());
            CountofAnalysisDone++;

            moreMessages.Add(analysis_6_BLL_Not_done_on());
            CountofAnalysisDone++;
            
        }

        public String DistinctEmpBLLdone()
        {
            String distinctList="";
            String[] sql2 =
                     {
                        "select count(distinct empID) from amco_result",
                        "select count(empID) from amco_result"
                    };
            if (amco.connection == null) amco.connection.Open();
            MySqlCommand cmd;
            MySqlDataReader reader;
            
            cmd = amco.connection.CreateCommand();
            cmd.CommandText = sql2[0];
            reader = cmd.ExecuteReader();
            int CountEmpBLLdone = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    CountEmpBLLdone = reader.GetInt32(0); // Count of distinct employees for whom BLL done
                  

                }
            }
            reader.Close();
            distinctList = " Number of Uniques Employees for whom BLL done = " + CountEmpBLLdone.ToString() + "\n";
            // get the total employee count
            cmd = amco.connection.CreateCommand();
            cmd.CommandText = sql2[1];
            reader = cmd.ExecuteReader();
            int totalEmp = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    totalEmp = reader.GetInt32(0); // Count of distinct employees for whom BLL done


                }
            }
            reader.Close();
            distinctList += " Total number of Employees " + totalEmp.ToString() + "\n";

            distinctList += " Percentage of Coverage for the period = " + ((double) CountEmpBLLdone / (double)totalEmp) * 100 + " % ";
            return distinctList;
        }
        public void setup(List<String> message, List<Amco_Person> reportBasedOnPersons, List<Amco_Person> personDataNotavailable,Form1 form1, Amco_HR myAmco)
        {
            messageRow = 0;
            button2.Enabled = false;
            button3.Enabled = true;
            button4.Visible = false;
            usedPerson = reportBasedOnPersons;
            nonUsedPerson = personDataNotavailable;
            parent = form1;
            amco = myAmco;
            for ( int i=0; i<message.Count;i++)  reportMessage.Add(message[i]);

            reportMessage.Add(DistinctEmpBLLdone());

            richTextBox1.Text = "";
            textBox1.Text = reportMessage[0];           // The Reporting Period is 
            richTextBox1.Text = "";

            for (int ll = 0; ll < reportMessage.Count; ll++)
            {
                richTextBox1.AppendText("\n" + reportMessage[ll]);
            }
            /*
                richTextBox1.AppendText("\n" + reportMessage[1]);   // No of of Times , test done for  BLL(during this period) 
            richTextBox1.AppendText("\n" + reportMessage[2]);   //No of employees in the selected Region for the period
            richTextBox1.AppendText("\n" + reportMessage[3]);   //BLL test coverage for the period

            reportMessage.Add(DistinctEmpBLLdone());
           richTextBox1.AppendText("\n" + reportMessage[4]);
            */



            doMoreAnalysis();
            // richTextBox2.AppendText("\n" + ".. More analysis will be shown here.. such as , people whose BLL used, not used, any correlation etc");



        }
        private void button1_Click(object sender, EventArgs e)
        {
            Close();

            parent.Close_R_graphs(); 
           
        }

       public void checkBoundaries()
        {
            if(messageRow <= 0) { button2.Enabled = false; button3.Enabled = true;  messageRow = 0; }
            if(messageRow >= CountofAnalysisDone-1) { button3.Enabled = false; button2.Enabled = true;  messageRow = CountofAnalysisDone-1 ; }
            if(messageRow >0 && messageRow < CountofAnalysisDone-1) { button2.Enabled = true;  button3.Enabled = true; }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // next
            messageRow++;
            checkBoundaries();
            richTextBox2.Text = moreMessages[messageRow];
            showInfoOnGrid(messageRow);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex > -1)
            {
                button4.Visible = true;
                selectedID = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                button4.Text = " Click for More Info/action on : "+ selectedID;
                
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(selectedID != "")
            {
                // show the individual record of the selected EmpID 
                // plan for booking for a lab report, 
                // show how this individual BLL is wrt to the Movement record and show his personal profile of previous, current years 
                // relevant to the BLL and on other hazard parameters
                // show his BLL trend for an YEAR

                // get person details
                List<Amco_Person> person2get = new List<Amco_Person>();
                  String queryString = "select * from amco_emp where empID="+selectedID;
               queryString += " && registereWithBeinge=1";
               
                int count = amco.getList(person2get,queryString);
                 if( count ==1)
                 {
                    // using below method
                    DateTime aDay = DateTime.Now;
                    DateTime endDay = aDay.Subtract(new System.TimeSpan(360, 0, 0, 0));
                    
                    String startdate = endDay.ToString("dd-MM-yyyy");   // "15-11-2015";  //get it from the caller of the previous screen
                    String enddate =  aDay.ToString("dd-MM-yyyy");    //"15-11-2016";
                    amco_params ap = new amco_params(278, "BLL");
                Amco_Person p = new Amco_Person(person2get[0]);

                parent.displayAnalytics_on_Person(p, ap, startdate, enddate);
                }
                
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            //previous
            messageRow--;
            checkBoundaries();
            richTextBox2.Text = moreMessages[messageRow];
            showInfoOnGrid(messageRow);
        }
    }
}
